# c3t

Constant Curvature Curve Tube codes for analog error correction of independent continuous uniform sources that can work in any integer dimension. We optimize the curve parameters by using tube packing density within a hypersphere.