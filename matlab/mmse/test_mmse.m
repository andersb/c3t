%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul
%    and Vijay Mishra 
%
% https://stats.stackexchange.com/questions/309400/pdf-of-cosine-of-a-uniform-random-variable
%

clear
format long g

% Radii
r = .9;

% Tx
src = pi/3;
x = r*[cos(src) sin(src)];
n = numel(x);
fprintf('Tx: %.2f degrees\n\n',src*180/pi)

% Compute noise power
SNR = 10;
lin_snr = 10^(SNR/10);
noise_pwr = sum(r.^2) / lin_snr;
std_dev = sqrt(noise_pwr/n);

% Number of angles
count = 400;

% Monte-carlo loop
m = 200;
mmse_hat = zeros(1,m);
l2_hat = mmse_hat;
for mx = 1:m

    % AWGN
    y = x + std_dev*randn(1,n);
    %noise=y-x
    
    % Loop for alpha +/-PI
    [alpha_hat,mmse,a_hat,l2,alpha] = mmse_est(y,r,std_dev,count);
    l2_hat(mx)      = a_hat;
    mmse_hat(mx)    = alpha_hat;

end % for

% RMS errors
rms_l2 = sqrt(mean((src-l2_hat).^2));
rms_mmse = sqrt(mean((src-mmse_hat).^2));

hat = mean(mmse_hat)*180/pi;
fprintf('Rx: %.2f degrees\tMMSE RMS: %.5f\tL2 RMS: %.5f\n\n',hat,rms_mmse,rms_l2)

figure(10)
plot(alpha/pi,l2),grid
xlabel('Alpha [rad/pi]')

figure(20)
plot(alpha/pi,mmse),grid
xlabel('Alpha [rad/\pi]')

figure(30)
plot(alpha/pi, mmse),grid
xlabel('$\hat{\alpha} \; [rad/\pi]$','Interpreter','latex')
ylabel('$E[|y-x(\alpha)|^2]$','Interpreter','latex')
title('$E[|y-x(\alpha)|^2] \;  vs. \; \hat{\alpha}$','Interpreter','latex')
legend('C3T n=2 with Source $\alpha = \pi/3$','Interpreter','latex')
