%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul
%    and Vijay Mishra 
%
% https://stats.stackexchange.com/questions/309400/pdf-of-cosine-of-a-uniform-random-variable
%

function [alpha_hat,mmse,a_l2,l2,alpha] = mmse_est(y,r,std_dev,count)

% Compute angles
steps = count;
half = steps/2;
l2 = zeros(1,steps);
mmse = l2;
rng = 1:steps;
alpha = 2*pi*(rng-1-half)/steps;

% Loop for alpha +/-PI
for sx = rng

    a = alpha(sx);
    x_hat = r*[cos(a) sin(a)];
    
    l2(sx) = norm(x_hat-y);
    
    m = 0;
    for ix = rng

        a = alpha(ix);
        x = r*[cos(a) sin(a)];

        %d = (x-x_hat).^2;
        no = norm(x-x_hat);
        
        % p(y|x)
        p_y_x1 = normpdf(y(1),x(1),std_dev);
        p_y_x2 = normpdf(y(2),x(2),std_dev);
    
        % p(x)
        p_x1 = pdf_x(x(1),r);
        p_x2 = pdf_x(x(2),r);

        % MMSE
        m = m + no*p_y_x1*p_y_x2*p_x1*p_x2;
        
        if isinf(m)
            m = 0;
        end

    end % for x

    % MMSE
    mmse(sx) = m;

end % for

% Return
[~,ix] = min(l2);
a_l2 = alpha(ix);
[~,ix] = min(mmse);
alpha_hat = alpha(ix);
end % mmse_est()

% PDF cos(alpha) / sin(alpha)
function p = pdf_x(x,r)
p = 1./sqrt(1-(0.99*x/r).^2)/pi;
end % pdf_x

