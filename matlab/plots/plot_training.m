%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul
%    and Vijay Mishra 
%

% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -5:+32;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Compute OPTA
opta4 = sdr(4,snr_rng);

% Read Raw files
fd_5_10 = fopen('c3t_sdr_vs_snr_n4_Raw_5_10.txt','r');
fd_0_10 = fopen('c3t_sdr_vs_snr_n4_Raw_0_5_10.txt','r');
fd_0_15 = fopen('c3t_sdr_vs_snr_n4_Raw_0_5_10_15.txt','r');
fd_m5_10 = fopen('c3t_sdr_vs_snr_n4_Raw_m5_0_5_10.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_Raw_layers_5.txt','r');
sdrRaw_5_10 = fscanf(fd_5_10,'%f');
sdrRaw_0_10 = fscanf(fd_0_10,'%f');
sdrRaw_0_15 = fscanf(fd_0_15,'%f');
sdrRaw_m5_10 = fscanf(fd_m5_10,'%f');
sdr4Raw = fscanf(fd4,'%f');
fclose(fd4);fclose(fd_5_10);fclose(fd_0_10);fclose(fd_0_15);fclose(fd_m5_10);

figure(11)

plot(snr_rng,opta4,'k-.'),grid,hold
plot(snr_rng,sdr4Raw,'b-s')
plot(snr_rng,sdrRaw_0_15,'r-d')
plot(snr_rng,sdrRaw_m5_10,'c-x')
plot(snr_rng,sdrRaw_0_10,'g-o')
plot(snr_rng,sdrRaw_5_10,'k-^')
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 4')
legend('OPTA','RAW TRAIN: +10 dB','RAW TRAIN: 0,+5,+10,+15 dB','RAW TRAIN: -5,0,+5,+10 dB','RAW TRAIN: 0,+5,+10 dB','RAW TRAIN: +5,+10 dB')
axis(ax)

% EOF