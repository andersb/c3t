%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul
%    and Vijay Mishra 
%

% Accuracy at 10 dB for the following seven feature categories:
% Raw, Torus Proj, Angles Only, Raw+Angles, TP+Angles, Raw+TP, Raw+TP+Angles, REP, REP MMSE, MAP, MMSE
a2      = [ 88.3 86.4 86.5 88.4 86.5 88.4 88.4 95.6 91.05 93 93 ];
a4      = [ 96.3 95.2 95.2 96.5 94.8 96.6 96.1 96.9 93.66 97 97 ];
a6      = [ 97.6 97.9 97.8 97.6 97.8 97.1 97.5 97.4 94.86 98.3 98.3 ];
a8      = [ 98 97.6 98.5   97.7 98.4 97.9 97.8 97.8 95.52 98.8 98.8 ];
a20     = [ 97.5 99 98.7   96.7 99   98   97.9 98.6 97.20 99.6 99.6 ];
a40     = [ 98.8 98.6 98.9 96.2 98.4 98.4 97.1 98.8 98 99.9 99.9    ];
a100    = [ 97.3 97.9 97   97.7 97.2 98.2 96.7 99.4 98.74 99.96 99.96 ];

n = [ 2 4 6 8 20 40 100 ];
z = [a2; a4; a6; a8; a20; a40; a100];

figure(15)
%bar3(z,0.25,'detached')
bar(z)
grid
set(gca, 'XTickLabel',{'n = 2','n = 4','n = 6','n = 8','n = 20','n = 40','n = 100'})
xlabel('Dimensions')
ylabel('Accuracy %')
title('Accuracy of Constant Curvature Curve Tube Codes at 10 dB SNR')
legend('Raw','Torus Projection','Angles Only','Raw+Angles','Torus Projection+Angles',...
       'Raw+Torus Projection','Raw+Torus Projection+Angles','REP','REP MMSE','MAP','MMSE')
%ylim([80 100])
