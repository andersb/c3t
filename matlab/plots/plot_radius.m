%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul 
%    and Vijay Mishra 
%

% Range of Delta
range = 0:res:2*pi;

plot(range/pi,radii),hold,grid
plot(range/pi,q,'r'),hold
legend('\rho^2','Derivative of \rho^2 with respect to \Delta')
xlabel('\Delta/\pi = \alpha_1 - \alpha_2')
ylabel('\rho^2')
title(sprintf('Circumradius for n = %d',numel(r)*2))

% EOF