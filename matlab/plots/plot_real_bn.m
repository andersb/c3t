%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
%


% Read accuracy files
fd = fopen('accu_c3t_n8_Raw_layers_5_type_real_act_tanh_small.txt','r');
raw_sm = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_real_act_tanh.txt','r');
raw = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_real_act_tanh_bn.txt','r');
raw_bn = fscanf(fd,'%f');
fclose(fd);

% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -2:+30;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;

% Plot BER
figure(35);
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(snr_rng,raw_sm,'-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
plot(snr_rng,raw,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,raw_bn,'-','LineWidth',line_width,'MarkerSize',marker_size)
hold

legend('Real-valued R4C Fixed','Real-valued R4C','Real-valued R4C BN')
xlabel('SNR [dB]')
ylabel('SDR [dB]')
%title('Dimension n = 8')
axis(ax)


% EOF