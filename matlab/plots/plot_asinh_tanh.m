%
%    Plot hyperbolic arcsin(z)
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

figure(10)

s=linspace(-pi,pi,100);
[re,im]=ndgrid(s,s);
z = re + 1j*im;
as = asinh(z);
mesh(s,s,abs(as))
xlabel('Real')
ylabel('Imaginary')
zlabel('|asinh(z)|')

figure(20)

mesh(s,s,atan2(imag(as),real(as)))
xlabel('Real')
ylabel('Imaginary')
zlabel('$\angle$ asinh(z) [rads]','interpreter','latex')

figure(30)
th = tanh(re)+j*tanh(im);
mesh(s,s,abs(th))
xlabel('Real')
ylabel('Imaginary')
zlabel('|tanh(re)+i*tanh(im)|')

figure(40)
mesh(s,s,atan2(tanh(im),tanh(re)))
xlabel('Real')
ylabel('Imaginary')
zlabel('$\angle$ tanh(re) + i*tanh(im)','interpreter','latex')

% EOF