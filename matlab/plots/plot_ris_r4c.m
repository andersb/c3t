%
% Plot performance of RIS-generated R4C
%
% Anders Buvarp
%
% Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Load BER data
fdRe = fopen('../../python/accuracy/n4/real/accu_c3t_n4_Raw_layers_5_type_real_act_tanh.txt');
fdCplx = fopen('../../python/accuracy/n4/cplx/accu_c3t_n4_Raw_layers_5_type_cplx_act_asinh.txt');
fdReLU = fopen('../../python/accuracy/n4/qnn/accuracy_ReLU_r4c.txt','r');
fdModReLUb02 = fopen('../../python/accuracy/n4/qnn/accuracy_HModReLU_b_02_r4c.txt','r');
fdModReLUb05 = fopen('../../python/accuracy/n4/qnn/accuracy_HModReLU_b_05_r4c.txt','r');
fdModReLUb08 = fopen('../../python/accuracy/n4/qnn/accuracy_HModReLU_b_08_r4c.txt','r');
fdAsinh = fopen('../../python/accuracy/n4/qnn/accuracy_asinh_r4c.txt','r');
fdCasinh = fopen('../../python/accuracy/n4/qnn/accuracy_Casinh_r4c.txt','r');
fdTanh = fopen('../../python/accuracy/n4/qnn/accuracy_tanh_r4c.txt','r');
fdHardTanh = fopen('../../python/accuracy/n4/qnn/accuracy_hardtanh_r4c.txt','r');

% Read BER data from files
sdr_re = fscanf(fdRe,'%f');
sdr_cplx = fscanf(fdCplx,'%f');
sdr_ReLU = fscanf(fdReLU,'%f');
sdr_ModReLU_b_02 = fscanf(fdModReLUb02,'%f');
sdr_ModReLU_b_05 = fscanf(fdModReLUb05,'%f');
sdr_ModReLU_b_08 = fscanf(fdModReLUb08,'%f');
sdr_Asinh = fscanf(fdAsinh,'%f');
sdr_Casinh = fscanf(fdCasinh,'%f');
sdr_Tanh = fscanf(fdTanh,'%f');
sdr_HardTanh = fscanf(fdHardTanh,'%f');

% Close files
fclose(fdRe);fclose(fdCplx);
fclose(fdReLU);fclose(fdModReLUb02);fclose(fdModReLUb05);fclose(fdModReLUb08);
fclose(fdAsinh);fclose(fdCasinh);fclose(fdTanh);fclose(fdHardTanh);

% SNR range
snr_rng = -5:10;
opta = sdr(4,snr_rng);

% Font and marker sizes
font_size = 35;
marker_size = 1;
line_width = 4;

% Plot BER with IMPN for LOS
f99 = figure(99);
set(f99,'defaulttextinterpreter','latex');
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(snr_rng,sdr_re(6:end),'--','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
plot(snr_rng,sdr_cplx(6:end),'-.','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_ReLU,'h-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_ModReLU_b_02,'^-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_ModReLU_b_05,'<-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_ModReLU_b_08,'d-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_Tanh,'o-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_HardTanh,'*-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_Asinh,'v-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,sdr_Casinh,'--','LineWidth',line_width,'MarkerSize',marker_size),hold

xlabel('SNR [dB]')
ylabel('SDR [dB]')
legend('Real Tanh','CV Casinh','RIS QNN ReLU',...
       'RIS QNN ModReLU b=0.2','RIS QNN ModReLU b=0.5','RIS QNN ModReLU b=0.8',...
       'RIS QNN Tanh','RIS QNN HardTanh','RIS QNN Asinh','RIS QNN Casinh')

%xlim([-15 10])
ylim([0 35 ])

% EOF
