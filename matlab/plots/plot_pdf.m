%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

clear 
sigma = 0.5;
s = sqrt(sigma);
r = [0.5125, 0.5162, 0.5551, 0.4035];
ix = -1.53:.01:1.53;
theta = ix;
alpha = 0;

mu_x = r(1)*cos(alpha);
mu_y = r(1)*sin(alpha);

a = mu_x*tan(theta) + mu_y;
c = 0.5*cos(theta).^2 / s^2;
e = 0.5*sqrt(pi./c).*(erf(a.*sqrt(c))+erf(0.5*pi*sqrt(c)));
h = sin(theta)/sqrt(2*pi)/s.*(sec(theta).*(e-a.*exp(-a.*a.*c)-0.5*pi*exp(-0.25*c*pi^2))-e);

pdf = mu_x*sec(theta) / s / sqrt(2*pi) .* exp(-.5/s^2*(mu_x^2*sin(theta).^2)) + h;


% Font and marker sizes
font_size = 35;
marker_size = 1;
line_width = 4;

% Plot BER
f78 = figure(78);
set(f78,'defaulttextinterpreter','latex');


set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(theta,fftshift(pdf),'LineWidth',line_width,'MarkerSize',marker_size)
grid
xlabel('$\alpha [rads]$')
ylabel('$PDF$')
title('$PDF f(\alpha)$')
% EOF