%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% n=8
n8_hub = [1.3402,1.28051,1.23267,1.17039,1.11273,1.05575,0.97639,0.90646,0.82829,0.75165,0.67367,0.59189,0.51935,0.45073,0.38986,0.33593,0.28939,0.25184,0.22151,0.19559,0.1728];
n8_arc = [3.66049,3.56607,3.46016,3.33313,3.18581,3.06058,2.88591,2.71838,2.52693,2.33007,2.11054,1.8774,1.66633,1.45531,1.26167,1.08755,0.93401,0.81173,0.70741,0.6232,0.54749];
n40_hub = [0.80996,0.7617,0.707,0.64942,0.5904,0.53981,0.48153,0.4284,0.38402,0.33789,0.29954,0.26349,0.234,0.2055,0.1832,0.16114,0.143,0.12696,0.11229,0.09966,0.08812];
n40_arc = [8.4789,8.28086,8.01848,7.73944,7.42212,7.10917,6.71138,6.31403,5.88184,5.44691,4.94811,4.44602,3.92118,3.4199,2.98145,2.56887,2.22103,1.92214,1.67023,1.47435,1.29937];
n100_hub = [0.62241,0.57475,0.52221,0.46796,0.41937,0.37138,0.32427,0.28604,0.24787,0.21938,0.19156,0.16908,0.14968,0.13223,0.1172,0.10347,0.09175,0.08164,0.07229,0.06432,0.05741];
n100_arc = [13.52239,13.20487,12.8021,12.31929,11.83953,11.33267,10.66812,10.06293,9.38986,8.65161,7.84536,7.04011,6.24709,5.49701,4.75403,4.09382,3.54994,3.07572,2.67221,2.34792,2.06766];


% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -2:+30;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Font and marker sizes
font_size = 40;
marker_size = 8;
line_width = 4;

% Plot
figure(85)
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(snr_rng,n8_hub,'-','LineWidth',line_width,'MarkerSize',marker_size)
hold, grid
plot(snr_rng,n8_arc,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n40_hub,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n40_arc,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n100_hub,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n100_arc,'-','LineWidth',line_width,'MarkerSize',marker_size)
hold

legend('n=8 M-estimator','n=8 Acos/Asin','n=40 M-estimator','n=40 Acos/Asin','n=100 M-estimator','n=100 Acos/Asin')
xlabel('SNR [dB]')
ylabel('RMS Error [rads]')
%axis(ax)


% EOF