%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul
%    and Vijay Mishra 
%

% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -2.5:+42.5;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Compute OPTA
opta2 = sdr(2,snr_rng);
opta3 = sdr(3,snr_rng);
opta4 = sdr(4,snr_rng);
opta6 = sdr(6,snr_rng);
opta8 = sdr(8,snr_rng);
opta20 = sdr(20,snr_rng);
opta40 = sdr(40,snr_rng);
opta100 = sdr(100,snr_rng);

% Read MAP files
fd2 = fopen('c3t_sdr_vs_snr_n2_MMSE.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_MMSE.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_MMSE.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_MMSE.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_MMSE.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_MMSE.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_MMSE.txt','r');
sdr2MMSE = fscanf(fd2,'%f');
sdr4MMSE = fscanf(fd4,'%f');
sdr6MMSE = fscanf(fd6,'%f');
sdr8MMSE = fscanf(fd8,'%f');
sdr20MMSE = fscanf(fd20,'%f');
sdr40MMSE = fscanf(fd40,'%f');
sdr100MMSE = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read repetition code files
fd2 = fopen('c3t_sdr_vs_snr_n2_Rep.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_Rep.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_Rep.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_Rep.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_Rep.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_Rep.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_Rep.txt','r');
sdr2Rep = fscanf(fd2,'%f');
sdr4Rep = fscanf(fd4,'%f');
sdr6Rep = fscanf(fd6,'%f');
sdr8Rep = fscanf(fd8,'%f');
sdr20Rep = fscanf(fd20,'%f');
sdr40Rep = fscanf(fd40,'%f');
sdr100Rep = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read MMSE repetition code files
fd2 = fopen('c3t_sdr_vs_snr_n2_Rep_MMSE.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_Rep_MMSE.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_Rep_MMSE.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_Rep_MMSE.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_Rep_MMSE.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_Rep_MMSE.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_Rep_MMSE.txt','r');
sdr2RepMMSE = fscanf(fd2,'%f');
sdr4RepMMSE = fscanf(fd4,'%f');
sdr6RepMMSE = fscanf(fd6,'%f');
sdr8RepMMSE = fscanf(fd8,'%f');
sdr20RepMMSE = fscanf(fd20,'%f');
sdr40RepMMSE = fscanf(fd40,'%f');
sdr100RepMMSE = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read MAP files
fd2 = fopen('c3t_sdr_vs_snr_n2_MAP.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_MAP.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_MAP.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_MAP.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_MAP.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_MAP.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_MAP.txt','r');
sdr2MAP = fscanf(fd2,'%f');
sdr4MAP = fscanf(fd4,'%f');
sdr6MAP = fscanf(fd6,'%f');
sdr8MAP = fscanf(fd8,'%f');
sdr20MAP = fscanf(fd20,'%f');
sdr40MAP = fscanf(fd40,'%f');
sdr100MAP = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Raw files
fd2 = fopen('c3t_sdr_vs_snr_n2_Raw_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_Raw_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_Raw_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_Raw_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_Raw_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_Raw_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_Raw_layers_5.txt','r');
sdr2Raw = fscanf(fd2,'%f');
sdr4Raw = fscanf(fd4,'%f');
sdr6Raw = fscanf(fd6,'%f');
sdr8Raw = fscanf(fd8,'%f');
sdr20Raw = fscanf(fd20,'%f');
sdr40Raw = fscanf(fd40,'%f');
sdr100Raw = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);%fclose(fd20);fclose(fd40);fclose(fd100);

% Read Torus Projection files
fd2 = fopen('c3t_sdr_vs_snr_n2_TorusProj_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_TorusProj_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_TorusProj_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_TorusProj_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_TorusProj_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_TorusProj_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_TorusProj_layers_5.txt','r');
sdr2TP = fscanf(fd2,'%f');
sdr4TP = fscanf(fd4,'%f');
sdr6TP = fscanf(fd6,'%f');
sdr8TP = fscanf(fd8,'%f');
sdr20TP = fscanf(fd20,'%f');
sdr40TP = fscanf(fd40,'%f');
sdr100TP = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Angle Only files
fd2 = fopen('c3t_sdr_vs_snr_n2_AngleOnly_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_AngleOnly_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_AngleOnly_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_AngleOnly_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_AngleOnly_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_AngleOnly_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_AngleOnly_layers_5.txt','r');
sdr2AO = fscanf(fd2,'%f');
sdr4AO = fscanf(fd4,'%f');
sdr6AO = fscanf(fd6,'%f');
sdr8AO = fscanf(fd8,'%f');
sdr20AO = fscanf(fd20,'%f');
sdr40AO = fscanf(fd40,'%f');
sdr100AO = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Raw + Torus Projection files
fd2 = fopen('c3t_sdr_vs_snr_n2_RawTP_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_RawTP_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_RawTP_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_RawTP_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_RawTP_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_RawTP_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_RawTP_layers_5.txt','r');
sdr2RawTP = fscanf(fd2,'%f');
sdr4RawTP = fscanf(fd4,'%f');
sdr6RawTP = fscanf(fd6,'%f');
sdr8RawTP = fscanf(fd8,'%f');
sdr20RawTP = fscanf(fd20,'%f');
sdr40RawTP = fscanf(fd40,'%f');
sdr100RawTP = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Raw + Angle Only files
fd2 = fopen('c3t_sdr_vs_snr_n2_RawAO_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_RawAO_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_RawAO_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_RawAO_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_RawAO_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_RawAO_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_RawAO_layers_5.txt','r');
sdr2RawAO = fscanf(fd2,'%f');
sdr4RawAO = fscanf(fd4,'%f');
sdr6RawAO = fscanf(fd6,'%f');
sdr8RawAO = fscanf(fd8,'%f');
sdr20RawAO = fscanf(fd20,'%f');
sdr40RawAO = fscanf(fd40,'%f');
sdr100RawAO = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Torus Projection + Angle Only files
fd2 = fopen('c3t_sdr_vs_snr_n2_TPAO_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_TPAO_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_TPAO_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_TPAO_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_TPAO_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_TPAO_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_TPAO_layers_5.txt','r');
sdr2TPAO = fscanf(fd2,'%f');
sdr4TPAO = fscanf(fd4,'%f');
sdr6TPAO = fscanf(fd6,'%f');
sdr8TPAO = fscanf(fd8,'%f');
sdr20TPAO = fscanf(fd20,'%f');
sdr40TPAO = fscanf(fd40,'%f');
sdr100TPAO = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

% Read Raw + Torus Projection + Angle Only files
fd2 = fopen('c3t_sdr_vs_snr_n2_RawTPAO_layers_5.txt','r');
fd4 = fopen('c3t_sdr_vs_snr_n4_RawTPAO_layers_5.txt','r');
fd6 = fopen('c3t_sdr_vs_snr_n6_RawTPAO_layers_5.txt','r');
fd8 = fopen('c3t_sdr_vs_snr_n8_RawTPAO_layers_5.txt','r');
fd20 = fopen('c3t_sdr_vs_snr_n20_RawTPAO_layers_5.txt','r');
fd40 = fopen('c3t_sdr_vs_snr_n40_RawTPAO_layers_5.txt','r');
fd100 = fopen('c3t_sdr_vs_snr_n100_RawTPAO_layers_5.txt','r');
sdr2RawTPAO = fscanf(fd2,'%f');
sdr4RawTPAO = fscanf(fd4,'%f');
sdr6RawTPAO = fscanf(fd6,'%f');
sdr8RawTPAO = fscanf(fd8,'%f');
sdr20RawTPAO = fscanf(fd20,'%f');
sdr40RawTPAO = fscanf(fd40,'%f');
sdr100RawTPAO = fscanf(fd100,'%f');
fclose(fd2);fclose(fd4);fclose(fd6);fclose(fd8);fclose(fd20);fclose(fd40);fclose(fd100);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(22)

subplot(1,4,1)
plot(snr_rng,opta2,'k-.'),grid,hold
plot(snr_rng,sdr2Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr2RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr2MAP,'c.-','MarkerSize',5)
plot(snr_rng,sdr2MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr2Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr2TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr2AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr2RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr2RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr2TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr2RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 2')
leg = {'OPTA Lower Bound','REP','REP LMMSE','MAP','MMSE','RAW','TP','AO','RAW+TP','RAW+AO','TP+AO','RAW+TP+AO'};
legend(leg)
axis(ax)

subplot(1,4,2)
plot(snr_rng,opta4,'k-.'),grid,hold
plot(snr_rng,sdr4Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr4RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr4MAP,'c.-','MarkerSize',5)
plot(snr_rng,sdr4MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr4Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr4TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr4AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr4RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr4RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr4TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr4RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 4')
legend(leg)
axis(ax)

subplot(1,4,3)
plot(snr_rng,opta6,'k-.'),grid,hold
plot(snr_rng,sdr6Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr6RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr6MAP,'c.-','MarkerSize',5)
plot(snr_rng,sdr6MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr6Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr6TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr6AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr6RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr6RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr6TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr6RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 6')
legend(leg)
axis(ax)

subplot(1,4,4)
plot(snr_rng,opta8,'k-.'),grid,hold
plot(snr_rng,sdr8Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr8RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr8MAP,'c.-','MarkerSize',5)
plot(snr_rng,sdr8MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr8Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr8TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr8AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr8RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr8RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr8TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr8RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 8')
legend(leg)
axis(ax)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%
 
figure(44)

subplot(1,3,1)
plot(snr_rng,opta20,'k-.'),grid,hold
plot(snr_rng,sdr20Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr20RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr20MAP,'c-^','MarkerSize',5)
plot(snr_rng,sdr20MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr20Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr20TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr20AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr20RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr20RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr20TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr20RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('Dimension n = 20')
legend(leg)
axis(ax)

subplot(1,3,2)
plot(snr_rng,opta40,'k-.'),grid,hold
plot(snr_rng,sdr40Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr40RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr40MAP,'c-^','MarkerSize',5)
plot(snr_rng,sdr40MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr40Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr40TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr40AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr40RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr40RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr40TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr40RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
title('Dimension n = 40')
legend(leg)
axis(ax)

subplot(1,3,3)
plot(snr_rng,opta100,'k-.'),grid,hold
plot(snr_rng,sdr100Rep,'g>-','MarkerSize',5)
plot(snr_rng,sdr100RepMMSE,'kv-','MarkerSize',5)
plot(snr_rng,sdr100MAP,'c-^','MarkerSize',5)
plot(snr_rng,sdr100MMSE,'m-x','MarkerSize',5)
plot(snr_rng,sdr100Raw,'b-s','MarkerSize',5)
plot(snr_rng,sdr100TP,'r-o','MarkerSize',5)
plot(snr_rng,sdr100AO,'g-h','MarkerSize',5)
plot(snr_rng,sdr100RawTP,'m-p','MarkerSize',5)
plot(snr_rng,sdr100RawAO,'b-d','MarkerSize',5)
plot(snr_rng,sdr100TPAO,'r-*','MarkerSize',5)
plot(snr_rng,sdr100RawTPAO,'c-<','MarkerSize',5)
hold
xlabel('SNR [dB]')
title('Dimension n = 100')
legend(leg)
axis(ax)

%%

% Odd dimension

fd3 = fopen('c3t_sdr_vs_snr_n3_Odd_layers_5.txt','r');
sdr3Odd = fscanf(fd3,'%f');
fclose(fd3);

figure(50)

plot(snr_rng,opta3,'k-.'),grid,hold
plot(snr_rng,opta4,'k--')
plot(snr_rng,sdr3Odd,'b.-','MarkerSize',5)
plot(snr_rng,sdr4Raw,'r','MarkerSize',5),hold

xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('SDR vs SNR')
legend('OPTA Lower Bound, n=3','OPTA Lower Bound, n=4','C3T RAW, n=3', 'C3T RAW, n=4')
axis(ax)
ylim([-2 20])

%%

% n = 2, SNR: 0 to 25

snr_rng = 0:+25;
sdr_rng = -10:+50;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Compute OPTA for n=2 and SNR 0 to 25 dB
opta_n2_snr_0_to_25 = sdr(2,snr_rng);

% SDR for n2 and SNR 0 to 25 dB
fd = fopen('c3t_n2_sdr_vs_snr_0_to_25.txt','r');
sdr_n2_vs_snr_0_to_25 = fscanf(fd,'%f');
fclose(fd);

% Archimedes Spiral
scale = 60/137.5;
archimedes_MMSE = [12 21 35 54 74 95]*scale;
archimedes_ML = [-14 8 30 51 72.5 94]*scale;
spiral_MMSE = [5.5 10 16.5 25 34 42];

figure(60)

plot(snr_rng,opta_n2_snr_0_to_25,'k-.'),grid,hold
plot(snr_rng,sdr_n2_vs_snr_0_to_25,'b.-','MarkerSize',5)
plot(0:5:25,archimedes_MMSE,'r-o','MarkerSize',5)
plot(0:5:25,archimedes_ML,'m-+','MarkerSize',5)
plot(0:5:25,spiral_MMSE,'g-x','MarkerSize',5),hold

xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('SDR vs SNR')
legend('OPTA Lower Bound','C3T Raw, n=2', 'Archimedes Spiral with MMSE decoder',...
       'Archimedes Spiral with ML decoder','Spiral-like curves with discretized MMSE')

axis(ax)

%%
if 0
% n = 3, SNR: 5 to 25

snr_rng = +5:+25;
sdr_rng = 0:+60;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Compute OPTA for n=3 and SNR 5 to 25 dB
opta_n3_snr_5_to_25 = sdr(3,snr_rng);

% SDR for n3 and SNR 5 to 25 dB
fd = fopen('c3t_n3_sdr_vs_snr_5_to_25.txt','r');
sdr_vs_snr_5_to_25 = fscanf(fd,'%f');
fclose(fd);

% Archimedes Spiral
scale = 60/138.5;
hybrid = [25 45 69 100 132.5]*scale;
hsqlc = [16 30.5 60 90 120.5]*scale;

figure(70)

plot(snr_rng,opta_n3_snr_5_to_25,'k-.'),grid,hold
plot(snr_rng,sdr_vs_snr_5_to_25,'b.-','MarkerSize',5)
plot(5:5:25,hybrid,'r','MarkerSize',5)
plot(5:5:25,hsqlc,'g-+','MarkerSize',5),hold

xlabel('SNR [dB]')
ylabel('SDR [dB]')
title('SDR vs SNR')
legend('OPTA Lower Bound','C3T Raw, n=3', 'Hybrid digital-analog coder', 'Generalized HSQLC')

axis(ax)
end % if
%%

% n = 8, SNR: 10 to 25
n = 8;

snr_rng = +10:+25;
sdr_rng = +15:+85;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Compute OPTA for n=8 and SNR 10 to 25 dB
opta_n8_snr_10_to_25 = sdr(n,snr_rng);

% SDR for n3 and SNR 5 to 25 dB
fd = fopen('c3t_n8_sdr_vs_snr_10_to_25.txt','r');
sdr_vs_snr_10_to_25 = fscanf(fd,'%f');
fclose(fd);

% The SDR for C3T uses the source variance
% The V&C data is 1/MSE in dB and no source variance
source_var = 1/3;
mse_vs_snr_10_to_25 = sdr_vs_snr_10_to_25 - 10*log10(source_var);

% V&C torus decoding
x_torus_scale = 10/132;
y_torus_scale = 50/124;
x_torus = 10 + [7 27 47 67 87 107 127 147]*x_torus_scale;
y_torus = 15 + [4.5 11 20 30 41 88 108 112]*y_torus_scale;

% V&C sphere
x_scale = 20/158;
y_scale = 60/124;
x_points = 6:6:102;
x_sphere = 12 + x_points*x_scale;
y_sphere = 10 + [20.5 25 27 33 40 42 54 59 60.5 100.5 102 103.5 105 107 108.5 110 111.5]*y_scale;

% V&C shift-map
x_shift_map = 12 + x_points(5:end)*x_scale;
y_shift_map = 10 + [18 21 26 29 36.5 45 48 58 62 96 98 99 101]*y_scale;

figure(80)

plot(snr_rng,opta_n8_snr_10_to_25,'k-.'),grid,hold
plot(snr_rng,mse_vs_snr_10_to_25,'b.-','MarkerSize',5)
plot(x_torus,y_torus,'r-o','MarkerSize',5)
plot(x_sphere,y_sphere,'g-x','MarkerSize',5)
plot(x_shift_map,y_shift_map,'m-+','MarkerSize',5),hold

xlabel('SNR [dB]')
ylabel('1/MSE [dB]')
title('MSE^{-1} vs SNR')
legend('OPTA Lower Bound','C3T Raw, n=8','V&C Sphere with Torus Decoding, L=4','V&C Homogeneous Spherical Code, L=4','V&C Shift Map, b_n=3')

axis(ax)

% EOF