% This script plots Figs. 3 and 4 in the C3T paper as a sanity check

% Generate the horizontal axis range
res = 1/1e4;
rng = 0:res:2*pi;
rng = rng/pi/2;

% Load data for n = 4, 8, 20, 100
load tube_radii_n4
load tube_radii_n8
load tube_radii_n20
load tube_radii_n100

% Plot Fig. 3
figure(3)
plot(rng,tube_radii_n4),grid,hold
plot(rng,tube_radii_n8)
plot(rng,tube_radii_n20)
plot(rng,tube_radii_n100),hold
ylim([.4 .85])
xlabel('\Delta [radians/(2\pi)]')
ylabel('\rho^2')
title('Circumradius^2 vs \Delta')

% Load data for n = 3, 7, 19, 99
load tube_radii_n3
load tube_radii_n7
load tube_radii_n19
load tube_radii_n99

% Plot Fig. 4
figure(4)
plot(rng,tube_radii_n3),grid,hold
plot(rng,tube_radii_n7)
plot(rng,tube_radii_n19)
plot(rng,tube_radii_n99),hold
ylim([0 12])
xlabel('\Delta [radians/(2\pi)]')
ylabel('\rho^2')
title('Circumradius^2 vs \Delta')
