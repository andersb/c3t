%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

% Range of SNR and SDR
snr_rng = -10:+10;
min_sdr = -3;
sdr_rng = min_sdr:+30;

order = 100;
feat_type = 'Raw';
ma = qnn_plots(order,feat_type,snr_rng,sdr_rng)
feat_type = 'TorusProj';
maTP = qnn_plots(order,feat_type,snr_rng,sdr_rng)
feat_type = 'RawTP';
maRawTP = qnn_plots(order,feat_type,snr_rng,sdr_rng)

ma = max([ma maTP maRawTP])
ma = ma + 2
subplot(3,1,1)
ylim([min_sdr ma])
subplot(3,1,2)
ylim([min_sdr ma])
subplot(3,1,3)
ylim([min_sdr ma])


%%%
% Plot QNN data in one pane
function ma = qnn_plots(order,type,snr_rng,sdr_rng)

    % Read accuracy files
    re = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_real_act_tanh.txt',order,type));
    cplx = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_cplx_act_asinh.txt',order,type));
    qnn_relu = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_relu.txt',order,type));
    qnn_qModReLU = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_qModReLU.txt',order,type));
    qnn_asinh = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_asinh.txt',order,type));
    qnn_Casinh = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_Casinh.txt',order,type));
    qnn_tanh = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_tanh.txt',order,type));
    qnn_hardtanh = ld_qnn(sprintf('accu_c3t_n%d_%s_layers_5_type_qnn_act_hardtanh.txt',order,type));
    
    % Figure
    switch type
        case 'Raw'
            pane = 1;
        case 'TorusProj'
            pane = 2;    
        case 'RawTP'
            pane = 3;            
    end % if
    fig_qnn(pane,order,type,snr_rng,sdr_rng,re,cplx,qnn_relu,qnn_qModReLU,qnn_asinh,qnn_Casinh,qnn_tanh,qnn_hardtanh)
    
    ma = max([re; cplx; qnn_relu; qnn_qModReLU; qnn_asinh; qnn_Casinh; qnn_tanh; qnn_hardtanh])
    %ma = round(ma+.5);
    %ylim([-2 ma+2])

end % plot_qnn

%%%
% Make QNN figure
function fig_qnn(pane,order,feat_type,snr_rng,sdr_rng,re,cplx,relu,qModReLU,asinh,Casinh,tanh,hardtanh)

	% Font and marker sizes
	font_size = 19;
	marker_size = 15;
	line_width = 4;

    % Archimedes Spiral
    scale = 60/137.5;
    archimedes_MMSE = [12 21 35]*scale;

    opta = sdr(order,snr_rng);

	% Plot BER
    if pane == 1
	    figure('color','w');
      	set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
               'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 
    end % if
    subplot(3,1,pane)
	plot(snr_rng,opta,'k-','LineWidth',line_width,'MarkerSize',marker_size),hold,grid
    plot(0:5:10,archimedes_MMSE,'--','LineWidth',line_width,'MarkerSize',marker_size)
    plot(snr_rng,re,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,cplx,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,relu,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,qModReLU,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,asinh,'-','LineWidth',line_width,'MarkerSize',marker_size)
    plot(snr_rng,Casinh,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,tanh,'-','LineWidth',line_width,'MarkerSize',marker_size)
	plot(snr_rng,hardtanh,'r-','LineWidth',line_width,'MarkerSize',marker_size)
	hold

    legend(sprintf('OPTA, n = %d',order),'Archimedes Spiral, n = 2', ...
           sprintf('Real %s tanh',feat_type), ...
           sprintf('CV %s CArcsinh',feat_type), ...
           sprintf('QNN %s HReLU',feat_type), ...
           sprintf('QNN %s HModReLU b=0.8',feat_type), ...
           sprintf('QNN %s Split-Arcsinh',feat_type), ...
           sprintf('QNN %s Split-CArcsinh',feat_type), ...
           sprintf('QNN %s Split-Tanh',feat_type), ...
           sprintf('QNN %s Split-Hardtanh',feat_type),'location','eastoutside')
    
    if pane == 3
        xlabel('SNR [dB]')
    end % if
    ylabel('SDR [dB]')
    %title(sprintf('QNN C3T %s with Dimension n = %d',feat_type,order))

    ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];
    axis(ax)
	
end % fig_qnn

function accu = ld_qnn(fname)
	fd = fopen(fname,'r');
	accu = fscanf(fd,'%f');
	fclose(fd);
end % ld_qnn

% EOF