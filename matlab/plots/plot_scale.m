%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
%


% Read accuracy files

% Raw accuracy
fd = fopen('accu_c3t_n8_Raw_layers_5_type_real_act_tanh.txt','r');
n8 = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n40_Raw_layers_5_type_real_act_tanh.txt','r');
n40 = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n100_Raw_layers_5_type_real_act_tanh.txt','r');
n100 = fscanf(fd,'%f');
fclose(fd);

% Scaled
fd = fopen('accu_c3t_n8_Raw_layers_5_type_real_act_tanh_scale.txt','r');
n8s = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n40_Raw_layers_5_type_real_act_tanh_scale.txt','r');
n40s = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n100_Raw_layers_5_type_real_act_tanh_scale.txt','r');
n100s = fscanf(fd,'%f');
fclose(fd);

% RTP accuracy
fd = fopen('accu_c3t_n8_RTP_layers_5_type_real_act_tanh_scale.txt','r');
n8rtp = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n40_RTP_layers_5_type_real_act_tanh_scale.txt','r');
n40rtp = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n100_RTP_layers_5_type_real_act_tanh_scale.txt','r');
n100rtp = fscanf(fd,'%f');
fclose(fd);

% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -2:+30;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Font and marker sizes
font_size = 35;
marker_size = 8;
line_width = 4;

% Plot
%figure('color','w');
figure(25)
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(snr_rng,n8,'-','LineWidth',line_width,'MarkerSize',marker_size)
hold, grid
plot(snr_rng,n8s,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n8rtp,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n40,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n40s,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n40rtp,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n100,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n100s,'d-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,n100rtp,'h-','LineWidth',line_width,'MarkerSize',marker_size)
hold

legend('C3T Real n=8','C3T Real Scale n=8','C3T Real RTP n=8','C3T Real n=40','C3T Real Scale n=40','C3T Real RTP n=40','C3T Real n=100','C3T Real Scale n=100','C3T Real RTP n=100')
xlabel('SNR [dB]')
ylabel('SDR [dB]')
%title('Dimension n = 4')
axis(ax)


% EOF