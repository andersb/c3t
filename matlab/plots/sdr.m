%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

function [s_low,s_upp] = sdr(n,snr)

% Convert to linear SNR
snr_lin = 10.^(snr/10.0);

% Compute linear SDR
e = exp(1);
sdr_lin = (pi*e/6.0) * (1+snr_lin).^n;

% Compute SDR upper bound
s_upp = 10.0*log10(sdr_lin);

% Capacity
C = 0.5*log(1+snr_lin);

% Gaussian source
sdr_lin = exp(2*n*C);

% Compute SDR lower bound
s_low = 10.0*log10(sdr_lin);

% EOF