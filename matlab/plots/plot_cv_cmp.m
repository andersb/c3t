%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
%


% Read complex-valued accuracy files
fd = fopen('accu_c3t_n8_Raw_layers_5_type_pop_act_asinh.txt','r');
pop = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_pop_act_asinh_bn.txt','r');
pop_bn = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_pop_act_asinh_bn_larger.txt','r');
pop_bn_lg = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_cplx_act_asinh.txt','r');
cplx = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_cplx_act_asinh_bn.txt','r');
cplx_bn = fscanf(fd,'%f');
fclose(fd);
fd = fopen('accu_c3t_n8_Raw_layers_5_type_cplx_act_asinh_bn_larger.txt','r');
cplx_bn_lg = fscanf(fd,'%f');
fclose(fd);

% Range of SNR and SDR
snr_rng = -10:+10;
sdr_rng = -2:+24;
ax = [snr_rng(1) snr_rng(end) sdr_rng(1) sdr_rng(end)];

% Font and marker sizes
font_size = 40;
marker_size = 1;
line_width = 4;

% Plot
%figure('color','w');
figure(25)
set(0, 'DefaultTextFontName', 'Calibri', 'DefaultTextFontSize', font_size, ...
       'DefaultAxesFontName', 'Calibri', 'DefaultAxesFontSize', font_size); 

plot(snr_rng,pop,'h-','LineWidth',line_width,'MarkerSize',marker_size)
hold, grid
plot(snr_rng,cplx,'p-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,pop_bn,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,cplx_bn,'s-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,pop_bn_lg,'-','LineWidth',line_width,'MarkerSize',marker_size)
plot(snr_rng,cplx_bn_lg,'d-','LineWidth',line_width,'MarkerSize',marker_size)
hold

legend('CV-R4C Fixed complexPyTorch','CV-R4C Fixed CplxModule','CV-R4C Fixed BN complexPyTorch',...
    'CV-R4C Fixed BN CplxModule','CV-R4C BN complexPyTorch','CV-R4C BN CplxModule')
xlabel('SNR [dB]')
ylabel('SDR [dB]')
%title('Dimension n = 8')
axis(ax)


% EOF