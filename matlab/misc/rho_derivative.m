%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%


r4 = [0.8165, 0.5774];
r6 = [0.5835, 0.6463, 0.4918];
r8 = [0.5125, 0.5162, 0.5551, 0.4035];
r10 = [0.4505, 0.4402, 0.4708, 0.5000, 0.3628];
r12 = [0.4141, 0.3953, 0.4102, 0.4346, 0.4566, 0.3265];

% Set r
dim = 40;
r = ones(1,dim/2);
r = r / norm(r);
r = r4;

% Set omega
len = length(r);
omega = 1:length(r);

% Resulution
res = 1e-4;

% Compute circumradius and derivative
[radii,m,der] = circumradius(r,omega,res);

% Print minimum
disp(sprintf('Minimum at Delta = %f [radians]',m))

x = 0:res/pi:(2-res/pi);
plot(x,radii,'r'),hold
plot(x,der,'b'),hold
grid
title('Rho^2(Delta) and the associated Derivative with respect to Delta')
xlabel('Delta / PI')
ylabel('Magnitude')

l1 = sprintf('Rho^2(Delta) for n = %d dimensions',2*len);
l2 = sprintf('dRho^2/dDelta for n = %d dimensions',2*len);
legend(l1,l2)