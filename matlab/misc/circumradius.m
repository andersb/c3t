%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

function [radii,m,q] = circumradius(r,omega,res)

% Range of Delta
range = 0:res:2*pi;
num = numel(range);
count = 0;

radii = zeros(1,num);
q = radii;
m = radii;


for delta = range
    
    % Increase the counter
    count = count + 1;

    % Compute Rho^2(delta)
    ra = rho2(delta,r,omega);
    radii(count) = ra;%sqrt(ra);
    
    % Find minimum
    [~,ix] = min(radii);
    m(count) = range(ix);
    
    % Derivative of Rho^2 with respect to Delta
    d = qu(delta,r,omega);
    q(count) = d;
    
end % for

function ra = rho2(x,r,omega)

% Compute t1, t2, t3
t1 = t_1(x,r,omega);
t2 = t_2(r,omega);
t3 = t_3(x,r,omega);

% Compute Rho^2
num = t2 .* t1 .* t1;
denum = 4*(t2.*t1 - t3.*t3);
ra = num ./ denum;

function q = qu(x,r,omega)

t1 = t_1(x,r,omega);
t2 = t_2(r,omega);
t3 = t_3(x,r,omega);
t3p = t_3_prime(x,r,omega);
q = 2*t3^3 - t3*t1*(t2 + t3p);

function t = t_1(x,r,omega)

ox = omega .* x;
r2 = r.^2;
t = 2*sum(r2 .* (1 - cos(ox)));

function t = t_2(r,omega)

r2 = r.^2;
o2 = omega.^2;
t = sum(r2 .* o2);

function t = t_3(x,r,omega)

ox = omega .* x;
r2 = r.^2;
t = sum(r2 .* omega .* sin(ox));

function t = t_3_prime(x,r,omega)

ox = omega .* x;
r2 = r.^2;
o2 = omega.^2;
t = sum(r2 .* o2 .* cos(ox));


% EOF