%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
%

function sdr = calc_sdr(rms)
sdr = 10*log10(1/3 ./ rms);

% EOF