%
%    Capacity bound
%
%    Polyanskiy et al., "Channel Coding Rate in the Finite Blocklength Regime"
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% Euler's number
eu = exp(1);

% SNR
SNR = 1;
P = SNR;

% Usual Gaussian right-tail probability
epsilon = 1e-2;
Q = erfc(epsilon)/(2*sqrt(2));

% Channel dispersion for AWGN, Equation 293
V = P*(P+2)*log2(eu)^2/(2*(P+1)^2)

% Capacity
C = 0.5*log(1+SNR);

% Code rate
R = C - sqrt(V)/Q;

% EOF