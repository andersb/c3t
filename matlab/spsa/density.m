%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% Tube density
function d = density(n,L,g_rad)

% Numerator
numer = L*vol_const(n-1)*g_rad^(n-1);

% Denominator
denom = vol_const(n)*(1+g_rad)^n;

% Density
d = numer/denom;

% Constant used in volume calculation
function Cn = vol_const(n)
p = n/2;
Cn = pi^p / gamma(p+1);

% EOF