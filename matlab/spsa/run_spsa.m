%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul 
%    and Kumar Vijay Mishra 
%

clear
format long g

% Number of calls to SPSA
n_spsa_calls = 600;

% Number of iterations inside SPSA
n_spsa_iter = 600;

% Number of dimensions
n = 4;
half_n = n/2;

% Range of Delta
res = 1/1e4;
delta_rng = 0:res:2*pi;
%delta_rng = pi;

% Check if odd
d2 = 0;
num_r = half_n;
if rem(n,2)
    d2 = delta_rng .^ 2;
    num_r = ceil(half_n);
end % if

% Initialize the radii
r0 = ones(1,num_r);
r = r0 / norm(r0);
%r = .5;

%r = [0.8165 0.5774];
%r = [ 1 .5 ];

% SPSA parameters
A = 0.01;
B = 11;

% 2-2*cos(w*Delta)
w = 1:floor(half_n);
%w = [1 4 5];
cosod = 2-2*cos(w' .* delta_rng);

% w*sin(w*Delta)
sinod = sin(w' .* delta_rng) .* w';

% Initial density
d_max = 0;
rho_min = 0;

% Flags
plot_flag = 0;
save_flag = 1;

% Repeated calls to SPSA
for ix = 1:n_spsa_calls

    % SPSA
    [r,L,rho,ma] = spsa(A,B,w.^2,r,n,delta_rng,d2,cosod,sinod,n_spsa_iter,plot_flag,save_flag);

    % Density
    d = density(n,L,rho);
    if d > d_max

        fprintf('Dim: %d\tNum SPSA calls: %d\tRho: %.5f\tTube density: %e\tA = %.3f B = %.1f\n',n,ix,rho,d,A,B)
        d_max = d;
        rho_min = rho;
        r_max = r
        %A = A*.95;
        %B = B + 0.5;
        
    end % if
end % if

fprintf('Global circumradius:\t%.5f\n',rho_min)
fprintf('Tube density:\t\t%.5e\n',d_max)
%fprintf('Radii:\n')
r_max

% EOF
