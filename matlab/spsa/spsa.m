%
%    Simultaneous perturbation stochastic approximation (SPSA)
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
%    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul 
%    and Kumar Vijay Mishra 
%

function [r,L,g_rad,ma] = spsa(A,B,w2,r,n,d1,d2,cosod,sinod,n_iter,plot_flag,save_flag)

% Allocate for .mat file
tube_radii = zeros(length(d1),n_iter*2);

% Loop for the number of iterations
p = numel(r);
count = 0;
ma = inf;
for k = 1:n_iter
    
    % Increase the counter
    count = count + 1;
    if count == 0
        res = 1/1e4;
        rng = 0:res:2*pi;
        rng = rng/pi/2;
        plot(rng,tube_radii)
    end

    % Random perturbation vector
    per = randsrc(1,p);

    % Stochastic pertubation gradient estimator, y
    c = 0.01*(k+1)^-0.101;
    cp = c*per;
    cp2 = 2*cp;
    [j1,~,~,ma,rho_plus] = obj(r+cp,n,w2,cosod,sinod,d1,d2,plot_flag,ma);
    [j2,~,~,ma,rho_minus] = obj(r-cp,n,w2,cosod,sinod,d1,d2,plot_flag,ma);
    y = (j1-j2) ./ cp2;

    % Update r
    a = A*(k+B)^-0.602;
    r = r + a*y;

    % Normalize
    r = r / norm(r);

    % Save tube radii
    if save_flag
        % Save data
        tube_radii(:,2*k-1) = rho_plus;
        tube_radii(:,2*k) = rho_minus;
    end % if

    % Plot
    if plot_flag
        res = 1/1e4;
        rng = 0:res:2*pi;
        rng = rng/pi/2;
        figure(2)
        plot(rng,rho_plus)
        plot(rng,rho_minus)
    end % if

end % for

% Return the length and the global circumradius
[~,L,g_rad,ma] = obj(r,n,w2,cosod,sinod,d1,d2,plot_flag,ma);

% Save to .mat
if save_flag
    tube_radii_n4 = tube_radii;
    save tube_radii_n4.mat tube_radii_n4
end % if

%%
%%

% Objective function
function [J,L,g_rad,ma,rho2] = obj(r,n,w2,cosod,sinod,d1,d2,plot_flag,ma)

% r^2
r2 = r'.^2;

% Check if odd
b2 = 0;
if rem(n,2)
    % b^2
    b2 = r2(end);
    % Extract r2
    r2 = r2(1:end-1);
end % if

% t1, t2 and t3
t1 = sum(cosod .* r2, 1)+b2*d2;
t2 = w2*r2+b2;
t3 = sum(sinod .* r2, 1)+b2*d1;

% Length
L = 2*pi*sqrt(t2);

% Global circumradius
[g_rad,m,rho2] = rho(t1,t2,t3,plot_flag);

% Look for maximum tube radius
if m > ma
    r_ma = r
    ma = m
    d = density(n,L,g_rad);
    g_rad;
    % Rho = 0.902059
    % Density = 0.064758
    % r = 0.343124, 0.450007, 0.583064, 0.599591

end % if

% This is for n=3
%g_rad = rho3(r2,b2,d1,d2);

% J
J = L*g_rad^(n-1)/(1+g_rad)^n;

%%

% Global circumradius
function [circumradius,ma,rho2] = rho(t1,t2,t3,plot_flag)

% Denominator
denom = 4*(t1-t3.^2/t2);

% rho-squared
rho2 = t1.^2 ./ denom;

% Minimum global circumradius
c = numel(rho2)/2;
[mi,ix] = min(rho2(4:c));

% Maximum global circumradius
ma = inf;
%[ma,ix] = max(rho2(4:c));

% Global circumradius
circumradius = sqrt(mi);

%%

% Global circumradius
% This is for n=3
function circumradius = rho3(r2,b2,d1,d2)

% t1 and t2
t1 = b2*d2+2*r2*(1-cos(d1));
t2 = b2*d1+r2*sin(d1);

% Denominator
b2r2 = b2+r2;
denom = t1*b2r2-t2.^2;

% rho-squared
rho2 = t1.^2*b2r2/4 ./ denom;

% Plot
if plot_flag
    res = 1/1e4;
    rng = 0:res:pi;
    rng = rng/pi;
    figure(2)
    plot(rng,rho2)
end % if

% Minimum
[mi,ix] = min(rho2(5:end));

% Global circumradius
circumradius = sqrt(mi);

% EOF