%
%   Iteratively Re-weighted Least-Squares algorithm
%
%   Anders Buvarp
%
%   The Bradley Department of Electrical and Computer Engineering
%   Virginia Tech
%
% 
% Inputs:
%
%    b      Parameter for Huber M-estimator
%    w      Weights
%    z      Response variable
%    H      H-matrix
%    Rinv   Inverse of covariance matrix
%    N      Number of iterations
%
function x_hat = irls(b,w,z,H,Rinv,N)

% Number of residuals
m = length(z);

% Generate an initial x_hat
Q = 0.01*eye(m);
x_hat = inv(H' * Q * Rinv * H) * H' * Q * Rinv * z;

% Compute residuals
r = z - H * x_hat;

%figure(33),plot(x_hat.*H/pi*180),hold,grid,plot(r/pi*180),ylim([-100 100])

% Loop for 10 iterations
for iter = 1:N
    
    % Compute residuals
    r = z - H * x_hat;
    %plot(H*x_hat)


    % s is calculated at the first three iterations
    if iter <= 13   
        
        % Compute MAD
        s = mad(r);
        
        % s should be at least 10^-6
        if s < 1e-6
            s = 1e-6;
        end % if

    end % if
    
    % Huber M-estimator
    [Q,q] = huber(r,s,w,b);

    % Compute S matrix, "Hat Matrix"
    x_hat = inv(H' * Q * Rinv * H) * H' * Q * Rinv * z;
    %plot(x_hat.*H*180/pi)
    %plot(180*r/pi)

end % sx

% EOF