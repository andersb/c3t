%
% Huber M-estimator
%
% Anders Buvarp
% ECE 5714 
% Robust Estimation and Filtering
% 
% Inputs:
%
%    r      Residuals
%    s      Standarization parameter
%    w      Weights
%    b      Huber 'b' parameter
%
function [Q,q] = huber(r,s,w,b)

% Number of residuals
m = length(r);

% Standardize the residuals
r_N = r ./ (s*w);

% q(r/s) is 1 for the interval +/-b
q = ones(1,m);

% q() is b/r for r > b
ix = find(r_N > b);
q(ix) = b ./ r_N(ix);

% q() is -b/r for r < -b
ix = find(r_N < -b);
q(ix) = -b ./ r_N(ix);

% Return the Q-matrix
Q = diag(q);

% EOF