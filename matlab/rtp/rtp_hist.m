%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

clear
format long g

% Flags
plot_flag = 0;

% SNR [dB]
snr = 0;

% Radii
r8 = [0.5125, 0.5162, 0.5551, 0.4035];
r20 = [0.35251032, 0.36757906, 0.37378317, 0.3705179, 0.36658535,0.34507531, 0.30497737, 0.24626995, 0.19969124, 0.12894291];
r = r20;
%r = r8;
n2 = numel(r);
n4 = n2/2;
n = 2*n2;
rng = 1:n2;
inv_rng = n2 ./ rng';

% Compute noise power
lin_snr_inv = 10^(-snr/10);
noise_pwr = r.^2 * lin_snr_inv;
std = sqrt(noise_pwr/2);
std_dev = repmat(std',1,2);
awgn = std_dev.*randn(n/2,2);

% Tx
src = pi/3;
%src = 0;
fprintf('Tx: %.2f degrees\n\n',src*180/pi)

% Map
s = src/n2;
x = [];
for w = rng
    x = [ x; r(w)*[cos(w*s) sin(w*s)] ];
end % for
y = x + awgn;
z = x;

% Map to the frequency domain
cp_len = 0;
[x,c,freq,td,r,Fs] = c3t_tx(src,n,cp_len,plot_flag);
fft_size = numel(freq);
xlen = numel(td);
pwr = td'*td/xlen;
lin_snr_inv = 10^(-snr/10);
noise_pwr = pwr * lin_snr_inv;
std = sqrt(noise_pwr/2);

% Number of angles
count = 400;

% Monte-carlo loop
m = 10000;
%m = 1;
hat = zeros(n,m);
for mx = 1:m

    % AWGN
    awgn = std.*randn(xlen,2);
    y = td + awgn(:,1) + 1j*awgn(:,2);

    if plot_flag
        re = real(fftshift(fft(y)));
        im = imag(fftshift(fft(y)));
        %re = real(fftshift(fft(td)));
        %im = imag(fftshift(fft(td)));
        
        figure(7)
        ix = -xlen/2:xlen/2-1;
        subplot(211),plot(ix,re),grid
        ylabel('Real\{CV-C3T\}')
        legend('n=20')
        subplot(212),plot(ix,im),grid
        xlabel('Frequency [bin]')
        ylabel('Imag\{CV-C3T\}')
        legend('n=20')
    end % plot_flag

    % Extract C3T from frequency domain
    re = real(fft(y));
    im = imag(fft(y));
    ix = 1:n4;
    offset = 2;         % Special from DC offset
    jx = ix+offset;     
    y = zeros(n2,2);
    y(ix,1) = re(jx);   % Positive frequency, real part
    y(ix,2) = im(jx);   % Positive frequency, imag part

    jx = ix + fft_size - n4 - offset;
    ix = ix + n4;
    y(ix,1) = re(jx);   % Negative frequency, real part
    y(ix,2) = im(jx);   % Negative frequency, imag part

    if plot_flag
        figure(22)
        subplot(211)
        plot(z(:,1)), grid, hold
        plot(y(:,1)), hold
        xlabel('x(\alpha), Odd Elements')
        ylabel('r_i \times cos(\omega_i\alpha)')      
        legend('Transmit','Receive')
        subplot(212)
        plot(z(:,2)), grid, hold
        plot(y(:,2)), hold
        xlabel('x(\alpha), Even Elements')
        ylabel('r_i \times sin(\omega_i\alpha)')
        legend('Transmit','Receive')
    end % plot_flag

    % Dec
    no = vecnorm(y,2,2);
    ve = y ./  repmat(no,1,2);
    re = ve(:,1);
    im = ve(:,2);
    ac = acos(re);
    as = asin(im);

    % Check for 2nd quadrant
    kx = find(re < 0); 
    jx = find(im >= 0);
    ix = intersect(kx,jx);
    as(ix) = pi-as(ix);

    % Check for 3rd quadrant
    kx = find(re < 0); 
    jx = find(im < 0);
    ix = intersect(kx,jx);
    ac(ix) = -ac(ix);
    as(ix) = -pi-as(ix);

    % Check for 4th quadrant
    kx = find(re >= 0);
    jx = find(im < 0);
    ix = intersect(kx,jx);
    ac(ix) = -ac(ix);

    % Scale
    ac = ac.* inv_rng;
    as = as.* inv_rng;

    % Store
    hat(:,mx) = [ac; as];

    % Plot
    if plot_flag
        err = [ac; as] - src
        rms = sqrt(err'*err)
        plot(err/pi,'+-'),grid
        %plot([ac; as])
        ylim([-1 1])
    end % if

end % for

h = reshape(hat,1,numel(hat));
%hx = find(hat > pi); h(hx) = pi;
%hx = find(hat < -pi); h(hx) = -pi;
deg = h*180/pi;

figure(99)
histogram(deg,150),grid
str = sprintf('Histogram of extracted C3T angles, SNR = %d dB, n=%d',snr,n);
%title(str)
ylabel('Count')
xlabel('Extracted C3T angles [degrees]')
str = sprintf('Source = %d degrees',int32(src*180/pi));
legend(str)

% RMS error
rmse = sqrt(mean((src-h).^2));
fprintf('\nMean Hat: %.2f degrees',mean(deg))
fprintf('\nVariance Hat: %.2f',var(h))
fprintf('\nMedian Hat: %.2f',median(deg))
fprintf('\nRMS Error: %.5f',rmse)
fprintf('\n')

if 0
% Noise variance corresponding to SNR and radii
w = 1.18;
w = 3.23;
%w = .25;

% ACF and autocorrelation matrix
acf = autocorr(h);
Rxx = toeplitz(acf);
Rww = diag(repmat(w,1,n));
Rss = repmat(1/3,1,n);

% Wiener
s = Rss*inv(Rxx)*h';
s_hat = median(s/n);
w_err = src-s_hat;

figure(88)
plot(s),grid,hold
plot(h),hold
legend('Wiener','Rx')
end % if 

% EOF