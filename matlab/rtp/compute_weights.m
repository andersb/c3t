%
% Compute weights for robustified Huber
%
% Anders Buvarp
% ECE 5714 
% Robust Estimation and Filtering
% 
% Inputs:
%
%       n       Dimension of the code
%
% Outputs:
%
%       w       Weights
%       H       Design space
%
function [w,H] = compute_weights(n)

% Half the dimension
n2 = n/2;
rng = 1:n2;

% Design space part of the data points
h = rng / n2;
H = zeros(n,1);
H(1:2:end) = h;
H(2:2:end) = h;

% Number of data points
N = length(H);

% Compute the median
m = median(H);
d = H-m;

% Compute the numerator
a = abs(d);

% Compute the denominator
s = mad(d);

% Compute the robust distance, RD
RD = a ./ s;
thresh = (3 ./ RD).^2;

% Return the weights
w = min(ones(N,1),thresh);

% EOF