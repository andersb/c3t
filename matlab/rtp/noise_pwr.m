%
% Compute the noise power standard deviation
%
% Anders Buvarp
%
% 
% Inputs:
%
%       r       The radii
%
function [std_dev,Rinv] = noise_pwr(r,snr)

% Compute noise power
lin_snr = 10^(snr/10);
noise_pwr = r.^2 / lin_snr;
std = sqrt(noise_pwr/2);
std_dev = repmat(std',1,2);

% Covariance matrix
n2 = numel(r);
n = 2*n2;
Rinv = inv(eye(n2).*std_dev(:,1).^2);
%v = [0.850153087652699,0.209948744622618,0.092626143899162,0.0534399020938971];
%Rinv = inv(eye(n2)) .* v;
Rinv = inv(eye(n2));
Rinv = inv(eye(n));

% EOF