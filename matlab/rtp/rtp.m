%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%
% Inputs:
%
%    src        Source angle
%    n          C3T dimension
%    snr        Signal-to-noise ratio
%    b_huber    Huber b-parameter
%    pflag      Plot flag
%    n_iter     Number of Monte-Carlo iterations
%
%   [x_hub,x_arc] = rtp(src,n,snr,b_huber,n_iter,pflag)
% 

function [x_hub,x_arc] = rtp(src,n,snr,b_huber,n_iter,pflag)

% The source angle
fprintf('Tx: %.2f degrees\n\n',src*180/pi)

% Tx vector
[x,~,~,~,r,~] = c3t_tx(src,n,0,pflag);

% Compute the noise power
[std_dev,Rinv] = noise_pwr(r,snr);

% Compute weights, we
[we,H] = compute_weights(n);

% Inv range
n2 = n/2;
rng = 1:n2;
inv_rng = n2 ./ rng';

% Monte-carlo loop
hat = zeros(n,n_iter);
x_h = zeros(1,n_iter);
z = zeros(n,1);
for mx = 1:n_iter

    % AWGN
    gn = std_dev .* randn(n/2,2);
    y = x + gn;
    %y=x;
    
    % Dec
    no = vecnorm(y,2,2);
    ve = y ./  repmat(no,1,2);
    re = ve(:,1);
    im = ve(:,2);
    ac = acos(re);
    as = asin(im);

    % Check for 2nd quadrant
    kx = find(re < 0); 
    jx = find(im >= 0);
    ix = intersect(kx,jx);
    as(ix) = pi-as(ix);

    % Check for 3rd quadrant
    kx = find(re < 0); 
    jx = find(im < 0);
    ix = intersect(kx,jx);
    ac(ix) = -ac(ix);
    as(ix) = -pi-as(ix);

    % Check for 4th quadrant
    kx = find(re >= 0);
    jx = find(im < 0);
    ix = intersect(kx,jx);
    ac(ix) = -ac(ix);

    % Scale
    ac_extra = ac.* inv_rng;
    as_extra = as.* inv_rng;

    % Store the extracted angles
    hat(:,mx) = [ac_extra; as_extra];

    % Plot
    if pflag
        figure('color','w');
        err = hat(:,mx) - src;
        rms = sqrt(err'*err);
        plot(err/pi,'+-'),grid
        %plot([ac; as])
        ylim([-1 1])
    end % if

    % Check for M-estimator
    if b_huber

        % Save for Huber
        z(1:2:end) = ac;
        z(2:2:end) = as;
    
        % IRLS
        n_iter = 50;
        %H = ones(n,1);
        %z = [ac; as];
        x_hat = irls(b_huber,we,z,H,Rinv,n_iter);
    
        % Store x_hat
        x_h(mx) = x_hat;
    
        % OLS
        %za = atan2(y(:,2),y(:,1));
        %x_hat = inv(H' * Rinv * H) * H' * Rinv * z;
        
    end % if
end % for

h = reshape(hat,1,numel(hat));
%hx = find(hat > pi); h(hx) = pi;
%hx = find(hat < -pi); h(hx) = -pi;
fprintf('SNR: %d   Mean Hat: %.2f degrees   Median Hat: %.2f degrees\n',snr,mean(h)*180/pi,median(h)*180/pi)

if pflag
    figure(33)
    h1 = histogram(h*180/pi,150,'FaceColor','#4DBEEE','EdgeColor','#4DBEEE');
    grid,hold
    h2 = histogram(x_h*180/pi,50,'FaceColor','#A2142F','EdgeColor','#A2142F');
    hold
    %title('Histogram of Recovered Sources for C3T, n=8, SNR = 0 dB')
    ylabel('Count')
    xlabel('Angles [degrees]')
    legend('Extracted raw angles','After Huber processing')
end % if

% Check for M-estimator
if ~b_huber
    x_h = 0;
end % if

% RMS error
rmse = sqrt(mean((src-h).^2));
rmse_robust = sqrt(mean((src-x_h).^2));
fprintf('Robust RMS Error: %.5f   RMS Error: %.5f\n\n',rmse_robust,rmse)

% Return estimates
x_hub = median(x_h);
x_arc = median(h);
fprintf('Robust Error: %.3e   Arc Error: %.3e\n\n',x_hub-src,x_arc-src)

% EOF
