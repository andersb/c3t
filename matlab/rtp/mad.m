%
% MAD function
%
% Median absolute deviation
%
% Anders Buvarp
% ECE 5714 
% Robust Estimation and Filtering
% 
% Inputs:
%
%    r      Residuals
%
function s = mad(r)

% Compute absolute of residuals
a = abs(r);

% Take median
m = median(a);

% Return the MAD
s = 1.4826 * m;

% EOF