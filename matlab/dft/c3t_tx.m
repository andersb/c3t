%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% Compute the C3T transmit vector
function [x,c,freq,td,r,Fs] = c3t_tx(src,n,cp_len,pflag)

% Radii
switch n
    case 8
        r = [0.5125, 0.5162, 0.5551, 0.4035];
        fft_size = 32*8;
    case 20
        r = [0.35251032, 0.36757906, 0.37378317, 0.3705179, 0.36658535,...
             0.34507531, 0.30497737, 0.24626995, 0.19969124, 0.12894291];
        fft_size = 256;
    case 40
        r = [0.23258918, 0.21673772, 0.25316337, 0.23811528, 0.26905355,...
             0.25429533, 0.27286513, 0.26387771, 0.26075637, 0.25428426,...
             0.25006161, 0.23869478, 0.23285036, 0.22070924, 0.19165789,...
             0.18310335, 0.17028559, 0.15342232, 0.09811064, 0.09736838];    
        fft_size = 256;
    case 100
        r = [0.12382308, 0.10847692, 0.15717168, 0.17994176, 0.06367002,...
             0.01645952, 0.17460105, 0.13555256, 0.12844574, 0.11499835,...
             0.11685306, 0.19139462, 0.20346245, 0.12069767, 0.20684936,...
             0.05684767, 0.20472595, 0.03709949, 0.10415552, 0.08590632,... 
             0.22643255, 0.21105967, 0.19915536, 0.16115404, 0.03118854,... 
             0.18561472, 0.12057285, 0.05374938, 0.17488107, 0.17700132,...
             0.15179817, 0.20157967, 0.13781863, 0.0384858,  0.14233394,...
             0.1544325,  0.12247276, 0.06843885, 0.15669923, 0.16760792,...
             0.17626555, 0.17180191, 0.06182777, 0.10257803, 0.08054303,... 
             0.13447065, 0.1068071,  0.12692424, 0.11186648, 0.05276959];    
        fft_size = 256;
    otherwise
        fprintf("%d dimension not supported\n",n)
end % switch

% Scale the source to +/-PI
n2 = n/2; 
s = src/n2;

% Generate real/complex transmit vector
rng = 1:n2;
x = [];
c = [];
for w = rng
    p = r(w)*[cos(w*s) sin(w*s)];
    x = [ x; p ];
    c = [ c; p(1)+1j*p(2) ];
end % for

% Apply complex vector in frequency domain
freq = zeros(1,fft_size);

% Index to positive frequency
n4 = n2/2;
px = 1:n4;
px = px + 2; % Skip DC

% Index to negative frequency
nx = px + fft_size - px(end) - 2;

% Map tx vector to frequency domain
ix = [px nx];
%c(2:end) = 0; % One tone
%c(end) = 0;
freq(ix) = c;

% Modulate using DFT
td = ifft(freq).';

% Cyclic prefix
if cp_len
    start = (1-cp_len)*fft_size+1;
    td = [zeros(1920,1);td(start:end);td];
    %td = [td;td];
end % if

% Sample rate
sc_spacing = 15e3; % Hz
Fs = fft_size * sc_spacing;

% Plot
if pflag
    figure(22)
    subplot(211)
    plot(real(td),'b'),grid
    subplot(212)
    plot(imag(td),'r'),grid
end % if

% EOF