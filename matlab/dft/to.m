%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% Find the fractional frequency offset
function offset = to(rx,Fs)

angles = atan2(imag(rx),real(rx));
e = unwrap(angles);

n = numel(rx);
nx = 0:n-1;
p = polyfit(nx,e,1);
delta = p(1)*n;

offset = sign(delta)*mod(delta,2*pi)/2/pi;

% EOF