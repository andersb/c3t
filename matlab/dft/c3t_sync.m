%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

clear
format long g

% Config
plot_flag = 1;          % Plot flag
n = 8;                  % Dimension
snr = 10;               % SNR
cp_len = 1/4;           % Cyclic prefix length as fraction of FFT size
freq_offset = 1/3;      % Frequency offset [fraction of the s.c. spacing]
%freq_offset = 0;

N = 1000;               % filter length - must be odd
f = [0 0.016 0.020 1];  % band edges
a = [1  1   0   0 ];    % desired band values
b = cfirpm(N,f,a);      % Parks-McClellan optimal FIR filter design
%figure
%[h,w] = freqz(b,1,512);
%figure(5)
%plot(f,a,w/pi,abs(h)),grid

%h = cfirpm(N-1,[-1 -.11 -.08 .08 .11 1],@lowpass);

% Filter design
%Hlp = design(fdesign.nyquist(64), SystemObject=true);     % Lowpass prototype
%lp_fir = dsp.FIRFilter('Numerator',Hlp.Numerator);

%N = length(Hlp.Numerator)-1;
%Fc = 1/16;                              % Desired frequency shift
%Hbp = clone(Hlp);
%Hbp.Numerator = Hbp.Numerator.*exp(1j*Fc*pi*(0:N));
%Hbp2 = clone(Hlp);
%Hbp2.Numerator = Hbp2.Numerator.*exp(1j*2*Fc*pi*(0:N));
%hfvt = fvtool(Hlp,Color='white');
%legend(hfvt,'Complex Bandpass','Complex Bandpass2',Location='NorthWest')

% Tx vector
src = pi/3;
[x,c,freq,td,r,Fs] = c3t_tx(src,n,cp_len,plot_flag);
fprintf('Tx: %.2f degrees\n\n',src*180/pi)

% Add frequency offset
M = numel(td);
t = 0:M-1;
rx = td .* exp(2j*pi*freq_offset*t'/M);

% Add time offset
if 0
    %td = circshift(td,-1);
    td = [ td(end) td(1:end-1)];
    freq = fft(td);
    
    iq = freq(ix);
    iq-c.';
end % if

%a = Hbp([td; zeros(N,1)]);
%b = a.*exp(1j*-Fc*pi*(0:47));
%b = a(17:end);
%a = filter(Hbp.Numerator,1,td);
g = filter(b,1,[rx; zeros(N,1)]);
%g = lp_fir([rx; zeros(N,1)]);

% Plot
ix = 500:750;
s = g(ix);
if plot_flag
    figure(28)
    %subplot(211)
    plot(real(s),'b'),grid,hold
    %subplot(212)
    plot(imag(s),'r'),hold
    title('After filtering')
end % if

% Find the fractional frequency offset
foffset = ffo(rx,Fs);
ferr = foffset - freq_offset

% Find the time offset
toffset = to(rx,Fs);

% EOF