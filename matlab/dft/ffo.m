%
%    Constant Curvature Curve Tube Codes for Analog Error Correction
%
%    Anders Buvarp
%
%    The Bradley Department of Electrical and Computer Engineering
%    Virginia Tech
%

% Find the fractional frequency offset
function offset = ffo(rx,Fs)

angles = atan2(imag(rx),real(rx));
e = unwrap(angles);
%figure(44),plot(e),grid

n = numel(rx);
nx = 0:n-1;
p = polyfit(nx,e,1);
delta = p(1)*n;
rng = 2*pi;
offset = delta/rng-1;

fft_size = 4/5*n;
cp_len = n/5;
cp_len = 256/4; 
fft_size = 256;
M = numel(rx);
rx = rx(1921:end);
corr = sum(rx(1:cp_len).*conj(rx(fft_size+1:end)));
offset = -atan2(imag(corr),real(corr))/(2*pi*fft_size/M);

% EOF