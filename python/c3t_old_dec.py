"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and Vijay Mishra 
"""

import sys
from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim

class Net(nn.Module):
    def __init__(self, n_feature, n_hidden, n_output, batch_size, device):
	
        super(Net, self).__init__()

        self.fc1 = nn.Linear(n_feature, n_hidden)
        self.fc2 = nn.Linear(n_hidden, n_hidden*2)
        self.fc3 = nn.Linear(n_hidden*2, n_hidden*4)
        self.fc4 = nn.Linear(n_hidden*4, n_hidden*2)
        self.fc5 = nn.Linear(n_hidden*2, n_hidden)
        #self.fc6 = nn.Linear(n_hidden, n_hidden)
        #self.fc7 = nn.Linear(n_hidden, n_hidden)
        #self.fc8 = nn.Linear(n_hidden, n_hidden)
        self.fcOut = nn.Linear(n_hidden, n_output)
        
        # The processing device; CPU or GPU
        self.device = device
        self.batch_size = batch_size
		
    def plot(self,x,y,z,ylabel,zlabel,snr_min,snr_max):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange", label=ylabel)
        plt.scatter(x, z, color = "red", label=zlabel)
        plt.title('Alpha vs Alpha Hat    SNR Range: %d dB to %d dB' % (snr_min,snr_max))
        plt.xlabel('Alpha')
        plt.ylabel('Alpha Hat')
        plt.grid(True)
        plt.legend()
        plt.savefig("regr.png")

    def forward(self, x):
        # Shape should be (N,order)
        x = self.fc1(x)
        x = torch.tanh(x)       # Hyperbolic tangent for activation function
        x = self.fc2(x)
        x = torch.tanh(x)
        x = self.fc3(x)
        x = torch.tanh(x)
        x = self.fc4(x)
        x = torch.tanh(x)
        x = self.fc5(x)
        x = torch.tanh(x)
        #x = self.fc6(x)
        #x = torch.tanh(x)
        #x = self.fc7(x)
        #x = torch.tanh(x)
        #x = self.fc8(x)
        #x = torch.tanh(x)
        x = self.fcOut(x)
        #x /= np.pi
        return x

    def train(self, trainloader, epochs, lr, decay):
        
        # Use Mean Square Error for loss function
        criterion = nn.MSELoss()
        
        # Use the Adam optimizer
        optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)

        # Number of examples in trainloader
        num = len(trainloader)
        print('Training with %d examples for %d epochs' % (num,epochs))
 
        # For each epoch
        loss_threshold = 0.1
        loss_counter = 0;
        for epoch in range(epochs):
            running_loss = 0.0
            sys.stdout.flush()
            for count, data in enumerate(trainloader,0):
                
                # Reset optimizer
                optimizer.zero_grad()
                
                # Extract data from trainloader
                inp, lbl = data
                
                # Forward propagation
                # Shape network input at (N,order)
                outp = self(inp)
                outp = outp.view(-1,1)

                # Backward propagation
                loss = criterion(outp, lbl)
                loss.backward()
                optimizer.step()
                running_loss += loss.item()

            # Compute average loss
            ave_loss = running_loss / num
            print('Epoch: %d   Loss: %.3e   Count: %d' % (epoch,ave_loss,num))

            # If first epoch, set the initial loss
            if epoch == 0:
                prev_loss = ave_loss

            # Update loss counter
            loss_counter += 1
            print('loss_counter: %d' % loss_counter)

            # Average change in loss
            a = (prev_loss - ave_loss) / loss_counter
            print('Average change in loss: %e' % a)
            ar = a / ave_loss
            print('Relative change in loss: %e' % ar)

            # Check if we should lower he learning rate
            if loss_counter >= 8 and ar < loss_threshold:
                #loss_threshold /= 2.0
                #print('Loss Threshold: %f' % loss_threshold)
                lr /= 2.0
                print('Learning Rate: %e' % lr)
                optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)        
                prev_loss = ave_loss
                loss_counter = 0

        print('Finished Training')

    def sdr(self,v,mse):
        sdr = 10*np.log10(v/mse)
        return sdr
    
    def accuracy(self, testloader, signal_variance, snr_min, snr_max, plot_flag=False):

        # Sum of squared error
        sqe = 0
        sum_err = 0
        
        with torch.no_grad():
            for data in testloader:

                # Extract data from testloader
                inp, lbl = data

                # Forward propagation
                # Shape network input at (N,order)
                outp = self(inp)
                
                # Compute MSE
                err = torch.abs(outp - lbl).item()
                sum_err += err
                sqe += err**2
                
        total = len(testloader)
        acc = 1 - sum_err / total
        mse = sqe / total
        sdr = self.sdr(signal_variance,mse)
        return acc,mse,sdr
      
# EOF
