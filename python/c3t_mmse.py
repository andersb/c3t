#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and
    Vijay Mishra 
"""

from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
import numpy.linalg as linalg
from c3t_enc import c3t_enc
from c3t_radii import get_radii

def gen_lut(r,alphas):
    
    # Get the number of alpha values
    num_alphas = len(alphas)
    
    # Modulation order
    num_r = len(r)
    mod_order = 2*num_r
    
    # Allocate the look-up table
    lut = np.empty((mod_order,num_alphas))
    
    # Loop for each cos/sin pair
    for ix in range(num_r):
        
        # Compute omega * alphas
        omega = ix + 1
        oa = omega*alphas
        
        # Compute possibly transmitted values
        kx = ix*2
        lut[kx,:] = r[ix] * np.cos(oa)
        lut[kx+1,:] = r[ix] * np.sin(oa)
            
    # Return the look-up table
    return lut

def mmse_dec(lut,alphas,symb):
    
    # Compute the difference between received vector and the look-up table entries
    symb = torch.flatten(symb, start_dim=1)
    delta = lut - symb.T
    
    # Compute the L2-norms
    l2 = linalg.norm(delta, axis=0)

    # Find index to smallest L2-norm
    ix = np.argmin(l2)
        
    # Find alpha_hat
    alpha_hat = alphas[ix]

    #print(delta[:,:4])
    #print(l2[:4])

    #print(ix)
    #print(alpha_hat)
    
    #plt.plot(alphas,l2)
    #plt.plot(alphas,'r')
    #plt.show()
    #exit()
    
    # Return decoded alpha_values after de-stretch
    return alpha_hat
 

def accuracy(lut,alphas,mag,testloader,variance_s):

    # Sum of squared error
    sqe = 0
    sum_err = 0
    
    # Loop for dataset
    for data in testloader:

        # Extract data from testloader
        inp, lbl = data
        
        # The source
        src = lbl.float()
        #print(src)

        # MMSE Decoder
        dec = mmse_dec(lut,alphas,inp) / mag

        # Compute MSE
        err = abs(dec-src)
        sqerr = err**2
        
        #print(sqerr)
        #print(dec)
        #exit()
        #err = torch.abs().item()
        
        # Accumulate errors
        sum_err += err
        sqe += sqerr

    # Total number of symbols        
    total = len(testloader)    
    print('total:',total)
    acc = 1 - sum_err / total
    mse = sqe / total
    sdr = 10.0*np.log10(variance_s / mse)
    return acc,mse,sdr.item()

    
"""
##############################################################################

    Main entry point for the C3T codes Monte-Carlo simulation

##############################################################################
"""

# Plotting flag
plot_flag = True
plot_flag = False

# Device
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print('Device:', device)

# Load radii
r2, r4, r6, r8, r10, r12, r20, r40, r100 = get_radii()

# Number of examples
num_ex_per_snr = 10000

# C3T Encoder parameters
beta = 0.9
stretch = np.pi

# The range of the source
num_alphas = 2000
mag = np.pi*beta
alphas = np.linspace(-mag,+mag,num_alphas)

# SNR range
snr_min = -10
snr_max = +10
snr_rng = np.arange(snr_min,snr_max+1)
print('SNR Range [dB]: [%d,%d]' % (snr_min,snr_max))

# Specify what radii to use
r_values = [r8]
r_values = [r2,r4,r6,r8,r20,r40,r100]

for radii in r_values:
    
    # Modulation order
    mod_order = 2*len(radii)
    print('Modulation Order:\t n =', mod_order)
    
    # Create the decoder look-up table
    lut = torch.tensor(gen_lut(radii,alphas))
    

    # Create the C3T encoder
    batch_size = 1
    enc = c3t_enc('Raw', mod_order, radii, beta, stretch, batch_size, device)
    
    # Compute SDR, MSE and Accuracy for a range of snr SNR levels
    sdr_list = []
    for snr in snr_rng:
        
        # get_data() expects a range
        srng = np.arange(snr,snr+1)

        # Generate symbols at a particular SNR value
        test_loader, signal_variance = enc.get_data(num_ex_per_snr, srng, False)
        
        # Measure performance of the MMSE decoder
        acc,mse,sdr = accuracy(lut,alphas,mag,test_loader,signal_variance)
        
        # Save SDR to list
        sdr_list.append(sdr)
        print('SNR: %d \t SDR: %.5f \t MSE: %.5f \t Accuracy: %0.2f percent' % (snr,sdr,mse,acc*100.0))

    print('res:',sdr_list)
    
    # Plot accuracy
    leg = 'n = %d' % mod_order
    plt.plot(snr_rng, sdr_list, color = "orange", label=leg)
    plt.xlim(snr_min-1,snr_max+1)
    plt.ylim(min(sdr_list)-1,max(sdr_list)+1)
    plt.grid(True)
    plt.title('Signal-to-Distorsion Rato vs SNR')
    plt.xlabel('SNR [dB]')
    plt.ylabel('SDR [dB]')
    plt.legend()
    fname = 'plots/c3t_sdr_vs_snr_n%d_MMSE.png' % mod_order
    plt.savefig(fname)
    if plot_flag == True:
        plt.show()
    
    # Save accuracy to file
    fname = 'accuracy/c3t_sdr_vs_snr_n%d_MMSE.txt' % mod_order
    np.savetxt(fname,np.asarray(sdr_list), delimiter=',')
    
# EOF
