"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
"""

import sys
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from cplxmodule import cplx
from cplxmodule.nn import CplxLinear
from cplxmodule.nn import CplxModReLU
from cplxmodule.nn import CplxSequential
from cplxmodule.nn import RealToCplx, CplxToReal
from cplxmodule.nn import CplxBatchNorm1d

def CplxTanh(x):
    x.real.copy_(torch.tanh(x.real))
    x.imag.copy_(torch.tanh(x.imag))
    return x

def CplxAsinh(x):
    z = x.real+x.imag*1j
    x = torch.arcsinh(z)
    return x

class CplxNet(CplxSequential):
    def __init__(self, n_feature, n_hidden, n_output, batch_size, act_fn, device):
	
        super(CplxNet, self).__init__()

        self.fc1 = CplxLinear(int(n_feature/2), n_hidden)
        self.fc2 = CplxLinear(n_hidden, n_hidden)
        self.fc3 = CplxLinear(n_hidden, n_hidden)
        self.fc4 = CplxLinear(n_hidden, n_hidden)
        self.fc5 = CplxLinear(n_hidden, n_hidden)
        self.fcCplxOut = CplxLinear(n_hidden, n_output)     # Output one complex
        self.fcOut = nn.Linear(2*n_output, n_output)        # Two real-valued numbers to a single real output

        # Batch normalization
        self.bn256  = CplxBatchNorm1d(n_hidden)
        self.bn512  = CplxBatchNorm1d(2*n_hidden)
        self.bn1024  = CplxBatchNorm1d(4*n_hidden)
        
        # Batch size
        self.batch_size = batch_size
        
        # Activation function
        if act_fn == 'zReLU':
            self.act_fn = CplxModReLU
        elif act_fn == 'modReLU':
            self.act_fn = CplxModReLU
        elif act_fn == 'ReLU':
            self.act_fn = CplxModReLU
        elif act_fn == 'asinh':
            self.act_fn = CplxAsinh
        elif act_fn == 'tanh':
            self.act_fn = CplxTanh
        elif act_fn == 'hardtanh':
            self.act_fn = CplxModReLU
        else:
            self.act_fn = CplxModReLU

        # The processing device; CPU or GPU
        self.device = device
        print('----------------------')
        print('Created CV C3T decoder')
        print('----------------------')
		
    def forward(self, x):

        # Convert n-dimensional input to n/2-dimensional complex-valued input
        x = RealToCplx()(x)

        x = self.fc1(x)
        x = self.act_fn(x)
        #x = self.bn256(x)

        x = self.fc2(x)
        x = self.act_fn(x)
        #x = self.bn512(x)

        x = self.fc3(x)
        x = self.act_fn(x)
        #x = self.bn1024(x)

        x = self.fc4(x)
        x = self.act_fn(x)
        #x = self.bn512(x)

        x = self.fc5(x)
        x = self.act_fn(x)
        #x = self.bn256(x)
        
        # Produce one complex-valued number
        x = self.fcCplxOut(x)
        
        # Magnitude/phase as output
        #mag = torch.sqrt(x.imag*x.imag + x.real*x.real)
        #angle = torch.atan2(x.imag,x.real)
        #x = torch.cat((mag, angle), dim=1)

        # Real/Imag as output
        x = torch.cat((x.imag,x.real), dim=1)
        
        # Linear layer as output layer
        x = self.fcOut(x)
        return x 
      
# EOF
