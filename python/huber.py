"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

from sys import exit
import torch
import numpy as np
import numpy.random as rnd
import numpy.linalg as linalg

def mad(r):
	a = np.abs(r)
	m = np.median(a)
	return 1.4826*m

def weights(n):

	n2 = n // 2
	rng = np.arange(n2)
	rng += 1
	
	# Design space
	H = rng / n2
	
	# Robust distance
	m = np.median(H)
	num = np.abs(H-m)
	denom = mad(H-m)
	RD = num / denom
	
	# Weights
	w = np.minimum(np.ones(n2),np.square((3.0/RD)))
	
	# Rinv
	Rinv = np.identity(n2)
	
	return w,H,Rinv

def huber(m,r,s,w,b):
	
	# q(r/s) is 1 for the interval +/-b
	q = np.ones(m)
	
	# Standardize the residuals
	r_N = r / (s*w);
	
	# q() is b/r for r > b
	ix = np.argwhere(r_N > b)
	q[ix] = b / r_N[ix]
	
	# q() is -b/r for r < -b
	ix = np.argwhere(r_N < -b)
	q[ix] = -b / r_N[ix]
	
	# Return Q matrix
	Q = np.diag(q)
	return Q

def irls(b,m,w,z,H,Rinv,n_iter):
	
	# Q matrix
	Q = 0.01*np.identity(m)
	QRinv = np.matmul(Q,Rinv);
	QRinv_z = np.matmul(QRinv,z);
	
	# Initial hat matrix
	a = np.matmul(Rinv,H)
	a = np.matmul(Q,a)
	a = np.dot(H,a)
	x_hat = np.dot(H,QRinv_z) / a
	
	# Loop for iterations
	for ix in range(n_iter): 
		
		# Compute residuals
		r = z - H * x_hat
		
		# s is calculated for the first 13 iterations
		if ix < 13:
			
			# MAD
			s = mad(r)
			
			# At least 1e-6
			s = np.maximum(s,1e-6)
			
		# M-estimator
		Q = huber(m,r,s,w,b)
		QRinv = np.matmul(Q,Rinv);
		QRinv_z = np.matmul(QRinv,z);
	
		# Initial hat matrix
		a = np.matmul(Rinv,H)
		a = np.matmul(Q,a)
		a = np.dot(H,a)
		x_hat = np.dot(H,QRinv_z) / a

	return x_hat
