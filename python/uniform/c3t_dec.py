"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and Vijay Mishra 
"""

from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim

class Net(nn.Module):
    def __init__(self, n_feature, n_hidden, n_output, device):
	
        super(Net, self).__init__()

        self.fc1 = nn.Linear(n_feature, n_hidden)
        self.fc2 = nn.Linear(n_hidden, n_hidden)
        self.fc3 = nn.Linear(n_hidden, n_hidden)
        #self.fc4 = nn.Linear(n_hidden, n_hidden)
        #self.fc5 = nn.Linear(n_hidden, n_hidden)
        #self.fc6 = nn.Linear(n_hidden, n_hidden)
        #self.fc7 = nn.Linear(n_hidden, n_hidden)
        #self.fc8 = nn.Linear(n_hidden, n_hidden)
        self.fcOut = nn.Linear(n_hidden, n_output)
        
        # The processing device; CPU or GPU
        self.device = device
		
    def plot(self,x,y,z,ylabel,zlabel,snr_min,snr_max):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange", label=ylabel)
        plt.scatter(x, z, color = "red", label=zlabel)
        plt.title('Alpha vs Alpha Hat    SNR Range: %d dB to %d dB' % (snr_min,snr_max))
        plt.xlabel('Alpha')
        plt.ylabel('Alpha Hat')
        plt.grid(True)
        plt.legend()
        plt.savefig("regr.png")

    def forward(self, x):
        # Shape should be (N,order)
        x = self.fc1(x)
        x = torch.tanh(x)       # Hyperbolic tangent for activation function
        x = self.fc2(x)
        x = torch.tanh(x)
        x = self.fc3(x)
        x = torch.tanh(x)
        #x = self.fc4(x)
        #x = torch.tanh(x)
        #x = self.fc5(x)
        #x = torch.tanh(x)
        #x = self.fc6(x)
        #x = torch.tanh(x)
        #x = self.fc7(x)
        #x = torch.tanh(x)
        #x = self.fc8(x)
        #x = torch.tanh(x)
        x = self.fcOut(x)
        #x = torch.sigmoid(x)
        x /= 2*np.pi
        return x

    def train(self, trainloader, epoch, lr, decay):
        
        # Set the device
        self.to(self.device)
        
        # Use Mean Square Error for loss function
        criterion = nn.MSELoss()
        
        # Use the Adam optimizer
        optimizer = optim.Adam(self.parameters(), lr=lr, weight_decay=decay)

        # For each epoch
        for epoch in range(epoch):
            running_loss = 0.0
            for count, data in enumerate(trainloader,0):
                
                # Reset optimizer
                optimizer.zero_grad()
                
                # Extract data from trainloader
                inputs, labels = data
                inputs, labels = inputs.to(self.device), labels.to(self.device)
               
                # Shape the network input as (N,order)
                order = inputs.shape[1]
                inp = inputs.view(order,-1)
                inp = inp.T

                # Forward propagation
                outputs = self(inp)

                # Backward propagation
                lab = labels.view(-1,1)
                loss = criterion(outputs, lab)
                loss.backward()
                running_loss += loss.item()
                
                #print(outputs,lab)
                #exit()
                
                # Adam
                optimizer.step()

            # Compute average loss
            count += 1
            ave_loss = running_loss / count
            print('Epoch: %d   Loss: %.6f   Count: %d' % (epoch,ave_loss,count))

        print('Finished Training')

    def sdr(self,v,mse):
        sdr = 10*np.log10(v/mse)
        return sdr
    
    def accuracy(self, testloader, signal_variance, snr_min, snr_max, plot_flag=False):

        # Set the device
        self.to(self.device)

        # Sum of squared error
        sqe = 0
        sum_err = 0
        
        with torch.no_grad():
            for data in testloader:

                # Extract data from testloader
                inputs, labels = data
                inputs, labels = inputs.to(self.device), labels.to(self.device)

                # Shape network input at (N,order)
                order = inputs.shape[1]
                inp = inputs.view(order,-1)
                inp = inp.T

                # Forward propagation
                outputs = self(inp)
                
                # Compute MSE
                lab = labels.view(-1,1)
                err = torch.abs(outputs - lab)
                sum_err += err.sum().item()
                err = err**2
                sqe += err.sum().item()
                                
        count = len(testloader) * len(labels)
        acc = 1 - sum_err / count
        mse = sqe / count
        sdr = self.sdr(signal_variance,mse)
        return acc,mse,sdr
      
# EOF