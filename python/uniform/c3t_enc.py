"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and Vijay Mishra 
"""

from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd


class c3t_enc:
    
    # Constructor
    def __init__(self, feat_type, radii, low, high, stretch, beta, batch_size, snr_min, snr_max):
        self.feat_type = feat_type
        self.mod_order = len(radii)*2
        self.dist_low = low
        self.dist_high = high
        self.stretch = stretch
        self.beta = beta
        self.batch_size = batch_size
        self.snr_min = snr_min
        self.snr_max = snr_max
        self.radius = radii
        self.power = 0.5*np.array(radii)**2
        
    def plot(x,y):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange")
        plt.title('Regression Analysis')
        plt.xlabel('Independent varible')
        plt.ylabel('Dependent varible')
        plt.grid()
        plt.show()
    
    def ch(self, symb):
        
        # Initialize the output
        rx = np.empty(symb.shape)

        # Pick a random SNR
        a = self.snr_min
        b = self.snr_max
        snr = np.random.uniform(a,b)
        
        # Compute the linear SNR
        snr_lin = 10.0**(snr/10.0)
        
        # Loop for each cos/sin pair
        # Each pair has different signal power
        for ix in range(self.mod_order // 2):
            
            # Compute power for the cos/sin pair
            symb_pow = self.power[ix]
            
            # Compute the noise variance
            noise_std = np.sqrt(symb_pow/snr_lin)
            
            # Generate noise for the cos/sin pair
            n = rnd.normal(0.0, noise_std, (2,symb.shape[1]))
            
            # AWGN
            jx = 2*ix
            kx = jx+1
            rng = [jx,kx]
            rx[rng,:] = symb[rng,:] + n
            
        # Return the channel output
        return rx
    
    # Generate Continuous Curvature Curve Tube symbols
    def get_c3t_symbols(self,num_samps):

        # Draw samples from the uniform distribution
        uni_samps = rnd.uniform(self.dist_low, self.dist_high, num_samps)
        #uni_samps = np.linspace(0,1,num_samps)
        
        # Stretching function, alpha
        alpha = uni_samps * self.stretch

        # Allocate output symbols
        symb = np.empty((self.mod_order,num_samps))
        
        # Loop for half the modulation order
        for ix in range(self.mod_order//2):
            
            # Get the next radius
            r = self.radius[ix]
            
            # Get real and imag parts
            omega = ix + 1
            re = r*np.cos(omega*alpha)
            im = r*np.sin(omega*alpha)
            
            # Save real and imag in rows
            # Each column is a symbol
            kx = 2*ix
            symb[kx,:] = re 
            symb[kx+1,:] = im
        
        # Return symbols and labels
        return symb, uni_samps

    # Compute angles between cos/sin pairs
    def angle_only(self,symb):
        
        # Allocate output
        angles = np.empty((self.mod_order//2, symb.shape[1]))
        
        # Loop for each cos/sin pair
        for ix in range(self.mod_order//2):
            kx = 2*ix
            re = symb[kx,:]
            im = symb[kx+1,:]

            # Find the angle
            angles[ix,:] = np.arctan2(im,re) 
            
        # Return angles
        return angles

    # Compute the L^2 norm for cos/sin pairs
    def torus_projection(self,symb):
        
        # Allocate Output, Gamma, Theta
        tp = np.empty(symb.shape)
        gamma = np.empty((self.mod_order//2, self.samps_per_symb))
        theta = gamma
        
        # Loop for each cos/sin pair
        for ix in range(self.mod_order//2):
            kx = 2*ix
            re = symb[kx,:]
            im = symb[kx+1,:]
            
            # Compute the Gamma and Theta
            gamma[ix,:] = np.sqrt(im*im+re*re)
            ratio = re / gamma[ix,:]
            theta[ix,:] = np.arccos(ratio)
            
            # Get the transmitted radius
            r = self.radius[ix]
            
            # Compute new cos/sin pair
            re = r*np.cos(theta[ix,:])
            im = r*np.sin(theta[ix,:])
            tp[kx,:] = re
            tp[kx+1,:] = im

        # Return the Torus Projection
        return tp


    # mod_order it the dimension 'n'
    def get_data(self, num_samps, shuffle):
      
        # Generate C3T features and the associated labels
        samps, labels = self.get_c3t_symbols(num_samps)
        
        # Send symbol through the AWGN channel
        rx_samps = self.ch(samps)

        # Check if we are doing AngleOnly
        if self.feat_type == 'AngleOnly':
            rx_samps = self.angle_only(rx_samps)
            
        # Check if we are doing Torus Projection
        if self.feat_type == 'TorusProj':
            rx_samps = self.torus_projection(rx_samps)
            
        # Append labels to the data
        dlist = []
        for ix in range(num_samps):
            dlist.append([torch.DoubleTensor(rx_samps[:,ix]), labels[ix]])
            
        print("Generated %d examples" % len(labels))
        loader = torch.utils.data.DataLoader(dlist, batch_size=self.batch_size, shuffle=shuffle)
        return loader

# EOF