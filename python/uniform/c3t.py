"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and Vijay Mishra 
"""

from sys import exit
import torch, numpy
import matplotlib.pyplot as plt
import numpy as np
from c3t_enc import c3t_enc
from c3t_dec import Net

  
"""
##############################################################################

    Main entry point for the C3T codes Monte-Carlo simulation

##############################################################################
"""

# Flags
train_flag = True           # Train the network
test_flag = True            # Test the network
sweep_flag = True           # Evaluate the network
plot_flag = False           # Plotting

# Device
device = torch.device("cpu")

# C3T radius
r4 = [0.8165, 0.5774]
r6 = [0.5835, 0.6463, 0.4918]
r8 = [0.5125, 0.5162, 0.5551, 0.4035]
r10 = [0.4505, 0.4402, 0.4708, 0.5000, 0.3628]
r12 = [0.4141, 0.3953, 0.4102, 0.4346, 0.4566, 0.3265]
r20 = [0.35251032, 0.36757906, 0.37378317, 0.3705179, 0.36658535,
       0.34507531, 0.30497737, 0.24626995, 0.19969124, 0.12894291]
# Density (n=20): 2.294370762688e-6
r40 = [0.23258918, 0.21673772, 0.25316337, 0.23811528, 0.26905355,
       0.25429533, 0.27286513, 0.26387771, 0.26075637, 0.25428426,
       0.25006161, 0.23869478, 0.23285036, 0.22070924, 0.19165789,
       0.18310335, 0.17028559, 0.15342232, 0.09811064, 0.09736838]
# Density (n=40): 0.121540e-12
r100 = [0.12382308, 0.10847692, 0.15717168, 0.17994176, 0.06367002, 0.01645952,
        0.17460105, 0.13555256, 0.12844574, 0.11499835,  0.11685306, 0.19139462,
        0.20346245, 0.12069767, 0.20684936, 0.05684767,  0.20472595, 0.03709949,
        0.10415552, 0.08590632, 0.22643255, 0.21105967,  0.19915536, 0.16115404,
        0.03118854, 0.18561472, 0.12057285, 0.05374938,  0.17488107, 0.17700132,
        0.15179817, 0.20157967, 0.13781863, 0.0384858 ,  0.14233394, 0.1544325,
        0.12247276, 0.06843885, 0.15669923, 0.16760792,  0.17626555, 0.17180191,
        0.06182777, 0.10257803, 0.08054303, 0.13447065,  0.1068071 , 0.12692424,
        0.11186648, 0.05276959]
# Density (n=100): 3.095880e-37

# Number of hidden layers
layers = 3

# Number of hidden neurons
n_hidden = 128*2

# The output dimension
n_output = 1

# Dataset quantities
train_num = 10000
test_num = 2000*5

# Uniform source distribution
low = 0
high = 1
stretch = 2*np.pi / (high - low)
beta = .8

# Hyperparameters
batch_size = 5
epochs = 20
lr = 1e-3
decay = 1e-6

# SNR range
snr_min = 20
snr_max = 20

# Variance of source signal
source_variance = (high-low)**2 / 12

# Features
features = ['Raw','AngleOnly']
features = ['TorusProj','Raw','AngleOnly']
features = ['Raw']

# r_values
r_values = [r4,r6,r8,r20,r40,r100]
r_values = [r4]

# Look for the type of features
for feat_type in features:
    print(feat_type)

    for radii in r_values:
        
        # Modulation order
        mod_order = 2*len(radii)
        print('mod_order:', mod_order)

        # Network dimensions
        if feat_type == 'AngleOnly':
            n_feature = mod_order // 2
        else:
            n_feature = mod_order
        
        # Create the C3T encoder
        enc = c3t_enc(feat_type, radii, low, high, stretch, beta, batch_size, snr_min, snr_max)
        
        # Create the MLP network decoder
        dec = Net(n_feature, n_hidden, n_output, device)
        dec = dec.double()
        
        # Check the Train Network flag
        if train_flag == True:
        
            # Generate data
            trainloader = enc.get_data(train_num, False)
            
            # Train the network
            dec.train(trainloader, epochs, lr, decay)
        
            # Save MLP to file
            fname = 'models\c3t_n%d_%s_layers_%d.ptm' % (mod_order,feat_type,layers)
            torch.save(dec.state_dict(), fname)
        
        # Check the Test Network flag
        if test_flag == True:
        
            # Load model
            fname = 'models\c3t_n%d_%s_layers_%d.ptm' % (mod_order,feat_type,layers)
            dec.load_state_dict(torch.load(fname))
            
            # Test network
            testloader = enc.get_data(test_num, False)
            acc,mse,sdr = dec.accuracy(testloader, source_variance, snr_min, snr_max, plot_flag)
            print('Accuracy: %0.2f percent' % (acc*100))
            print('Mean Square Error: %f' % mse)
            print('Signal-to-Distorion Ratio: %.3f [dB]' % sdr)
        
        # Check for sweeping SNR
        if sweep_flag == True:
        
            # Load model
            fname = 'models\c3t_n%d_%s_layers_%d.ptm' % (mod_order,feat_type,layers)
            dec.load_state_dict(torch.load(fname))
        
            # Compute SDR, MSE and Accuracy for a range of snr SNR levels
            sdr_list = []
            snr_range = np.arange(snr_min, snr_max+1)
            for snr in snr_range:
        
                # Generate symbols at a particular SNR value
                enc.snr_min = snr
                enc.snr_max = snr
                test_loader = enc.get_data(test_num, False) 
                acc,mse,sdr = dec.accuracy(test_loader, source_variance, snr, snr)
                sdr_list.append(sdr)
                print('SNR: %d \t SDR: %.5f \t MSE: %.5f \t Accuracy: %0.2f percent' % (snr,sdr,mse,acc*100.0))

            print('res:',sdr_list)
            
            # Plot accuracy
            leg = 'n = %d   %s' % (mod_order, feat_type)
            plt.plot(snr_range, sdr_list, color = "orange", label=leg)
            plt.xlim(snr_min-1,snr_max+1)
            plt.ylim(min(sdr_list)-1,max(sdr_list)+1)
            plt.grid(True)
            plt.title('Signal-to-Distorsion Rato vs SNR')
            plt.xlabel('SNR [dB]')
            plt.ylabel('SDR [dB]')
            plt.legend()
            fname = 'plots\c3t_sdr_vs_snr_n%d_%s_layers_%d.png' % (mod_order,feat_type,layers)
            plt.savefig(fname)
            plt.show()
            
            # Save accuracy to file
            fname = 'accuracy\c3t_sdr_vs_snr_n%d_%s_layers_%d.txt' % (mod_order,feat_type,layers)
            np.savetxt(fname,np.asarray(sdr_list), delimiter=',')
    
# EOF