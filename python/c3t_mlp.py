"""
    Constant Curvature Curve Tube Codes for Analog Error Correction
    
    Multi-layer Perceptron

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
"""

import sys
from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim

class Net(nn.Module):
    def __init__(self, n_feature, n_hidden, n_output, batch_size, act_fn, device):
        super(Net, self).__init__()

        self.fc1 = nn.Linear(n_feature, n_hidden)
        self.fc2 = nn.Linear(n_hidden, n_hidden*2)
        self.fc3 = nn.Linear(n_hidden*2, n_hidden*4)
        self.fc4 = nn.Linear(n_hidden*4, n_hidden*2)
        self.fc5 = nn.Linear(n_hidden*2, n_hidden)
        self.fcOut = nn.Linear(n_hidden, n_output)
        
        # Batch normalization
        self.bn256  = nn.BatchNorm1d(n_hidden)
        self.bn512  = nn.BatchNorm1d(2*n_hidden)
        self.bn1024  = nn.BatchNorm1d(4*n_hidden)
        
        # The processing device; CPU or GPU
        self.device = device
        self.batch_size = batch_size
        
        # Activation function
        if act_fn == 'ReLU':
            self.act_fn = nn.dReLU
        elif act_fn == 'asinh':
            self.act_fn = torch.asinh
        elif act_fn == 'tanh':
            self.act_fn = torch.tanh
        elif act_fn == 'hardtanh':
            self.act_fn = torch.hardtanh
        else:
            self.act_fn = torch.relu
       
        print('-------------------')
        print('Created C3T decoder')
        print('-------------------')
        
		
    def forward(self, x):
        # Shape should be (batches,order)
        x = self.fc1(x)
        x = self.act_fn(x)       
        x = self.bn256(x)
        x = self.fc2(x)
        x = self.act_fn(x)
        x = self.bn512(x)
        x = self.fc3(x)
        x = self.act_fn(x)
        x = self.bn1024(x)
        x = self.fc4(x)
        x = self.act_fn(x)
        x = self.bn512(x)
        x = self.fc5(x)
        x = self.act_fn(x)
        x = self.bn256(x)
        x = self.fcOut(x)
        return x
     
# EOF
