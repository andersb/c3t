"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
"""

import sys
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
from complexLayers import ComplexBatchNorm1d, ComplexConv2d, ComplexLinear
from complexFunctions import complex_relu, complex_max_pool2d

def CplxTanh(xr,xi):
    xr = torch.tanh(xr)
    xi = torch.tanh(xi)
    return xr,xi

def CplxAsinh(xr,xi):
    z = xr+xi*1j
    x = torch.arcsinh(z)
    return x.real,x.imag

# modReLU
# relu(|z|+b) * (z / |z|)
# b is bias
def modReLU(input_r,input_i,b):
    norm = torch.sqrt(input_r*input_r+input_i*input_i)
    scale = torch.relu(norm + b) / (norm + 1e-6)
    return input_r*scale,input_i*scale

# zReLU
def zReLU(input_r,input_i):
    rmask = torch.ge(input_r, torch.zeros_like(input_r)).double()
    imask = torch.ge(input_i, torch.zeros_like(input_i)).double()
    mask = rmask * imask
    return input_r*mask,input_i*mask

class ComplexNet(nn.Module):
    def __init__(self, n_feature, n_hidden, n_output, batch_size, act_fn, device):
	
        super(ComplexNet, self).__init__()

        # Index to real/imag
        self.rng_r = range(0,n_feature-1,2)
        self.rng_i = range(1,n_feature,2)
        
        # Complex input dimension
        n_inp = int(n_feature/2)
        self.fc1 = ComplexLinear(n_inp, n_hidden)
        self.fc2 = ComplexLinear(n_hidden, n_hidden)
        self.fc3 = ComplexLinear(n_hidden, n_hidden)
        self.fc4 = ComplexLinear(n_hidden, n_hidden)
        self.fc5 = ComplexLinear(n_hidden, n_hidden)
        
        # Output one complex
        self.fcCplxOut = ComplexLinear(n_hidden, n_output)
        
        # Two real-valued numbers to a single real output
        self.fcOut = nn.Linear(2*n_output, n_output)
        
        # Batch normalization
        self.bn256  = ComplexBatchNorm1d(n_hidden)
        self.bn512  = ComplexBatchNorm1d(2*n_hidden)
        self.bn1024  = ComplexBatchNorm1d(4*n_hidden)
        
        # Batch size
        self.batch_size = batch_size
        
        # Activation function
        if act_fn == 'zReLU':
            self.act_fn = zReLU
        elif act_fn == 'modReLU':
            self.act_fn = modReLU
        elif act_fn == 'ReLU':
            self.act_fn = complex_relu
        elif act_fn == 'asinh':
            self.act_fn = CplxAsinh
        elif act_fn == 'tanh':
            self.act_fn = CplxTanh
        elif act_fn == 'hardtanh':
            self.act_fn = nn.Hardtanh()
        else:
            self.act_fn = complex_relu

        # The processing device; CPU or GPU
        self.device = device
        print('-------------------------------')
        print('Created CV (Popoff) C3T decoder')
        print('-------------------------------')
		
    def forward(self, x):

        # Convert n-dimensional input to real/imag
        xr = x[:,self.rng_r]
        xi = x[:,self.rng_i]

        xr,xi = self.fc1(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = self.bn256(xr,xi)
        
        xr,xi = self.fc2(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = self.bn512(xr,xi)

        xr,xi = self.fc3(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = self.bn1024(xr,xi)

        xr,xi = self.fc4(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = self.bn512(xr,xi)

        xr,xi = self.fc5(xr,xi)
        xr,xi = self.act_fn(xr,xi)
        #xr,xi = self.bn256(xr,xi)
        
        # Produce one complex-valued number
        xr,xi = self.fcCplxOut(xr,xi)
        
        # Magnitude/phase as output
        #mag = torch.sqrt(x.imag*x.imag + x.real*x.real)
        #angle = torch.atan2(x.imag,x.real) # +/-pi
        #x = torch.cat((mag, angle), dim=1)

        # Real/Imag as output
        x = torch.cat((xr,xi), dim=1)
        
        # Linear layer as output layer
        x = self.fcOut(x)
        return x 
      
# EOF
