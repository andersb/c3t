Device: cuda
Learning Rate: 5e-05
SNR Training Levels [dB]: [10  5  0 -5]
SNR Eval Range [dB]: [-10,10]
Feature Type:  AngleOnly
Feature Dimension:  1
Modulation Order:  2
Stretching Function: pi * 1.0
Generated 15000 examples with SNR 10 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR -5 dB
The source variance is 0.334668
Training with 60000 examples for 50 epochs
Epoch: 0   Loss: 1.637e-01   Count: 60000
loss_counter: 1
Average change in loss: 0.000000e+00
Relative change in loss: 0.000000e+00
Epoch: 1   Loss: 1.597e-01   Count: 60000
loss_counter: 2
Average change in loss: 1.994527e-03
Relative change in loss: 1.248850e-02
Epoch: 2   Loss: 1.580e-01   Count: 60000
loss_counter: 3
Average change in loss: 1.898573e-03
Relative change in loss: 1.201611e-02
Epoch: 3   Loss: 1.572e-01   Count: 60000
loss_counter: 4
Average change in loss: 1.623390e-03
Relative change in loss: 1.032661e-02
Epoch: 4   Loss: 1.568e-01   Count: 60000
loss_counter: 5
Average change in loss: 1.389544e-03
Relative change in loss: 8.864693e-03
Epoch: 5   Loss: 1.564e-01   Count: 60000
loss_counter: 6
Average change in loss: 1.221086e-03
Relative change in loss: 7.808875e-03
Epoch: 6   Loss: 1.560e-01   Count: 60000
loss_counter: 7
Average change in loss: 1.094166e-03
Relative change in loss: 7.012133e-03
Epoch: 7   Loss: 1.558e-01   Count: 60000
loss_counter: 8
Average change in loss: 9.919399e-04
Relative change in loss: 6.368281e-03
Learning Rate: 2.500000e-05
Epoch: 8   Loss: 1.539e-01   Count: 60000
loss_counter: 1
Average change in loss: 1.863493e-03
Relative change in loss: 1.210854e-02
Epoch: 9   Loss: 1.539e-01   Count: 60000
loss_counter: 2
Average change in loss: 9.163496e-04
Relative change in loss: 5.953033e-03
Epoch: 10   Loss: 1.541e-01   Count: 60000
loss_counter: 3
Average change in loss: 5.581483e-04
Relative change in loss: 3.622267e-03
Epoch: 11   Loss: 1.540e-01   Count: 60000
loss_counter: 4
Average change in loss: 4.284650e-04
Relative change in loss: 2.781361e-03
Epoch: 12   Loss: 1.538e-01   Count: 60000
loss_counter: 5
Average change in loss: 3.922225e-04
Relative change in loss: 2.550187e-03
Epoch: 13   Loss: 1.539e-01   Count: 60000
loss_counter: 6
Average change in loss: 3.076260e-04
Relative change in loss: 1.998651e-03
Epoch: 14   Loss: 1.538e-01   Count: 60000
loss_counter: 7
Average change in loss: 2.834082e-04
Relative change in loss: 1.842961e-03
Epoch: 15   Loss: 1.540e-01   Count: 60000
loss_counter: 8
Average change in loss: 2.199865e-04
Relative change in loss: 1.428459e-03
Learning Rate: 1.250000e-05
Epoch: 16   Loss: 1.532e-01   Count: 60000
loss_counter: 1
Average change in loss: 7.886071e-04
Relative change in loss: 5.147093e-03
Epoch: 17   Loss: 1.532e-01   Count: 60000
loss_counter: 2
Average change in loss: 4.093346e-04
Relative change in loss: 2.672175e-03
Epoch: 18   Loss: 1.532e-01   Count: 60000
loss_counter: 3
Average change in loss: 2.817751e-04
Relative change in loss: 1.839775e-03
Epoch: 19   Loss: 1.531e-01   Count: 60000
loss_counter: 4
Average change in loss: 2.137192e-04
Relative change in loss: 1.395510e-03
Epoch: 20   Loss: 1.530e-01   Count: 60000
loss_counter: 5
Average change in loss: 2.015651e-04
Relative change in loss: 1.317463e-03
Epoch: 21   Loss: 1.532e-01   Count: 60000
loss_counter: 6
Average change in loss: 1.340092e-04
Relative change in loss: 8.747414e-04
Epoch: 22   Loss: 1.532e-01   Count: 60000
loss_counter: 7
Average change in loss: 1.193454e-04
Relative change in loss: 7.791837e-04
Epoch: 23   Loss: 1.531e-01   Count: 60000
loss_counter: 8
Average change in loss: 1.190216e-04
Relative change in loss: 7.776625e-04
Learning Rate: 6.250000e-06
Epoch: 24   Loss: 1.527e-01   Count: 60000
loss_counter: 1
Average change in loss: 3.701031e-04
Relative change in loss: 2.424038e-03
Epoch: 25   Loss: 1.527e-01   Count: 60000
loss_counter: 2
Average change in loss: 1.734239e-04
Relative change in loss: 1.135690e-03
Epoch: 26   Loss: 1.528e-01   Count: 60000
loss_counter: 3
Average change in loss: 9.956808e-05
Relative change in loss: 6.518292e-04
Epoch: 27   Loss: 1.528e-01   Count: 60000
loss_counter: 4
Average change in loss: 7.184694e-05
Relative change in loss: 4.703160e-04
Epoch: 28   Loss: 1.527e-01   Count: 60000
loss_counter: 5
Average change in loss: 7.360135e-05
Relative change in loss: 4.820549e-04
Epoch: 29   Loss: 1.527e-01   Count: 60000
loss_counter: 6
Average change in loss: 6.463518e-05
Relative change in loss: 4.233855e-04
Epoch: 30   Loss: 1.527e-01   Count: 60000
loss_counter: 7
Average change in loss: 4.605345e-05
Relative change in loss: 3.015387e-04
Epoch: 31   Loss: 1.528e-01   Count: 60000
loss_counter: 8
Average change in loss: 3.535316e-05
Relative change in loss: 2.314178e-04
Learning Rate: 3.125000e-06
Epoch: 32   Loss: 1.525e-01   Count: 60000
loss_counter: 1
Average change in loss: 2.289641e-04
Relative change in loss: 1.501023e-03
Epoch: 33   Loss: 1.525e-01   Count: 60000
loss_counter: 2
Average change in loss: 1.194361e-04
Relative change in loss: 7.830397e-04
Epoch: 34   Loss: 1.526e-01   Count: 60000
loss_counter: 3
Average change in loss: 6.330181e-05
Relative change in loss: 4.148822e-04
Epoch: 35   Loss: 1.525e-01   Count: 60000
loss_counter: 4
Average change in loss: 5.532705e-05
Relative change in loss: 3.626900e-04
Epoch: 36   Loss: 1.525e-01   Count: 60000
loss_counter: 5
Average change in loss: 4.831140e-05
Relative change in loss: 3.167418e-04
Epoch: 37   Loss: 1.525e-01   Count: 60000
loss_counter: 6
Average change in loss: 3.774261e-05
Relative change in loss: 2.474257e-04
Epoch: 38   Loss: 1.525e-01   Count: 60000
loss_counter: 7
Average change in loss: 3.259960e-05
Relative change in loss: 2.137125e-04
Epoch: 39   Loss: 1.525e-01   Count: 60000
loss_counter: 8
Average change in loss: 3.224584e-05
Relative change in loss: 2.114346e-04
Learning Rate: 1.562500e-06
Epoch: 40   Loss: 1.524e-01   Count: 60000
loss_counter: 1
Average change in loss: 8.679273e-05
Relative change in loss: 5.694204e-04
Epoch: 41   Loss: 1.524e-01   Count: 60000
loss_counter: 2
Average change in loss: 5.283466e-05
Relative change in loss: 3.466749e-04
Epoch: 42   Loss: 1.524e-01   Count: 60000
loss_counter: 3
Average change in loss: 3.281035e-05
Relative change in loss: 2.152751e-04
Epoch: 43   Loss: 1.524e-01   Count: 60000
loss_counter: 4
Average change in loss: 2.271507e-05
Relative change in loss: 1.490306e-04
Epoch: 44   Loss: 1.524e-01   Count: 60000
loss_counter: 5
Average change in loss: 1.514567e-05
Relative change in loss: 9.935888e-05
Epoch: 45   Loss: 1.524e-01   Count: 60000
loss_counter: 6
Average change in loss: 1.251072e-05
Relative change in loss: 8.207269e-05
Epoch: 46   Loss: 1.524e-01   Count: 60000
loss_counter: 7
Average change in loss: 1.078178e-05
Relative change in loss: 7.073070e-05
Epoch: 47   Loss: 1.524e-01   Count: 60000
loss_counter: 8
Average change in loss: 1.548942e-05
Relative change in loss: 1.016461e-04
Learning Rate: 7.812500e-07
Epoch: 48   Loss: 1.524e-01   Count: 60000
loss_counter: 1
Average change in loss: 1.661963e-06
Relative change in loss: 1.090640e-05
Epoch: 49   Loss: 1.524e-01   Count: 60000
loss_counter: 2
Average change in loss: 1.279000e-05
Relative change in loss: 8.394580e-05
Finished Training
Generated 10000 examples with SNR -10 dB
Generated 10000 examples with SNR -9 dB
Generated 10000 examples with SNR -8 dB
Generated 10000 examples with SNR -7 dB
Generated 10000 examples with SNR -6 dB
Generated 10000 examples with SNR -5 dB
Generated 10000 examples with SNR -4 dB
Generated 10000 examples with SNR -3 dB
Generated 10000 examples with SNR -2 dB
Generated 10000 examples with SNR -1 dB
Generated 10000 examples with SNR 0 dB
Generated 10000 examples with SNR 1 dB
Generated 10000 examples with SNR 2 dB
Generated 10000 examples with SNR 3 dB
Generated 10000 examples with SNR 4 dB
Generated 10000 examples with SNR 5 dB
Generated 10000 examples with SNR 6 dB
Generated 10000 examples with SNR 7 dB
Generated 10000 examples with SNR 8 dB
Generated 10000 examples with SNR 9 dB
Generated 10000 examples with SNR 10 dB
The source variance is 0.334098
Accuracy: 70.00 percent
Mean Square Error: 0.197785
Signal-to-Distorion Ratio: 2.277 [dB]
Generated 10000 examples with SNR -10 dB
The source variance is 0.335984
SNR: -10 	 SDR: -0.75201 	 MSE: 0.39950 	 Accuracy: 51.20 percent
Generated 10000 examples with SNR -9 dB
The source variance is 0.333284
SNR: -9 	 SDR: -0.57380 	 MSE: 0.38036 	 Accuracy: 52.79 percent
Generated 10000 examples with SNR -8 dB
The source variance is 0.330755
SNR: -8 	 SDR: -0.36172 	 MSE: 0.35948 	 Accuracy: 54.50 percent
Generated 10000 examples with SNR -7 dB
The source variance is 0.326723
SNR: -7 	 SDR: -0.11376 	 MSE: 0.33539 	 Accuracy: 56.67 percent
Generated 10000 examples with SNR -6 dB
The source variance is 0.334509
SNR: -6 	 SDR: 0.24455 	 MSE: 0.31619 	 Accuracy: 58.08 percent
Generated 10000 examples with SNR -5 dB
The source variance is 0.331352
SNR: -5 	 SDR: 0.33842 	 MSE: 0.30651 	 Accuracy: 59.27 percent
Generated 10000 examples with SNR -4 dB
The source variance is 0.329355
SNR: -4 	 SDR: 0.79039 	 MSE: 0.27455 	 Accuracy: 62.12 percent
Generated 10000 examples with SNR -3 dB
The source variance is 0.330762
SNR: -3 	 SDR: 0.92085 	 MSE: 0.26757 	 Accuracy: 63.04 percent
Generated 10000 examples with SNR -2 dB
The source variance is 0.334634
SNR: -2 	 SDR: 1.36564 	 MSE: 0.24435 	 Accuracy: 65.32 percent
Generated 10000 examples with SNR -1 dB
The source variance is 0.331473
SNR: -1 	 SDR: 1.90889 	 MSE: 0.21358 	 Accuracy: 68.06 percent
Generated 10000 examples with SNR 0 dB
The source variance is 0.333295
SNR: 0 	 SDR: 2.51106 	 MSE: 0.18695 	 Accuracy: 70.63 percent
Generated 10000 examples with SNR 1 dB
The source variance is 0.334847
SNR: 1 	 SDR: 3.07747 	 MSE: 0.16485 	 Accuracy: 72.75 percent
Generated 10000 examples with SNR 2 dB
The source variance is 0.333891
SNR: 2 	 SDR: 4.17639 	 MSE: 0.12763 	 Accuracy: 76.05 percent
Generated 10000 examples with SNR 3 dB
The source variance is 0.335747
SNR: 3 	 SDR: 4.67026 	 MSE: 0.11455 	 Accuracy: 77.56 percent
Generated 10000 examples with SNR 4 dB
The source variance is 0.329015
SNR: 4 	 SDR: 5.37449 	 MSE: 0.09545 	 Accuracy: 79.60 percent
Generated 10000 examples with SNR 5 dB
The source variance is 0.337757
SNR: 5 	 SDR: 5.97418 	 MSE: 0.08535 	 Accuracy: 80.76 percent
Generated 10000 examples with SNR 6 dB
The source variance is 0.332604
SNR: 6 	 SDR: 6.88481 	 MSE: 0.06815 	 Accuracy: 82.47 percent
Generated 10000 examples with SNR 7 dB
The source variance is 0.335380
SNR: 7 	 SDR: 7.54171 	 MSE: 0.05907 	 Accuracy: 83.65 percent
Generated 10000 examples with SNR 8 dB
The source variance is 0.334129
SNR: 8 	 SDR: 8.13802 	 MSE: 0.05130 	 Accuracy: 84.60 percent
Generated 10000 examples with SNR 9 dB
The source variance is 0.332119
SNR: 9 	 SDR: 8.62058 	 MSE: 0.04563 	 Accuracy: 85.52 percent
Generated 10000 examples with SNR 10 dB
The source variance is 0.325783
SNR: 10 	 SDR: 9.04604 	 MSE: 0.04058 	 Accuracy: 86.45 percent
res: [-0.7520069335566405, -0.573800967621652, -0.3617221243744241, -0.11375984056383143, 0.24454665019235705, 0.3384205889764719, 0.7903903796256989, 0.9208484244215266, 1.3656429179338563, 1.9088907643982223, 2.5110565055614744, 3.0774744249797674, 4.176385569283654, 4.6702600507867675, 5.374487883222372, 5.974176419956945, 6.8848142470332006, 7.541713209174901, 8.138016952685842, 8.620579441269927, 9.046038940960322]
[0.8165, 0.5774]
Log is written to logs/log_n4_AngleOnly.txt
