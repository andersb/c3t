Device: cuda
Learning Rate: 5e-05
SNR Training Levels [dB]: [10  5  0 -5]
SNR Eval Range [dB]: [-10,10]
Feature Type:  Raw
Feature Dimension:  4
Modulation Order:  4
Stretching Function: pi * 1.0
Generated 15000 examples with SNR 10 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR -5 dB
The source variance is 0.333225
Training with 60000 examples for 50 epochs
Epoch: 0   Loss: 1.189e-01   Count: 60000
loss_counter: 1
Average change in loss: 0.000000e+00
Relative change in loss: 0.000000e+00
Epoch: 1   Loss: 1.095e-01   Count: 60000
loss_counter: 2
Average change in loss: 4.683795e-03
Relative change in loss: 4.277383e-02
Epoch: 2   Loss: 1.075e-01   Count: 60000
loss_counter: 3
Average change in loss: 3.773712e-03
Relative change in loss: 3.508866e-02
Epoch: 3   Loss: 1.068e-01   Count: 60000
loss_counter: 4
Average change in loss: 3.012612e-03
Relative change in loss: 2.820308e-02
Epoch: 4   Loss: 1.064e-01   Count: 60000
loss_counter: 5
Average change in loss: 2.496253e-03
Relative change in loss: 2.346372e-02
Epoch: 5   Loss: 1.058e-01   Count: 60000
loss_counter: 6
Average change in loss: 2.185036e-03
Relative change in loss: 2.066056e-02
Epoch: 6   Loss: 1.055e-01   Count: 60000
loss_counter: 7
Average change in loss: 1.906959e-03
Relative change in loss: 1.807197e-02
Epoch: 7   Loss: 1.051e-01   Count: 60000
loss_counter: 8
Average change in loss: 1.715130e-03
Relative change in loss: 1.631159e-02
Learning Rate: 2.500000e-05
Epoch: 8   Loss: 1.031e-01   Count: 60000
loss_counter: 1
Average change in loss: 2.026971e-03
Relative change in loss: 1.965624e-02
Epoch: 9   Loss: 1.027e-01   Count: 60000
loss_counter: 2
Average change in loss: 1.238423e-03
Relative change in loss: 1.206203e-02
Epoch: 10   Loss: 1.027e-01   Count: 60000
loss_counter: 3
Average change in loss: 8.274140e-04
Relative change in loss: 8.059300e-03
Epoch: 11   Loss: 1.026e-01   Count: 60000
loss_counter: 4
Average change in loss: 6.363851e-04
Relative change in loss: 6.202436e-03
Epoch: 12   Loss: 1.025e-01   Count: 60000
loss_counter: 5
Average change in loss: 5.260891e-04
Relative change in loss: 5.131699e-03
Epoch: 13   Loss: 1.024e-01   Count: 60000
loss_counter: 6
Average change in loss: 4.544674e-04
Relative change in loss: 4.437241e-03
Epoch: 14   Loss: 1.022e-01   Count: 60000
loss_counter: 7
Average change in loss: 4.162947e-04
Relative change in loss: 4.071982e-03
Epoch: 15   Loss: 1.021e-01   Count: 60000
loss_counter: 8
Average change in loss: 3.828297e-04
Relative change in loss: 3.750095e-03
Learning Rate: 1.250000e-05
Epoch: 16   Loss: 1.011e-01   Count: 60000
loss_counter: 1
Average change in loss: 9.694453e-04
Relative change in loss: 9.587467e-03
Epoch: 17   Loss: 1.010e-01   Count: 60000
loss_counter: 2
Average change in loss: 5.488462e-04
Relative change in loss: 5.434785e-03
Epoch: 18   Loss: 1.008e-01   Count: 60000
loss_counter: 3
Average change in loss: 4.231807e-04
Relative change in loss: 4.197563e-03
Epoch: 19   Loss: 1.008e-01   Count: 60000
loss_counter: 4
Average change in loss: 3.210400e-04
Relative change in loss: 3.184884e-03
Epoch: 20   Loss: 1.007e-01   Count: 60000
loss_counter: 5
Average change in loss: 2.711803e-04
Relative change in loss: 2.692165e-03
Epoch: 21   Loss: 1.007e-01   Count: 60000
loss_counter: 6
Average change in loss: 2.278302e-04
Relative change in loss: 2.262052e-03
Epoch: 22   Loss: 1.007e-01   Count: 60000
loss_counter: 7
Average change in loss: 1.943848e-04
Relative change in loss: 1.929864e-03
Epoch: 23   Loss: 1.006e-01   Count: 60000
loss_counter: 8
Average change in loss: 1.899269e-04
Relative change in loss: 1.888581e-03
Learning Rate: 6.250000e-06
Epoch: 24   Loss: 9.997e-02   Count: 60000
loss_counter: 1
Average change in loss: 5.981539e-04
Relative change in loss: 5.983467e-03
Epoch: 25   Loss: 9.986e-02   Count: 60000
loss_counter: 2
Average change in loss: 3.534246e-04
Relative change in loss: 3.539234e-03
Epoch: 26   Loss: 9.987e-02   Count: 60000
loss_counter: 3
Average change in loss: 2.313762e-04
Relative change in loss: 2.316732e-03
Epoch: 27   Loss: 9.979e-02   Count: 60000
loss_counter: 4
Average change in loss: 1.931435e-04
Relative change in loss: 1.935435e-03
Epoch: 28   Loss: 9.976e-02   Count: 60000
loss_counter: 5
Average change in loss: 1.607174e-04
Relative change in loss: 1.611002e-03
Epoch: 29   Loss: 9.975e-02   Count: 60000
loss_counter: 6
Average change in loss: 1.358743e-04
Relative change in loss: 1.362139e-03
Epoch: 30   Loss: 9.973e-02   Count: 60000
loss_counter: 7
Average change in loss: 1.189709e-04
Relative change in loss: 1.192893e-03
Epoch: 31   Loss: 9.963e-02   Count: 60000
loss_counter: 8
Average change in loss: 1.174231e-04
Relative change in loss: 1.178633e-03
Learning Rate: 3.125000e-06
Epoch: 32   Loss: 9.931e-02   Count: 60000
loss_counter: 1
Average change in loss: 3.172246e-04
Relative change in loss: 3.194308e-03
Epoch: 33   Loss: 9.927e-02   Count: 60000
loss_counter: 2
Average change in loss: 1.764655e-04
Relative change in loss: 1.777567e-03
Epoch: 34   Loss: 9.922e-02   Count: 60000
loss_counter: 3
Average change in loss: 1.356038e-04
Relative change in loss: 1.366702e-03
Epoch: 35   Loss: 9.914e-02   Count: 60000
loss_counter: 4
Average change in loss: 1.204530e-04
Relative change in loss: 1.214921e-03
Epoch: 36   Loss: 9.918e-02   Count: 60000
loss_counter: 5
Average change in loss: 8.959364e-05
Relative change in loss: 9.033569e-04
Epoch: 37   Loss: 9.913e-02   Count: 60000
loss_counter: 6
Average change in loss: 8.344474e-05
Relative change in loss: 8.418059e-04
Epoch: 38   Loss: 9.909e-02   Count: 60000
loss_counter: 7
Average change in loss: 7.701991e-05
Relative change in loss: 7.772927e-04
Epoch: 39   Loss: 9.901e-02   Count: 60000
loss_counter: 8
Average change in loss: 7.669243e-05
Relative change in loss: 7.745693e-04
Learning Rate: 1.562500e-06
Epoch: 40   Loss: 9.885e-02   Count: 60000
loss_counter: 1
Average change in loss: 1.662495e-04
Relative change in loss: 1.681891e-03
Epoch: 41   Loss: 9.880e-02   Count: 60000
loss_counter: 2
Average change in loss: 1.070168e-04
Relative change in loss: 1.083178e-03
Epoch: 42   Loss: 9.879e-02   Count: 60000
loss_counter: 3
Average change in loss: 7.360040e-05
Relative change in loss: 7.450022e-04
Epoch: 43   Loss: 9.876e-02   Count: 60000
loss_counter: 4
Average change in loss: 6.333277e-05
Relative change in loss: 6.412817e-04
Epoch: 44   Loss: 9.874e-02   Count: 60000
loss_counter: 5
Average change in loss: 5.384435e-05
Relative change in loss: 5.452936e-04
Epoch: 45   Loss: 9.873e-02   Count: 60000
loss_counter: 6
Average change in loss: 4.680590e-05
Relative change in loss: 4.740694e-04
Epoch: 46   Loss: 9.871e-02   Count: 60000
loss_counter: 7
Average change in loss: 4.286762e-05
Relative change in loss: 4.342655e-04
Epoch: 47   Loss: 9.870e-02   Count: 60000
loss_counter: 8
Average change in loss: 3.887458e-05
Relative change in loss: 3.938581e-04
Learning Rate: 7.812500e-07
Epoch: 48   Loss: 9.855e-02   Count: 60000
loss_counter: 1
Average change in loss: 1.537715e-04
Relative change in loss: 1.560368e-03
Epoch: 49   Loss: 9.852e-02   Count: 60000
loss_counter: 2
Average change in loss: 8.890568e-05
Relative change in loss: 9.023741e-04
Finished Training
Generated 10000 examples with SNR -10 dB
Generated 10000 examples with SNR -9 dB
Generated 10000 examples with SNR -8 dB
Generated 10000 examples with SNR -7 dB
Generated 10000 examples with SNR -6 dB
Generated 10000 examples with SNR -5 dB
Generated 10000 examples with SNR -4 dB
Generated 10000 examples with SNR -3 dB
Generated 10000 examples with SNR -2 dB
Generated 10000 examples with SNR -1 dB
Generated 10000 examples with SNR 0 dB
Generated 10000 examples with SNR 1 dB
Generated 10000 examples with SNR 2 dB
Generated 10000 examples with SNR 3 dB
Generated 10000 examples with SNR 4 dB
Generated 10000 examples with SNR 5 dB
Generated 10000 examples with SNR 6 dB
Generated 10000 examples with SNR 7 dB
Generated 10000 examples with SNR 8 dB
Generated 10000 examples with SNR 9 dB
Generated 10000 examples with SNR 10 dB
The source variance is 0.332968
Accuracy: 76.22 percent
Mean Square Error: 0.148264
Signal-to-Distorion Ratio: 3.514 [dB]
Generated 10000 examples with SNR -10 dB
The source variance is 0.334480
SNR: -10 	 SDR: -0.31245 	 MSE: 0.35943 	 Accuracy: 53.12 percent
Generated 10000 examples with SNR -9 dB
The source variance is 0.331413
SNR: -9 	 SDR: -0.16958 	 MSE: 0.34461 	 Accuracy: 54.59 percent
Generated 10000 examples with SNR -8 dB
The source variance is 0.333506
SNR: -8 	 SDR: 0.11269 	 MSE: 0.32496 	 Accuracy: 56.23 percent
Generated 10000 examples with SNR -7 dB
The source variance is 0.331960
SNR: -7 	 SDR: 0.39685 	 MSE: 0.30297 	 Accuracy: 58.23 percent
Generated 10000 examples with SNR -6 dB
The source variance is 0.332251
SNR: -6 	 SDR: 0.66699 	 MSE: 0.28495 	 Accuracy: 59.93 percent
Generated 10000 examples with SNR -5 dB
The source variance is 0.331101
SNR: -5 	 SDR: 0.88270 	 MSE: 0.27020 	 Accuracy: 61.91 percent
Generated 10000 examples with SNR -4 dB
The source variance is 0.334470
SNR: -4 	 SDR: 1.35749 	 MSE: 0.24469 	 Accuracy: 64.32 percent
Generated 10000 examples with SNR -3 dB
The source variance is 0.333432
SNR: -3 	 SDR: 2.02730 	 MSE: 0.20906 	 Accuracy: 67.60 percent
Generated 10000 examples with SNR -2 dB
The source variance is 0.330219
SNR: -2 	 SDR: 2.66098 	 MSE: 0.17894 	 Accuracy: 71.04 percent
Generated 10000 examples with SNR -1 dB
The source variance is 0.329306
SNR: -1 	 SDR: 3.45054 	 MSE: 0.14878 	 Accuracy: 74.29 percent
Generated 10000 examples with SNR 0 dB
The source variance is 0.336694
SNR: 0 	 SDR: 4.24403 	 MSE: 0.12672 	 Accuracy: 76.82 percent
Generated 10000 examples with SNR 1 dB
The source variance is 0.330588
SNR: 1 	 SDR: 5.30567 	 MSE: 0.09744 	 Accuracy: 80.27 percent
Generated 10000 examples with SNR 2 dB
The source variance is 0.333402
SNR: 2 	 SDR: 6.58865 	 MSE: 0.07313 	 Accuracy: 83.23 percent
Generated 10000 examples with SNR 3 dB
The source variance is 0.332341
SNR: 3 	 SDR: 8.38250 	 MSE: 0.04823 	 Accuracy: 86.47 percent
Generated 10000 examples with SNR 4 dB
The source variance is 0.334198
SNR: 4 	 SDR: 9.62932 	 MSE: 0.03640 	 Accuracy: 88.54 percent
Generated 10000 examples with SNR 5 dB
The source variance is 0.331912
SNR: 5 	 SDR: 11.31713 	 MSE: 0.02451 	 Accuracy: 90.72 percent
Generated 10000 examples with SNR 6 dB
The source variance is 0.333277
SNR: 6 	 SDR: 13.46669 	 MSE: 0.01500 	 Accuracy: 92.45 percent
Generated 10000 examples with SNR 7 dB
The source variance is 0.333630
SNR: 7 	 SDR: 15.52852 	 MSE: 0.00934 	 Accuracy: 93.82 percent
Generated 10000 examples with SNR 8 dB
The source variance is 0.330928
SNR: 8 	 SDR: 17.60972 	 MSE: 0.00574 	 Accuracy: 94.88 percent
Generated 10000 examples with SNR 9 dB
The source variance is 0.336016
SNR: 9 	 SDR: 19.21957 	 MSE: 0.00402 	 Accuracy: 95.66 percent
Generated 10000 examples with SNR 10 dB
The source variance is 0.334931
SNR: 10 	 SDR: 20.79202 	 MSE: 0.00279 	 Accuracy: 96.25 percent
res: [-0.3124450860327251, -0.16957931716216956, 0.11268635226420667, 0.3968481634808151, 0.6669902561292175, 0.8826956704730903, 1.3574946324892454, 2.0273027128251915, 2.660983240802108, 3.4505406771045455, 4.2440305473247495, 5.3056701772885795, 6.588650320582011, 8.382500157084571, 9.629317812301878, 11.317130613684867, 13.466693253516208, 15.528517786525939, 17.609719698615905, 19.219574079899587, 20.792021031352945]
[0.5835, 0.6463, 0.4918]
Log is written to logs/log_n6_Raw.txt
