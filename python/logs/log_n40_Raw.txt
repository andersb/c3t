Device: cuda
Learning Rate: 0.0001
SNR Training Levels [dB]: [10  5  0 -5]
SNR Eval Range [dB]: [-10,10]
Feature Type:  Raw
Feature Dimension:  40
Modulation Order:  40
Stretching Function: pi * 1.0
Generated 15000 examples with SNR 10 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR -5 dB
The source variance is 0.333699
Training with 60000 examples for 50 epochs
Epoch: 0   Loss: 1.184e-01   Count: 60000
loss_counter: 1
Average change in loss: 0.000000e+00
Relative change in loss: 0.000000e+00
Epoch: 1   Loss: 9.023e-02   Count: 60000
loss_counter: 2
Average change in loss: 1.410141e-02
Relative change in loss: 1.562764e-01
Epoch: 2   Loss: 8.110e-02   Count: 60000
loss_counter: 3
Average change in loss: 1.244692e-02
Relative change in loss: 1.534840e-01
Epoch: 3   Loss: 7.371e-02   Count: 60000
loss_counter: 4
Average change in loss: 1.118126e-02
Relative change in loss: 1.516894e-01
Epoch: 4   Loss: 6.692e-02   Count: 60000
loss_counter: 5
Average change in loss: 1.030275e-02
Relative change in loss: 1.539496e-01
Epoch: 5   Loss: 6.090e-02   Count: 60000
loss_counter: 6
Average change in loss: 9.590224e-03
Relative change in loss: 1.574872e-01
Epoch: 6   Loss: 5.637e-02   Count: 60000
loss_counter: 7
Average change in loss: 8.866196e-03
Relative change in loss: 1.572767e-01
Epoch: 7   Loss: 5.239e-02   Count: 60000
loss_counter: 8
Average change in loss: 8.255726e-03
Relative change in loss: 1.575798e-01
Epoch: 8   Loss: 4.851e-02   Count: 60000
loss_counter: 9
Average change in loss: 7.769448e-03
Relative change in loss: 1.601566e-01
Epoch: 9   Loss: 4.436e-02   Count: 60000
loss_counter: 10
Average change in loss: 7.407906e-03
Relative change in loss: 1.670045e-01
Epoch: 10   Loss: 4.100e-02   Count: 60000
loss_counter: 11
Average change in loss: 7.039827e-03
Relative change in loss: 1.717094e-01
Epoch: 11   Loss: 3.689e-02   Count: 60000
loss_counter: 12
Average change in loss: 6.795930e-03
Relative change in loss: 1.842443e-01
Epoch: 12   Loss: 3.382e-02   Count: 60000
loss_counter: 13
Average change in loss: 6.509129e-03
Relative change in loss: 1.924758e-01
Epoch: 13   Loss: 3.077e-02   Count: 60000
loss_counter: 14
Average change in loss: 6.261717e-03
Relative change in loss: 2.034839e-01
Epoch: 14   Loss: 2.730e-02   Count: 60000
loss_counter: 15
Average change in loss: 6.075524e-03
Relative change in loss: 2.225163e-01
Epoch: 15   Loss: 2.474e-02   Count: 60000
loss_counter: 16
Average change in loss: 5.855788e-03
Relative change in loss: 2.366551e-01
Epoch: 16   Loss: 2.242e-02   Count: 60000
loss_counter: 17
Average change in loss: 5.648068e-03
Relative change in loss: 2.519274e-01
Epoch: 17   Loss: 2.001e-02   Count: 60000
loss_counter: 18
Average change in loss: 5.468090e-03
Relative change in loss: 2.732546e-01
Epoch: 18   Loss: 1.817e-02   Count: 60000
loss_counter: 19
Average change in loss: 5.277299e-03
Relative change in loss: 2.904738e-01
Epoch: 19   Loss: 1.671e-02   Count: 60000
loss_counter: 20
Average change in loss: 5.086308e-03
Relative change in loss: 3.043795e-01
Epoch: 20   Loss: 1.523e-02   Count: 60000
loss_counter: 21
Average change in loss: 4.914704e-03
Relative change in loss: 3.227454e-01
Epoch: 21   Loss: 1.393e-02   Count: 60000
loss_counter: 22
Average change in loss: 4.750407e-03
Relative change in loss: 3.410776e-01
Epoch: 22   Loss: 1.290e-02   Count: 60000
loss_counter: 23
Average change in loss: 4.588338e-03
Relative change in loss: 3.555526e-01
Epoch: 23   Loss: 1.191e-02   Count: 60000
loss_counter: 24
Average change in loss: 4.438497e-03
Relative change in loss: 3.725864e-01
Epoch: 24   Loss: 1.097e-02   Count: 60000
loss_counter: 25
Average change in loss: 4.298799e-03
Relative change in loss: 3.919898e-01
Epoch: 25   Loss: 1.030e-02   Count: 60000
loss_counter: 26
Average change in loss: 4.159251e-03
Relative change in loss: 4.039653e-01
Epoch: 26   Loss: 9.851e-03   Count: 60000
loss_counter: 27
Average change in loss: 4.021692e-03
Relative change in loss: 4.082556e-01
Epoch: 27   Loss: 9.452e-03   Count: 60000
loss_counter: 28
Average change in loss: 3.892313e-03
Relative change in loss: 4.118058e-01
Epoch: 28   Loss: 8.800e-03   Count: 60000
loss_counter: 29
Average change in loss: 3.780559e-03
Relative change in loss: 4.295911e-01
Epoch: 29   Loss: 8.504e-03   Count: 60000
loss_counter: 30
Average change in loss: 3.664407e-03
Relative change in loss: 4.308850e-01
Epoch: 30   Loss: 8.034e-03   Count: 60000
loss_counter: 31
Average change in loss: 3.561382e-03
Relative change in loss: 4.433036e-01
Epoch: 31   Loss: 7.731e-03   Count: 60000
loss_counter: 32
Average change in loss: 3.459547e-03
Relative change in loss: 4.474863e-01
Epoch: 32   Loss: 7.136e-03   Count: 60000
loss_counter: 33
Average change in loss: 3.372745e-03
Relative change in loss: 4.726376e-01
Epoch: 33   Loss: 6.848e-03   Count: 60000
loss_counter: 34
Average change in loss: 3.282030e-03
Relative change in loss: 4.792993e-01
Epoch: 34   Loss: 6.452e-03   Count: 60000
loss_counter: 35
Average change in loss: 3.199569e-03
Relative change in loss: 4.959298e-01
Epoch: 35   Loss: 6.395e-03   Count: 60000
loss_counter: 36
Average change in loss: 3.112255e-03
Relative change in loss: 4.866383e-01
Epoch: 36   Loss: 5.826e-03   Count: 60000
loss_counter: 37
Average change in loss: 3.043527e-03
Relative change in loss: 5.223969e-01
Epoch: 37   Loss: 5.801e-03   Count: 60000
loss_counter: 38
Average change in loss: 2.964092e-03
Relative change in loss: 5.109542e-01
Epoch: 38   Loss: 5.508e-03   Count: 60000
loss_counter: 39
Average change in loss: 2.895616e-03
Relative change in loss: 5.257520e-01
Epoch: 39   Loss: 5.554e-03   Count: 60000
loss_counter: 40
Average change in loss: 2.822077e-03
Relative change in loss: 5.081603e-01
Epoch: 40   Loss: 5.001e-03   Count: 60000
loss_counter: 41
Average change in loss: 2.766733e-03
Relative change in loss: 5.532862e-01
Epoch: 41   Loss: 5.000e-03   Count: 60000
loss_counter: 42
Average change in loss: 2.700867e-03
Relative change in loss: 5.401531e-01
Epoch: 42   Loss: 4.837e-03   Count: 60000
loss_counter: 43
Average change in loss: 2.641859e-03
Relative change in loss: 5.462147e-01
Epoch: 43   Loss: 4.847e-03   Count: 60000
loss_counter: 44
Average change in loss: 2.581591e-03
Relative change in loss: 5.326595e-01
Epoch: 44   Loss: 4.680e-03   Count: 60000
loss_counter: 45
Average change in loss: 2.527924e-03
Relative change in loss: 5.401527e-01
Epoch: 45   Loss: 4.387e-03   Count: 60000
loss_counter: 46
Average change in loss: 2.479347e-03
Relative change in loss: 5.652085e-01
Epoch: 46   Loss: 4.480e-03   Count: 60000
loss_counter: 47
Average change in loss: 2.424612e-03
Relative change in loss: 5.412296e-01
Epoch: 47   Loss: 4.327e-03   Count: 60000
loss_counter: 48
Average change in loss: 2.377276e-03
Relative change in loss: 5.493634e-01
Epoch: 48   Loss: 4.126e-03   Count: 60000
loss_counter: 49
Average change in loss: 2.332877e-03
Relative change in loss: 5.654606e-01
Epoch: 49   Loss: 4.051e-03   Count: 60000
loss_counter: 50
Average change in loss: 2.287717e-03
Relative change in loss: 5.647670e-01
Finished Training
Generated 10000 examples with SNR -10 dB
Generated 10000 examples with SNR -9 dB
Generated 10000 examples with SNR -8 dB
Generated 10000 examples with SNR -7 dB
Generated 10000 examples with SNR -6 dB
Generated 10000 examples with SNR -5 dB
Generated 10000 examples with SNR -4 dB
Generated 10000 examples with SNR -3 dB
Generated 10000 examples with SNR -2 dB
Generated 10000 examples with SNR -1 dB
Generated 10000 examples with SNR 0 dB
Generated 10000 examples with SNR 1 dB
Generated 10000 examples with SNR 2 dB
Generated 10000 examples with SNR 3 dB
Generated 10000 examples with SNR 4 dB
Generated 10000 examples with SNR 5 dB
Generated 10000 examples with SNR 6 dB
Generated 10000 examples with SNR 7 dB
Generated 10000 examples with SNR 8 dB
Generated 10000 examples with SNR 9 dB
Generated 10000 examples with SNR 10 dB
The source variance is 0.333370
Accuracy: 87.54 percent
Mean Square Error: 0.091340
Signal-to-Distorion Ratio: 5.623 [dB]
Generated 10000 examples with SNR -10 dB
The source variance is 0.331485
SNR: -10 	 SDR: -0.91363 	 MSE: 0.40910 	 Accuracy: 54.02 percent
Generated 10000 examples with SNR -9 dB
The source variance is 0.331917
SNR: -9 	 SDR: -0.39700 	 MSE: 0.36369 	 Accuracy: 58.08 percent
Generated 10000 examples with SNR -8 dB
The source variance is 0.330000
SNR: -8 	 SDR: 0.22334 	 MSE: 0.31346 	 Accuracy: 62.18 percent
Generated 10000 examples with SNR -7 dB
The source variance is 0.329501
SNR: -7 	 SDR: 1.16514 	 MSE: 0.25197 	 Accuracy: 68.30 percent
Generated 10000 examples with SNR -6 dB
The source variance is 0.336120
SNR: -6 	 SDR: 2.32368 	 MSE: 0.19685 	 Accuracy: 73.47 percent
Generated 10000 examples with SNR -5 dB
The source variance is 0.332340
SNR: -5 	 SDR: 3.64875 	 MSE: 0.14345 	 Accuracy: 79.38 percent
Generated 10000 examples with SNR -4 dB
The source variance is 0.332981
SNR: -4 	 SDR: 5.22482 	 MSE: 0.09999 	 Accuracy: 84.38 percent
Generated 10000 examples with SNR -3 dB
The source variance is 0.328326
SNR: -3 	 SDR: 7.27218 	 MSE: 0.06153 	 Accuracy: 89.13 percent
Generated 10000 examples with SNR -2 dB
The source variance is 0.333342
SNR: -2 	 SDR: 10.07099 	 MSE: 0.03279 	 Accuracy: 92.89 percent
Generated 10000 examples with SNR -1 dB
The source variance is 0.334992
SNR: -1 	 SDR: 12.75178 	 MSE: 0.01778 	 Accuracy: 95.20 percent
Generated 10000 examples with SNR 0 dB
The source variance is 0.332453
SNR: 0 	 SDR: 16.57311 	 MSE: 0.00732 	 Accuracy: 96.99 percent
Generated 10000 examples with SNR 1 dB
The source variance is 0.333321
SNR: 1 	 SDR: 20.27764 	 MSE: 0.00313 	 Accuracy: 97.84 percent
Generated 10000 examples with SNR 2 dB
The source variance is 0.332658
SNR: 2 	 SDR: 24.50455 	 MSE: 0.00118 	 Accuracy: 98.28 percent
Generated 10000 examples with SNR 3 dB
The source variance is 0.334753
SNR: 3 	 SDR: 28.30633 	 MSE: 0.00049 	 Accuracy: 98.50 percent
Generated 10000 examples with SNR 4 dB
The source variance is 0.330688
SNR: 4 	 SDR: 29.73012 	 MSE: 0.00035 	 Accuracy: 98.61 percent
Generated 10000 examples with SNR 5 dB
The source variance is 0.329246
SNR: 5 	 SDR: 30.66015 	 MSE: 0.00028 	 Accuracy: 98.69 percent
Generated 10000 examples with SNR 6 dB
The source variance is 0.330919
SNR: 6 	 SDR: 31.25020 	 MSE: 0.00025 	 Accuracy: 98.72 percent
Generated 10000 examples with SNR 7 dB
The source variance is 0.331669
SNR: 7 	 SDR: 31.48531 	 MSE: 0.00024 	 Accuracy: 98.75 percent
Generated 10000 examples with SNR 8 dB
The source variance is 0.330839
SNR: 8 	 SDR: 31.74610 	 MSE: 0.00022 	 Accuracy: 98.79 percent
Generated 10000 examples with SNR 9 dB
The source variance is 0.332646
SNR: 9 	 SDR: 31.87579 	 MSE: 0.00022 	 Accuracy: 98.80 percent
Generated 10000 examples with SNR 10 dB
The source variance is 0.334087
SNR: 10 	 SDR: 32.01355 	 MSE: 0.00021 	 Accuracy: 98.82 percent
res: [-0.913630447964814, -0.3970033114096419, 0.22334032101893997, 1.165138302591332, 2.323680257775514, 3.6487482409494736, 5.224822163554034, 7.272178063459958, 10.070989564227792, 12.751782495407696, 16.573114770700407, 20.277637677221435, 24.50454949520893, 28.306331883226438, 29.73012235364437, 30.66015179696318, 31.25020490278501, 31.485313324387086, 31.74609525993992, 31.875793882237154, 32.01354612158143]
[0.12382308, 0.10847692, 0.15717168, 0.17994176, 0.06367002, 0.01645952, 0.17460105, 0.13555256, 0.12844574, 0.11499835, 0.11685306, 0.19139462, 0.20346245, 0.12069767, 0.20684936, 0.05684767, 0.20472595, 0.03709949, 0.10415552, 0.08590632, 0.22643255, 0.21105967, 0.19915536, 0.16115404, 0.03118854, 0.18561472, 0.12057285, 0.05374938, 0.17488107, 0.17700132, 0.15179817, 0.20157967, 0.13781863, 0.0384858, 0.14233394, 0.1544325, 0.12247276, 0.06843885, 0.15669923, 0.16760792, 0.17626555, 0.17180191, 0.06182777, 0.10257803, 0.08054303, 0.13447065, 0.1068071, 0.12692424, 0.11186648, 0.05276959]
Log is written to logs/log_n100_Raw.txt
