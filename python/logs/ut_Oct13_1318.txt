Device: cuda
TorusProj
Modulation Order: 4
SNR Range [dB]: [-5,10]
Generated 50000 examples with variance 0.083859
Epoch: 0   Loss: 0.00203   Count: 50000
Epoch: 1   Loss: 0.00091   Count: 50000
Epoch: 2   Loss: 0.00088   Count: 50000
Epoch: 3   Loss: 0.00081   Count: 50000
Epoch: 4   Loss: 0.00081   Count: 50000
Epoch: 5   Loss: 0.00077   Count: 50000
Epoch: 6   Loss: 0.00077   Count: 50000
Epoch: 7   Loss: 0.00074   Count: 50000
Epoch: 8   Loss: 0.00074   Count: 50000
Epoch: 9   Loss: 0.00077   Count: 50000
Epoch: 10   Loss: 0.00073   Count: 50000
Epoch: 11   Loss: 0.00075   Count: 50000
Epoch: 12   Loss: 0.00076   Count: 50000
Epoch: 13   Loss: 0.00076   Count: 50000
Epoch: 14   Loss: 0.00077   Count: 50000
Epoch: 15   Loss: 0.00074   Count: 50000
Epoch: 16   Loss: 0.00076   Count: 50000
Epoch: 17   Loss: 0.00075   Count: 50000
Epoch: 18   Loss: 0.00074   Count: 50000
Epoch: 19   Loss: 0.00075   Count: 50000
Epoch: 20   Loss: 0.00075   Count: 50000
Epoch: 21   Loss: 0.00073   Count: 50000
Epoch: 22   Loss: 0.00074   Count: 50000
Epoch: 23   Loss: 0.00072   Count: 50000
Epoch: 24   Loss: 0.00073   Count: 50000
Epoch: 25   Loss: 0.00072   Count: 50000
Epoch: 26   Loss: 0.00073   Count: 50000
Epoch: 27   Loss: 0.00073   Count: 50000
Epoch: 28   Loss: 0.00074   Count: 50000
Epoch: 29   Loss: 0.00073   Count: 50000
Finished Training
Generated 1000 examples with variance 0.082325
Accuracy: 87.59 percent
Mean Square Error: 0.044224
Signal-to-Distorion Ratio: 2.699 [dB]
Generated 1000 examples with variance 0.084759
SNR: -5 	 SDR: -0.14235 	 MSE: 0.08758 	 Accuracy: 79.85 percent
Generated 1000 examples with variance 0.085514
SNR: -4 	 SDR: 0.27144 	 MSE: 0.08033 	 Accuracy: 81.44 percent
Generated 1000 examples with variance 0.082902
SNR: -3 	 SDR: 0.89708 	 MSE: 0.06743 	 Accuracy: 83.87 percent
Generated 1000 examples with variance 0.082503
SNR: -2 	 SDR: 1.81781 	 MSE: 0.05429 	 Accuracy: 85.97 percent
Generated 1000 examples with variance 0.084880
SNR: -1 	 SDR: 2.28776 	 MSE: 0.05012 	 Accuracy: 87.00 percent
Generated 1000 examples with variance 0.083795
SNR: 0 	 SDR: 3.25973 	 MSE: 0.03956 	 Accuracy: 89.21 percent
Generated 1000 examples with variance 0.082621
SNR: 1 	 SDR: 4.67843 	 MSE: 0.02814 	 Accuracy: 91.12 percent
Generated 1000 examples with variance 0.081414
SNR: 2 	 SDR: 6.24615 	 MSE: 0.01932 	 Accuracy: 93.08 percent
Generated 1000 examples with variance 0.085512
SNR: 3 	 SDR: 7.91911 	 MSE: 0.01381 	 Accuracy: 94.15 percent
Generated 1000 examples with variance 0.082503
SNR: 4 	 SDR: 9.61405 	 MSE: 0.00902 	 Accuracy: 95.46 percent
Generated 1000 examples with variance 0.078444
SNR: 5 	 SDR: 10.98704 	 MSE: 0.00625 	 Accuracy: 96.19 percent
Generated 1000 examples with variance 0.085519
SNR: 6 	 SDR: 16.12535 	 MSE: 0.00209 	 Accuracy: 97.00 percent
Generated 1000 examples with variance 0.083711
SNR: 7 	 SDR: 18.56959 	 MSE: 0.00116 	 Accuracy: 97.63 percent
Generated 1000 examples with variance 0.082140
SNR: 8 	 SDR: 20.60619 	 MSE: 0.00071 	 Accuracy: 97.89 percent
Generated 1000 examples with variance 0.081410
SNR: 9 	 SDR: 21.25365 	 MSE: 0.00061 	 Accuracy: 98.04 percent
Generated 1000 examples with variance 0.082369
SNR: 10 	 SDR: 22.21365 	 MSE: 0.00049 	 Accuracy: 98.20 percent
res: [-0.14234782138880284, 0.2714384078520737, 0.8970754884917553, 1.8178093775844797, 2.287762038213541, 3.259733472256964, 4.678425887485453, 6.246145013336286, 7.919107188472651, 9.614050181013933, 10.987042060957624, 16.125354565057464, 18.56958728266057, 20.606192459148613, 21.253648527891396, 22.2136518817318]
Modulation Order: 6
SNR Range [dB]: [-5,10]
Generated 50000 examples with variance 0.083579
Epoch: 0   Loss: 0.01968   Count: 50000
Epoch: 1   Loss: 0.01555   Count: 50000
Epoch: 2   Loss: 0.01497   Count: 50000
Epoch: 3   Loss: 0.01476   Count: 50000
Epoch: 4   Loss: 0.01466   Count: 50000
Epoch: 5   Loss: 0.01457   Count: 50000
Epoch: 6   Loss: 0.01444   Count: 50000
Epoch: 7   Loss: 0.01445   Count: 50000
Epoch: 8   Loss: 0.01437   Count: 50000
Epoch: 9   Loss: 0.01442   Count: 50000
Epoch: 10   Loss: 0.01448   Count: 50000
Epoch: 11   Loss: 0.01439   Count: 50000
Epoch: 12   Loss: 0.01456   Count: 50000
Epoch: 13   Loss: 0.01443   Count: 50000
Epoch: 14   Loss: 0.01452   Count: 50000
Epoch: 15   Loss: 0.01439   Count: 50000
Epoch: 16   Loss: 0.01450   Count: 50000
Epoch: 17   Loss: 0.01449   Count: 50000
Epoch: 18   Loss: 0.01442   Count: 50000
Epoch: 19   Loss: 0.01444   Count: 50000
Epoch: 20   Loss: 0.01444   Count: 50000
Epoch: 21   Loss: 0.01448   Count: 50000
Epoch: 22   Loss: 0.01441   Count: 50000
Epoch: 23   Loss: 0.01436   Count: 50000
Epoch: 24   Loss: 0.01432   Count: 50000
Epoch: 25   Loss: 0.01443   Count: 50000
Epoch: 26   Loss: 0.01436   Count: 50000
Epoch: 27   Loss: 0.01435   Count: 50000
Epoch: 28   Loss: 0.01430   Count: 50000
Epoch: 29   Loss: 0.01437   Count: 50000
Finished Training
Generated 1000 examples with variance 0.085526
Accuracy: 88.60 percent
Mean Square Error: 0.036397
Signal-to-Distorion Ratio: 3.710 [dB]
Generated 1000 examples with variance 0.082211
SNR: -5 	 SDR: 0.93742 	 MSE: 0.06625 	 Accuracy: 82.49 percent
Generated 1000 examples with variance 0.085258
SNR: -4 	 SDR: 1.55717 	 MSE: 0.05957 	 Accuracy: 83.69 percent
Generated 1000 examples with variance 0.081402
SNR: -3 	 SDR: 1.94719 	 MSE: 0.05199 	 Accuracy: 85.44 percent
Generated 1000 examples with variance 0.081508
SNR: -2 	 SDR: 2.83079 	 MSE: 0.04247 	 Accuracy: 87.22 percent
Generated 1000 examples with variance 0.084095
SNR: -1 	 SDR: 3.76727 	 MSE: 0.03532 	 Accuracy: 89.13 percent
Generated 1000 examples with variance 0.083987
SNR: 0 	 SDR: 6.14790 	 MSE: 0.02039 	 Accuracy: 91.79 percent
Generated 1000 examples with variance 0.085355
SNR: 1 	 SDR: 6.90999 	 MSE: 0.01739 	 Accuracy: 92.88 percent
Generated 1000 examples with variance 0.081678
SNR: 2 	 SDR: 9.12925 	 MSE: 0.00998 	 Accuracy: 94.73 percent
Generated 1000 examples with variance 0.080610
SNR: 3 	 SDR: 10.67263 	 MSE: 0.00690 	 Accuracy: 95.50 percent
Generated 1000 examples with variance 0.084972
SNR: 4 	 SDR: 11.50057 	 MSE: 0.00601 	 Accuracy: 95.97 percent
Generated 1000 examples with variance 0.084620
SNR: 5 	 SDR: 14.78111 	 MSE: 0.00281 	 Accuracy: 96.87 percent
Generated 1000 examples with variance 0.086240
SNR: 6 	 SDR: 18.07601 	 MSE: 0.00134 	 Accuracy: 97.47 percent
Generated 1000 examples with variance 0.083561
SNR: 7 	 SDR: 19.67078 	 MSE: 0.00090 	 Accuracy: 97.72 percent
Generated 1000 examples with variance 0.084846
SNR: 8 	 SDR: 20.45164 	 MSE: 0.00076 	 Accuracy: 97.85 percent
Generated 1000 examples with variance 0.080942
SNR: 9 	 SDR: 20.35819 	 MSE: 0.00075 	 Accuracy: 97.85 percent
Generated 1000 examples with variance 0.082346
SNR: 10 	 SDR: 20.95755 	 MSE: 0.00066 	 Accuracy: 98.02 percent
res: [0.9374153143509989, 1.5571736233579365, 1.9471853555878855, 2.8307885998795417, 3.7672651457324893, 6.1478980452018686, 6.909989060535528, 9.129245708603772, 10.672630802213236, 11.500573368732194, 14.781107730323553, 18.076013577098024, 19.670783304031286, 20.451642209016818, 20.358189266517158, 20.957551417874583]
Modulation Order: 8
SNR Range [dB]: [-5,10]
Generated 50000 examples with variance 0.083452
Epoch: 0   Loss: 0.03210   Count: 50000
Epoch: 1   Loss: 0.02555   Count: 50000
Epoch: 2   Loss: 0.02439   Count: 50000
Epoch: 3   Loss: 0.02428   Count: 50000
Epoch: 4   Loss: 0.02408   Count: 50000
Epoch: 5   Loss: 0.02385   Count: 50000
Epoch: 6   Loss: 0.02364   Count: 50000
Epoch: 7   Loss: 0.02364   Count: 50000
Epoch: 8   Loss: 0.02340   Count: 50000
Epoch: 9   Loss: 0.02336   Count: 50000
Epoch: 10   Loss: 0.02332   Count: 50000
Epoch: 11   Loss: 0.02338   Count: 50000
Epoch: 12   Loss: 0.02313   Count: 50000
Epoch: 13   Loss: 0.02306   Count: 50000
Epoch: 14   Loss: 0.02305   Count: 50000
Epoch: 15   Loss: 0.02299   Count: 50000
Epoch: 16   Loss: 0.02311   Count: 50000
Epoch: 17   Loss: 0.02297   Count: 50000
Epoch: 18   Loss: 0.02307   Count: 50000
Epoch: 19   Loss: 0.02300   Count: 50000
Epoch: 20   Loss: 0.02303   Count: 50000
Epoch: 21   Loss: 0.02300   Count: 50000
Epoch: 22   Loss: 0.02290   Count: 50000
Epoch: 23   Loss: 0.02300   Count: 50000
Epoch: 24   Loss: 0.02280   Count: 50000
Epoch: 25   Loss: 0.02296   Count: 50000
Epoch: 26   Loss: 0.02296   Count: 50000
Epoch: 27   Loss: 0.02290   Count: 50000
Epoch: 28   Loss: 0.02282   Count: 50000
Epoch: 29   Loss: 0.02285   Count: 50000
Finished Training
Generated 1000 examples with variance 0.084381
Accuracy: 97.40 percent
Mean Square Error: 0.001074
Signal-to-Distorion Ratio: 18.952 [dB]
Generated 1000 examples with variance 0.083652
SNR: -5 	 SDR: 0.98259 	 MSE: 0.06671 	 Accuracy: 82.35 percent
Generated 1000 examples with variance 0.081451
SNR: -4 	 SDR: 1.43658 	 MSE: 0.05851 	 Accuracy: 83.57 percent
Generated 1000 examples with variance 0.080369
SNR: -3 	 SDR: 2.02679 	 MSE: 0.05040 	 Accuracy: 85.54 percent
Generated 1000 examples with variance 0.081582
SNR: -2 	 SDR: 3.62005 	 MSE: 0.03545 	 Accuracy: 88.34 percent
Generated 1000 examples with variance 0.084184
SNR: -1 	 SDR: 4.79979 	 MSE: 0.02788 	 Accuracy: 90.09 percent
Generated 1000 examples with variance 0.082378
SNR: 0 	 SDR: 6.64504 	 MSE: 0.01784 	 Accuracy: 92.00 percent
Generated 1000 examples with variance 0.082385
SNR: 1 	 SDR: 7.62525 	 MSE: 0.01423 	 Accuracy: 93.24 percent
Generated 1000 examples with variance 0.082758
SNR: 2 	 SDR: 9.09693 	 MSE: 0.01019 	 Accuracy: 94.44 percent
Generated 1000 examples with variance 0.083848
SNR: 3 	 SDR: 11.65333 	 MSE: 0.00573 	 Accuracy: 95.73 percent
Generated 1000 examples with variance 0.083076
SNR: 4 	 SDR: 14.11635 	 MSE: 0.00322 	 Accuracy: 96.51 percent
Generated 1000 examples with variance 0.078708
SNR: 5 	 SDR: 16.07989 	 MSE: 0.00194 	 Accuracy: 97.08 percent
Generated 1000 examples with variance 0.085315
SNR: 6 	 SDR: 17.82410 	 MSE: 0.00141 	 Accuracy: 97.40 percent
Generated 1000 examples with variance 0.081425
SNR: 7 	 SDR: 19.53835 	 MSE: 0.00091 	 Accuracy: 97.51 percent
Generated 1000 examples with variance 0.081839
SNR: 8 	 SDR: 19.32164 	 MSE: 0.00096 	 Accuracy: 97.49 percent
Generated 1000 examples with variance 0.083778
SNR: 9 	 SDR: 19.86229 	 MSE: 0.00086 	 Accuracy: 97.55 percent
Generated 1000 examples with variance 0.083137
SNR: 10 	 SDR: 20.05609 	 MSE: 0.00082 	 Accuracy: 97.61 percent
res: [0.9825898258131374, 1.436584055505162, 2.026789173175473, 3.6200482154789166, 4.799789606556116, 6.645043169021202, 7.625250126162167, 9.096931428700822, 11.653332075266526, 14.116348056289304, 16.079888288853585, 17.824095263167596, 19.538351456149048, 19.32164003385087, 19.86229470882163, 20.05608874949274]
Modulation Order: 20
SNR Range [dB]: [-15,5]
Generated 50000 examples with variance 0.083401
Epoch: 0   Loss: 0.07767   Count: 50000
Epoch: 1   Loss: 0.07698   Count: 50000
Epoch: 2   Loss: 0.07665   Count: 50000
Epoch: 3   Loss: 0.07650   Count: 50000
Epoch: 4   Loss: 0.07617   Count: 50000
Epoch: 5   Loss: 0.07592   Count: 50000
Epoch: 6   Loss: 0.07575   Count: 50000
Epoch: 7   Loss: 0.07552   Count: 50000
Epoch: 8   Loss: 0.07539   Count: 50000
Epoch: 9   Loss: 0.07526   Count: 50000
Epoch: 10   Loss: 0.07515   Count: 50000
Epoch: 11   Loss: 0.07485   Count: 50000
Epoch: 12   Loss: 0.07471   Count: 50000
Epoch: 13   Loss: 0.07431   Count: 50000
Epoch: 14   Loss: 0.07409   Count: 50000
Epoch: 15   Loss: 0.07382   Count: 50000
Epoch: 16   Loss: 0.07369   Count: 50000
Epoch: 17   Loss: 0.07360   Count: 50000
Epoch: 18   Loss: 0.07347   Count: 50000
Epoch: 19   Loss: 0.07330   Count: 50000
Epoch: 20   Loss: 0.07324   Count: 50000
Epoch: 21   Loss: 0.07305   Count: 50000
Epoch: 22   Loss: 0.07291   Count: 50000
Epoch: 23   Loss: 0.07277   Count: 50000
Epoch: 24   Loss: 0.07257   Count: 50000
Epoch: 25   Loss: 0.07251   Count: 50000
Epoch: 26   Loss: 0.07245   Count: 50000
Epoch: 27   Loss: 0.07231   Count: 50000
Epoch: 28   Loss: 0.07221   Count: 50000
Epoch: 29   Loss: 0.07216   Count: 50000
Finished Training
Generated 1000 examples with variance 0.080843
Accuracy: 82.38 percent
Mean Square Error: 0.043451
Signal-to-Distorion Ratio: 2.696 [dB]
Generated 1000 examples with variance 0.086610
SNR: -15 	 SDR: 0.02201 	 MSE: 0.08617 	 Accuracy: 75.19 percent
Generated 1000 examples with variance 0.084076
SNR: -14 	 SDR: -0.01859 	 MSE: 0.08444 	 Accuracy: 75.48 percent
Generated 1000 examples with variance 0.087188
SNR: -13 	 SDR: -0.00655 	 MSE: 0.08732 	 Accuracy: 74.89 percent
Generated 1000 examples with variance 0.080212
SNR: -12 	 SDR: 0.13663 	 MSE: 0.07773 	 Accuracy: 76.57 percent
Generated 1000 examples with variance 0.083060
SNR: -11 	 SDR: 0.33599 	 MSE: 0.07688 	 Accuracy: 76.50 percent
Generated 1000 examples with variance 0.080939
SNR: -10 	 SDR: 0.30695 	 MSE: 0.07542 	 Accuracy: 76.77 percent
Generated 1000 examples with variance 0.086525
SNR: -9 	 SDR: 0.52914 	 MSE: 0.07660 	 Accuracy: 76.45 percent
Generated 1000 examples with variance 0.081383
SNR: -8 	 SDR: 0.82505 	 MSE: 0.06730 	 Accuracy: 78.16 percent
Generated 1000 examples with variance 0.080020
SNR: -7 	 SDR: 0.83583 	 MSE: 0.06601 	 Accuracy: 78.57 percent
Generated 1000 examples with variance 0.082444
SNR: -6 	 SDR: 1.14338 	 MSE: 0.06336 	 Accuracy: 78.89 percent
Generated 1000 examples with variance 0.085644
SNR: -5 	 SDR: 1.35660 	 MSE: 0.06267 	 Accuracy: 78.97 percent
Generated 1000 examples with variance 0.080574
SNR: -4 	 SDR: 1.92275 	 MSE: 0.05175 	 Accuracy: 80.97 percent
Generated 1000 examples with variance 0.081892
SNR: -3 	 SDR: 2.59183 	 MSE: 0.04509 	 Accuracy: 82.09 percent
Generated 1000 examples with variance 0.084492
SNR: -2 	 SDR: 2.78343 	 MSE: 0.04451 	 Accuracy: 82.25 percent
Generated 1000 examples with variance 0.083423
SNR: -1 	 SDR: 3.39164 	 MSE: 0.03820 	 Accuracy: 83.35 percent
Generated 1000 examples with variance 0.081307
SNR: 0 	 SDR: 4.09143 	 MSE: 0.03169 	 Accuracy: 84.52 percent
Generated 1000 examples with variance 0.085909
SNR: 1 	 SDR: 4.77175 	 MSE: 0.02863 	 Accuracy: 85.40 percent
Generated 1000 examples with variance 0.080368
SNR: 2 	 SDR: 5.16365 	 MSE: 0.02447 	 Accuracy: 86.47 percent
Generated 1000 examples with variance 0.083852
SNR: 3 	 SDR: 5.67627 	 MSE: 0.02269 	 Accuracy: 86.93 percent
Generated 1000 examples with variance 0.082966
SNR: 4 	 SDR: 5.94843 	 MSE: 0.02109 	 Accuracy: 87.20 percent
Generated 1000 examples with variance 0.083448
SNR: 5 	 SDR: 6.23868 	 MSE: 0.01984 	 Accuracy: 87.68 percent
res: [0.0220085591306363, -0.018591792341743362, -0.0065481443476040745, 0.13663491816847187, 0.3359919545391157, 0.30694873707293424, 0.5291403264041796, 0.8250450653652042, 0.8358348636999356, 1.1433793298253891, 1.3565997706684967, 1.9227540109597652, 2.591832213354908, 2.783427550792237, 3.3916397448925357, 4.091429508979793, 4.7717517787327, 5.16365059585058, 5.676266076174388, 5.948427980757618, 6.238676829417425]
Modulation Order: 40
SNR Range [dB]: [-15,5]
Generated 50000 examples with variance 0.083099
