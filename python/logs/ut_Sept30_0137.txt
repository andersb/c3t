Device: cuda
AngleOnly
Modulation Order: 4
SNR Range [dB]: [-5,10]
Generated 50000 examples with variance 0.084582
Epoch: 0   Loss: 0.01818   Count: 50000
Epoch: 1   Loss: 0.01716   Count: 50000
Epoch: 2   Loss: 0.01695   Count: 50000
Epoch: 3   Loss: 0.01687   Count: 50000
Epoch: 4   Loss: 0.01682   Count: 50000
Epoch: 5   Loss: 0.01679   Count: 50000
Epoch: 6   Loss: 0.01675   Count: 50000
Epoch: 7   Loss: 0.01673   Count: 50000
Epoch: 8   Loss: 0.01670   Count: 50000
Epoch: 9   Loss: 0.01670   Count: 50000
Epoch: 10   Loss: 0.01668   Count: 50000
Epoch: 11   Loss: 0.01667   Count: 50000
Epoch: 12   Loss: 0.01666   Count: 50000
Epoch: 13   Loss: 0.01666   Count: 50000
Epoch: 14   Loss: 0.01665   Count: 50000
Epoch: 15   Loss: 0.01664   Count: 50000
Epoch: 16   Loss: 0.01663   Count: 50000
Epoch: 17   Loss: 0.01663   Count: 50000
Epoch: 18   Loss: 0.01662   Count: 50000
Epoch: 19   Loss: 0.01662   Count: 50000
Epoch: 20   Loss: 0.01661   Count: 50000
Epoch: 21   Loss: 0.01660   Count: 50000
Epoch: 22   Loss: 0.01660   Count: 50000
Epoch: 23   Loss: 0.01660   Count: 50000
Epoch: 24   Loss: 0.01659   Count: 50000
Epoch: 25   Loss: 0.01660   Count: 50000
Epoch: 26   Loss: 0.01659   Count: 50000
Epoch: 27   Loss: 0.01659   Count: 50000
Epoch: 28   Loss: 0.01658   Count: 50000
Epoch: 29   Loss: 0.01658   Count: 50000
Epoch: 30   Loss: 0.01658   Count: 50000
Epoch: 31   Loss: 0.01657   Count: 50000
Epoch: 32   Loss: 0.01658   Count: 50000
Epoch: 33   Loss: 0.01657   Count: 50000
Epoch: 34   Loss: 0.01657   Count: 50000
Epoch: 35   Loss: 0.01657   Count: 50000
Epoch: 36   Loss: 0.01656   Count: 50000
Epoch: 37   Loss: 0.01657   Count: 50000
Epoch: 38   Loss: 0.01656   Count: 50000
Epoch: 39   Loss: 0.01656   Count: 50000
Epoch: 40   Loss: 0.01656   Count: 50000
Epoch: 41   Loss: 0.01656   Count: 50000
Epoch: 42   Loss: 0.01655   Count: 50000
Epoch: 43   Loss: 0.01656   Count: 50000
Epoch: 44   Loss: 0.01656   Count: 50000
Epoch: 45   Loss: 0.01655   Count: 50000
Epoch: 46   Loss: 0.01655   Count: 50000
Epoch: 47   Loss: 0.01655   Count: 50000
Epoch: 48   Loss: 0.01655   Count: 50000
Epoch: 49   Loss: 0.01655   Count: 50000
Finished Training
Generated 1000 examples with variance 0.061136
Accuracy: 91.69 percent
Mean Square Error: 0.016340
Signal-to-Distorion Ratio: 5.730 [dB]
Generated 1000 examples with variance 0.063388
SNR: -5 	 SDR: 1.22188 	 MSE: 0.04784 	 Accuracy: 83.76 percent
Generated 1000 examples with variance 0.065804
SNR: -4 	 SDR: 1.87568 	 MSE: 0.04273 	 Accuracy: 84.82 percent
Generated 1000 examples with variance 0.069725
SNR: -3 	 SDR: 2.72444 	 MSE: 0.03723 	 Accuracy: 85.99 percent
Generated 1000 examples with variance 0.066534
SNR: -2 	 SDR: 3.18745 	 MSE: 0.03194 	 Accuracy: 87.16 percent
Generated 1000 examples with variance 0.069710
SNR: -1 	 SDR: 4.15645 	 MSE: 0.02677 	 Accuracy: 88.37 percent
Generated 1000 examples with variance 0.066918
SNR: 0 	 SDR: 4.88276 	 MSE: 0.02174 	 Accuracy: 89.60 percent
Generated 1000 examples with variance 0.069974
SNR: 1 	 SDR: 6.11741 	 MSE: 0.01711 	 Accuracy: 90.80 percent
Generated 1000 examples with variance 0.066744
SNR: 2 	 SDR: 7.05905 	 MSE: 0.01314 	 Accuracy: 91.92 percent
Generated 1000 examples with variance 0.069754
SNR: 3 	 SDR: 8.54058 	 MSE: 0.00976 	 Accuracy: 92.96 percent
Generated 1000 examples with variance 0.067370
SNR: 4 	 SDR: 9.70267 	 MSE: 0.00721 	 Accuracy: 93.85 percent
Generated 1000 examples with variance 0.072008
SNR: 5 	 SDR: 11.36881 	 MSE: 0.00525 	 Accuracy: 94.63 percent
Generated 1000 examples with variance 0.065681
SNR: 6 	 SDR: 12.30589 	 MSE: 0.00386 	 Accuracy: 95.27 percent
Generated 1000 examples with variance 0.072547
SNR: 7 	 SDR: 13.93996 	 MSE: 0.00293 	 Accuracy: 95.79 percent
Generated 1000 examples with variance 0.067455
SNR: 8 	 SDR: 14.71370 	 MSE: 0.00228 	 Accuracy: 96.23 percent
Generated 1000 examples with variance 0.070178
SNR: 9 	 SDR: 15.82806 	 MSE: 0.00183 	 Accuracy: 96.59 percent
Generated 1000 examples with variance 0.069535
SNR: 10 	 SDR: 16.57169 	 MSE: 0.00153 	 Accuracy: 96.87 percent
res: [1.2218840566944595, 1.8756771373888963, 2.7244430647674367, 3.1874522408132844, 4.156453269862157, 4.882763045062819, 6.117414808974411, 7.05904893869956, 8.540578201212208, 9.702671185605507, 11.368811294740983, 12.30589172688353, 13.939963688170742, 14.713699715878263, 15.828059059884815, 16.571685088780047]
