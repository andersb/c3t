Device: cuda
Learning Rate: 0.0001
SNR Training Levels [dB]: [10  5  0 -5]
SNR Eval Range [dB]: [-10,10]
Feature Type:  RawAO
Feature Dimension:  60
Modulation Order:  40
Stretching Function: pi * 1.0
Generated 15000 examples with SNR 10 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR -5 dB
The source variance is 0.331493
Training with 60000 examples for 50 epochs
Epoch: 0   Loss: 1.122e-01   Count: 60000
loss_counter: 1
Average change in loss: 0.000000e+00
Relative change in loss: 0.000000e+00
Epoch: 1   Loss: 8.217e-02   Count: 60000
loss_counter: 2
Average change in loss: 1.502301e-02
Relative change in loss: 1.828326e-01
Epoch: 2   Loss: 7.271e-02   Count: 60000
loss_counter: 3
Average change in loss: 1.316773e-02
Relative change in loss: 1.810969e-01
Epoch: 3   Loss: 6.487e-02   Count: 60000
loss_counter: 4
Average change in loss: 1.183593e-02
Relative change in loss: 1.824548e-01
Epoch: 4   Loss: 5.884e-02   Count: 60000
loss_counter: 5
Average change in loss: 1.067525e-02
Relative change in loss: 1.814349e-01
Epoch: 5   Loss: 5.232e-02   Count: 60000
loss_counter: 6
Average change in loss: 9.982604e-03
Relative change in loss: 1.908044e-01
Epoch: 6   Loss: 4.747e-02   Count: 60000
loss_counter: 7
Average change in loss: 9.249225e-03
Relative change in loss: 1.948453e-01
Epoch: 7   Loss: 4.304e-02   Count: 60000
loss_counter: 8
Average change in loss: 8.646201e-03
Relative change in loss: 2.008663e-01
Epoch: 8   Loss: 3.878e-02   Count: 60000
loss_counter: 9
Average change in loss: 8.159426e-03
Relative change in loss: 2.104066e-01
Epoch: 9   Loss: 3.470e-02   Count: 60000
loss_counter: 10
Average change in loss: 7.750999e-03
Relative change in loss: 2.233449e-01
Epoch: 10   Loss: 3.125e-02   Count: 60000
loss_counter: 11
Average change in loss: 7.360153e-03
Relative change in loss: 2.355062e-01
Epoch: 11   Loss: 2.838e-02   Count: 60000
loss_counter: 12
Average change in loss: 6.986431e-03
Relative change in loss: 2.462006e-01
Epoch: 12   Loss: 2.507e-02   Count: 60000
loss_counter: 13
Average change in loss: 6.703714e-03
Relative change in loss: 2.674438e-01
Epoch: 13   Loss: 2.280e-02   Count: 60000
loss_counter: 14
Average change in loss: 6.386459e-03
Relative change in loss: 2.800620e-01
Epoch: 14   Loss: 2.083e-02   Count: 60000
loss_counter: 15
Average change in loss: 6.092166e-03
Relative change in loss: 2.924474e-01
Epoch: 15   Loss: 1.822e-02   Count: 60000
loss_counter: 16
Average change in loss: 5.874924e-03
Relative change in loss: 3.225255e-01
Epoch: 16   Loss: 1.669e-02   Count: 60000
loss_counter: 17
Average change in loss: 5.619212e-03
Relative change in loss: 3.367307e-01
Epoch: 17   Loss: 1.561e-02   Count: 60000
loss_counter: 18
Average change in loss: 5.366695e-03
Relative change in loss: 3.437183e-01
Epoch: 18   Loss: 1.419e-02   Count: 60000
loss_counter: 19
Average change in loss: 5.159337e-03
Relative change in loss: 3.636726e-01
Epoch: 19   Loss: 1.306e-02   Count: 60000
loss_counter: 20
Average change in loss: 4.957677e-03
Relative change in loss: 3.795895e-01
Epoch: 20   Loss: 1.245e-02   Count: 60000
loss_counter: 21
Average change in loss: 4.750583e-03
Relative change in loss: 3.815139e-01
Epoch: 21   Loss: 1.121e-02   Count: 60000
loss_counter: 22
Average change in loss: 4.590899e-03
Relative change in loss: 4.093764e-01
Epoch: 22   Loss: 1.067e-02   Count: 60000
loss_counter: 23
Average change in loss: 4.415163e-03
Relative change in loss: 4.139705e-01
Epoch: 23   Loss: 9.979e-03   Count: 60000
loss_counter: 24
Average change in loss: 4.259816e-03
Relative change in loss: 4.268958e-01
Epoch: 24   Loss: 9.479e-03   Count: 60000
loss_counter: 25
Average change in loss: 4.109409e-03
Relative change in loss: 4.335312e-01
Epoch: 25   Loss: 8.724e-03   Count: 60000
loss_counter: 26
Average change in loss: 3.980396e-03
Relative change in loss: 4.562656e-01
Epoch: 26   Loss: 8.652e-03   Count: 60000
loss_counter: 27
Average change in loss: 3.835638e-03
Relative change in loss: 4.433279e-01
Epoch: 27   Loss: 7.964e-03   Count: 60000
loss_counter: 28
Average change in loss: 3.723215e-03
Relative change in loss: 4.674967e-01
Epoch: 28   Loss: 7.858e-03   Count: 60000
loss_counter: 29
Average change in loss: 3.598504e-03
Relative change in loss: 4.579677e-01
Epoch: 29   Loss: 7.340e-03   Count: 60000
loss_counter: 30
Average change in loss: 3.495806e-03
Relative change in loss: 4.762694e-01
Epoch: 30   Loss: 7.156e-03   Count: 60000
loss_counter: 31
Average change in loss: 3.388971e-03
Relative change in loss: 4.735812e-01
Epoch: 31   Loss: 6.977e-03   Count: 60000
loss_counter: 32
Average change in loss: 3.288663e-03
Relative change in loss: 4.713614e-01
Epoch: 32   Loss: 6.783e-03   Count: 60000
loss_counter: 33
Average change in loss: 3.194887e-03
Relative change in loss: 4.710226e-01
Epoch: 33   Loss: 6.392e-03   Count: 60000
loss_counter: 34
Average change in loss: 3.112427e-03
Relative change in loss: 4.869540e-01
Epoch: 34   Loss: 6.356e-03   Count: 60000
loss_counter: 35
Average change in loss: 3.024513e-03
Relative change in loss: 4.758357e-01
Epoch: 35   Loss: 6.080e-03   Count: 60000
loss_counter: 36
Average change in loss: 2.948177e-03
Relative change in loss: 4.849130e-01
Epoch: 36   Loss: 5.805e-03   Count: 60000
loss_counter: 37
Average change in loss: 2.875937e-03
Relative change in loss: 4.954659e-01
Epoch: 37   Loss: 5.827e-03   Count: 60000
loss_counter: 38
Average change in loss: 2.799657e-03
Relative change in loss: 4.804456e-01
Epoch: 38   Loss: 5.616e-03   Count: 60000
loss_counter: 39
Average change in loss: 2.733283e-03
Relative change in loss: 4.866858e-01
Epoch: 39   Loss: 5.528e-03   Count: 60000
loss_counter: 40
Average change in loss: 2.667162e-03
Relative change in loss: 4.825087e-01
Epoch: 40   Loss: 5.350e-03   Count: 60000
loss_counter: 41
Average change in loss: 2.606448e-03
Relative change in loss: 4.872043e-01
Epoch: 41   Loss: 5.263e-03   Count: 60000
loss_counter: 42
Average change in loss: 2.546454e-03
Relative change in loss: 4.838326e-01
Epoch: 42   Loss: 5.206e-03   Count: 60000
loss_counter: 43
Average change in loss: 2.488556e-03
Relative change in loss: 4.779924e-01
Epoch: 43   Loss: 5.058e-03   Count: 60000
loss_counter: 44
Average change in loss: 2.435360e-03
Relative change in loss: 4.814572e-01
Epoch: 44   Loss: 4.987e-03   Count: 60000
loss_counter: 45
Average change in loss: 2.382826e-03
Relative change in loss: 4.778075e-01
Epoch: 45   Loss: 4.974e-03   Count: 60000
loss_counter: 46
Average change in loss: 2.331303e-03
Relative change in loss: 4.686785e-01
Epoch: 46   Loss: 4.722e-03   Count: 60000
loss_counter: 47
Average change in loss: 2.287073e-03
Relative change in loss: 4.843702e-01
Epoch: 47   Loss: 4.665e-03   Count: 60000
loss_counter: 48
Average change in loss: 2.240600e-03
Relative change in loss: 4.802607e-01
Epoch: 48   Loss: 4.655e-03   Count: 60000
loss_counter: 49
Average change in loss: 2.195084e-03
Relative change in loss: 4.715520e-01
Epoch: 49   Loss: 4.586e-03   Count: 60000
loss_counter: 50
Average change in loss: 2.152562e-03
Relative change in loss: 4.693704e-01
Finished Training
Generated 10000 examples with SNR -10 dB
Generated 10000 examples with SNR -9 dB
Generated 10000 examples with SNR -8 dB
Generated 10000 examples with SNR -7 dB
Generated 10000 examples with SNR -6 dB
Generated 10000 examples with SNR -5 dB
Generated 10000 examples with SNR -4 dB
Generated 10000 examples with SNR -3 dB
Generated 10000 examples with SNR -2 dB
Generated 10000 examples with SNR -1 dB
Generated 10000 examples with SNR 0 dB
Generated 10000 examples with SNR 1 dB
Generated 10000 examples with SNR 2 dB
Generated 10000 examples with SNR 3 dB
Generated 10000 examples with SNR 4 dB
Generated 10000 examples with SNR 5 dB
Generated 10000 examples with SNR 6 dB
Generated 10000 examples with SNR 7 dB
Generated 10000 examples with SNR 8 dB
Generated 10000 examples with SNR 9 dB
Generated 10000 examples with SNR 10 dB
The source variance is 0.332796
Accuracy: 84.59 percent
Mean Square Error: 0.103132
Signal-to-Distorion Ratio: 5.088 [dB]
Generated 10000 examples with SNR -10 dB
The source variance is 0.334305
SNR: -10 	 SDR: -0.63397 	 MSE: 0.38685 	 Accuracy: 54.00 percent
Generated 10000 examples with SNR -9 dB
The source variance is 0.332279
SNR: -9 	 SDR: -0.29011 	 MSE: 0.35523 	 Accuracy: 57.35 percent
Generated 10000 examples with SNR -8 dB
The source variance is 0.327001
SNR: -8 	 SDR: 0.13162 	 MSE: 0.31724 	 Accuracy: 61.00 percent
Generated 10000 examples with SNR -7 dB
The source variance is 0.330409
SNR: -7 	 SDR: 0.60568 	 MSE: 0.28740 	 Accuracy: 64.30 percent
Generated 10000 examples with SNR -6 dB
The source variance is 0.335466
SNR: -6 	 SDR: 1.54266 	 MSE: 0.23517 	 Accuracy: 69.28 percent
Generated 10000 examples with SNR -5 dB
The source variance is 0.330568
SNR: -5 	 SDR: 2.47579 	 MSE: 0.18693 	 Accuracy: 74.18 percent
Generated 10000 examples with SNR -4 dB
The source variance is 0.337966
SNR: -4 	 SDR: 3.77504 	 MSE: 0.14170 	 Accuracy: 79.36 percent
Generated 10000 examples with SNR -3 dB
The source variance is 0.331996
SNR: -3 	 SDR: 5.12003 	 MSE: 0.10212 	 Accuracy: 83.47 percent
Generated 10000 examples with SNR -2 dB
The source variance is 0.326834
SNR: -2 	 SDR: 7.24984 	 MSE: 0.06157 	 Accuracy: 88.01 percent
Generated 10000 examples with SNR -1 dB
The source variance is 0.340066
SNR: -1 	 SDR: 9.69720 	 MSE: 0.03646 	 Accuracy: 91.25 percent
Generated 10000 examples with SNR 0 dB
The source variance is 0.334107
SNR: 0 	 SDR: 12.88005 	 MSE: 0.01721 	 Accuracy: 93.71 percent
Generated 10000 examples with SNR 1 dB
The source variance is 0.328795
SNR: 1 	 SDR: 16.17478 	 MSE: 0.00793 	 Accuracy: 95.01 percent
Generated 10000 examples with SNR 2 dB
The source variance is 0.339795
SNR: 2 	 SDR: 19.10428 	 MSE: 0.00418 	 Accuracy: 95.72 percent
Generated 10000 examples with SNR 3 dB
The source variance is 0.337968
SNR: 3 	 SDR: 20.78675 	 MSE: 0.00282 	 Accuracy: 96.05 percent
Generated 10000 examples with SNR 4 dB
The source variance is 0.338844
SNR: 4 	 SDR: 22.50693 	 MSE: 0.00190 	 Accuracy: 96.21 percent
Generated 10000 examples with SNR 5 dB
The source variance is 0.328253
SNR: 5 	 SDR: 22.62647 	 MSE: 0.00179 	 Accuracy: 96.26 percent
Generated 10000 examples with SNR 6 dB
The source variance is 0.336480
SNR: 6 	 SDR: 22.85943 	 MSE: 0.00174 	 Accuracy: 96.27 percent
Generated 10000 examples with SNR 7 dB
The source variance is 0.338152
SNR: 7 	 SDR: 22.95383 	 MSE: 0.00171 	 Accuracy: 96.26 percent
Generated 10000 examples with SNR 8 dB
The source variance is 0.337166
SNR: 8 	 SDR: 23.00886 	 MSE: 0.00169 	 Accuracy: 96.28 percent
Generated 10000 examples with SNR 9 dB
The source variance is 0.340470
SNR: 9 	 SDR: 23.01461 	 MSE: 0.00170 	 Accuracy: 96.25 percent
Generated 10000 examples with SNR 10 dB
The source variance is 0.330137
SNR: 10 	 SDR: 22.85906 	 MSE: 0.00171 	 Accuracy: 96.23 percent
res: [-0.6339694077645837, -0.2901070615948764, 0.1316210342205013, 0.6056753842124909, 1.5426596584821923, 2.475786132580506, 3.7750371780369396, 5.1200292469938, 7.249841773295863, 9.69719856311609, 12.88004675672424, 16.17477855179469, 19.10427830376395, 20.786752256956206, 22.5069313811558, 22.626470812738305, 22.859434856937977, 22.953826491878395, 23.008857190405006, 23.01461107793985, 22.859061556196142]
[0.12382308, 0.10847692, 0.15717168, 0.17994176, 0.06367002, 0.01645952, 0.17460105, 0.13555256, 0.12844574, 0.11499835, 0.11685306, 0.19139462, 0.20346245, 0.12069767, 0.20684936, 0.05684767, 0.20472595, 0.03709949, 0.10415552, 0.08590632, 0.22643255, 0.21105967, 0.19915536, 0.16115404, 0.03118854, 0.18561472, 0.12057285, 0.05374938, 0.17488107, 0.17700132, 0.15179817, 0.20157967, 0.13781863, 0.0384858, 0.14233394, 0.1544325, 0.12247276, 0.06843885, 0.15669923, 0.16760792, 0.17626555, 0.17180191, 0.06182777, 0.10257803, 0.08054303, 0.13447065, 0.1068071, 0.12692424, 0.11186648, 0.05276959]
Log is written to logs/log_n100_RawAO.txt
