Device: cpu
Learning Rate: 1e-06
SNR Training Levels [dB]: [-5  0  5 10]
SNR Eval Range [dB]: [-10,10]
Feature Type:  AO
Feature Dimension:  3
Modulation Order:  2
Stretching Function: pi * 1.0
Generated 15000 examples with SNR -5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 10 dB
The source variance is 0.333803
Training with 60000 examples for 50 epochs
