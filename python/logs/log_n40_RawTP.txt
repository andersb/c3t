Device: cuda
Learning Rate: 0.0001
SNR Training Levels [dB]: [10  5  0 -5]
SNR Eval Range [dB]: [-10,10]
Feature Type:  RawTP
Feature Dimension:  80
Modulation Order:  40
Stretching Function: pi * 1.0
Generated 15000 examples with SNR 10 dB
Generated 15000 examples with SNR 5 dB
Generated 15000 examples with SNR 0 dB
Generated 15000 examples with SNR -5 dB
The source variance is 0.334012
Training with 60000 examples for 50 epochs
Epoch: 0   Loss: 1.154e-01   Count: 60000
loss_counter: 1
Average change in loss: 0.000000e+00
Relative change in loss: 0.000000e+00
Epoch: 1   Loss: 8.635e-02   Count: 60000
loss_counter: 2
Average change in loss: 1.454116e-02
Relative change in loss: 1.683981e-01
Epoch: 2   Loss: 7.843e-02   Count: 60000
loss_counter: 3
Average change in loss: 1.233512e-02
Relative change in loss: 1.572819e-01
Epoch: 3   Loss: 7.329e-02   Count: 60000
loss_counter: 4
Average change in loss: 1.053511e-02
Relative change in loss: 1.437420e-01
Epoch: 4   Loss: 6.962e-02   Count: 60000
loss_counter: 5
Average change in loss: 9.162686e-03
Relative change in loss: 1.316122e-01
Epoch: 5   Loss: 6.557e-02   Count: 60000
loss_counter: 6
Average change in loss: 8.309934e-03
Relative change in loss: 1.267287e-01
Epoch: 6   Loss: 6.168e-02   Count: 60000
loss_counter: 7
Average change in loss: 7.679125e-03
Relative change in loss: 1.245028e-01
Epoch: 7   Loss: 5.696e-02   Count: 60000
loss_counter: 8
Average change in loss: 7.308967e-03
Relative change in loss: 1.283164e-01
Epoch: 8   Loss: 5.298e-02   Count: 60000
loss_counter: 9
Average change in loss: 6.939249e-03
Relative change in loss: 1.309811e-01
Epoch: 9   Loss: 4.988e-02   Count: 60000
loss_counter: 10
Average change in loss: 6.555354e-03
Relative change in loss: 1.314260e-01
Epoch: 10   Loss: 4.700e-02   Count: 60000
loss_counter: 11
Average change in loss: 6.221497e-03
Relative change in loss: 1.323842e-01
Epoch: 11   Loss: 4.425e-02   Count: 60000
loss_counter: 12
Average change in loss: 5.932150e-03
Relative change in loss: 1.340707e-01
Epoch: 12   Loss: 4.059e-02   Count: 60000
loss_counter: 13
Average change in loss: 5.756867e-03
Relative change in loss: 1.418193e-01
Epoch: 13   Loss: 3.765e-02   Count: 60000
loss_counter: 14
Average change in loss: 5.555860e-03
Relative change in loss: 1.475653e-01
Epoch: 14   Loss: 3.509e-02   Count: 60000
loss_counter: 15
Average change in loss: 5.356084e-03
Relative change in loss: 1.526343e-01
Epoch: 15   Loss: 3.208e-02   Count: 60000
loss_counter: 16
Average change in loss: 5.209574e-03
Relative change in loss: 1.623981e-01
Epoch: 16   Loss: 2.984e-02   Count: 60000
loss_counter: 17
Average change in loss: 5.034893e-03
Relative change in loss: 1.687351e-01
Epoch: 17   Loss: 2.721e-02   Count: 60000
loss_counter: 18
Average change in loss: 4.901359e-03
Relative change in loss: 1.801457e-01
Epoch: 18   Loss: 2.492e-02   Count: 60000
loss_counter: 19
Average change in loss: 4.763604e-03
Relative change in loss: 1.911271e-01
Epoch: 19   Loss: 2.273e-02   Count: 60000
loss_counter: 20
Average change in loss: 4.635060e-03
Relative change in loss: 2.039091e-01
Epoch: 20   Loss: 2.056e-02   Count: 60000
loss_counter: 21
Average change in loss: 4.517897e-03
Relative change in loss: 2.197807e-01
Epoch: 21   Loss: 1.881e-02   Count: 60000
loss_counter: 22
Average change in loss: 4.391803e-03
Relative change in loss: 2.334507e-01
Epoch: 22   Loss: 1.726e-02   Count: 60000
loss_counter: 23
Average change in loss: 4.268313e-03
Relative change in loss: 2.472805e-01
Epoch: 23   Loss: 1.559e-02   Count: 60000
loss_counter: 24
Average change in loss: 4.160255e-03
Relative change in loss: 2.669210e-01
Epoch: 24   Loss: 1.452e-02   Count: 60000
loss_counter: 25
Average change in loss: 4.036672e-03
Relative change in loss: 2.780955e-01
Epoch: 25   Loss: 1.312e-02   Count: 60000
loss_counter: 26
Average change in loss: 3.935226e-03
Relative change in loss: 3.000247e-01
Epoch: 26   Loss: 1.219e-02   Count: 60000
loss_counter: 27
Average change in loss: 3.823749e-03
Relative change in loss: 3.136532e-01
Epoch: 27   Loss: 1.141e-02   Count: 60000
loss_counter: 28
Average change in loss: 3.715151e-03
Relative change in loss: 3.256622e-01
Epoch: 28   Loss: 1.065e-02   Count: 60000
loss_counter: 29
Average change in loss: 3.613094e-03
Relative change in loss: 3.391782e-01
Epoch: 29   Loss: 1.001e-02   Count: 60000
loss_counter: 30
Average change in loss: 3.514180e-03
Relative change in loss: 3.511787e-01
Epoch: 30   Loss: 9.126e-03   Count: 60000
loss_counter: 31
Average change in loss: 3.429242e-03
Relative change in loss: 3.757773e-01
Epoch: 31   Loss: 8.823e-03   Count: 60000
loss_counter: 32
Average change in loss: 3.331545e-03
Relative change in loss: 3.776064e-01
Epoch: 32   Loss: 8.224e-03   Count: 60000
loss_counter: 33
Average change in loss: 3.248726e-03
Relative change in loss: 3.950172e-01
Epoch: 33   Loss: 8.107e-03   Count: 60000
loss_counter: 34
Average change in loss: 3.156615e-03
Relative change in loss: 3.893544e-01
Epoch: 34   Loss: 7.471e-03   Count: 60000
loss_counter: 35
Average change in loss: 3.084606e-03
Relative change in loss: 4.128775e-01
Epoch: 35   Loss: 7.207e-03   Count: 60000
loss_counter: 36
Average change in loss: 3.006244e-03
Relative change in loss: 4.171038e-01
Epoch: 36   Loss: 7.097e-03   Count: 60000
loss_counter: 37
Average change in loss: 2.927984e-03
Relative change in loss: 4.125780e-01
Epoch: 37   Loss: 6.847e-03   Count: 60000
loss_counter: 38
Average change in loss: 2.857506e-03
Relative change in loss: 4.173373e-01
Epoch: 38   Loss: 6.403e-03   Count: 60000
loss_counter: 39
Average change in loss: 2.795630e-03
Relative change in loss: 4.366347e-01
Epoch: 39   Loss: 6.195e-03   Count: 60000
loss_counter: 40
Average change in loss: 2.730933e-03
Relative change in loss: 4.408368e-01
Epoch: 40   Loss: 6.293e-03   Count: 60000
loss_counter: 41
Average change in loss: 2.661928e-03
Relative change in loss: 4.229863e-01
Epoch: 41   Loss: 5.799e-03   Count: 60000
loss_counter: 42
Average change in loss: 2.610303e-03
Relative change in loss: 4.500926e-01
Epoch: 42   Loss: 5.713e-03   Count: 60000
loss_counter: 43
Average change in loss: 2.551603e-03
Relative change in loss: 4.466072e-01
Epoch: 43   Loss: 5.217e-03   Count: 60000
loss_counter: 44
Average change in loss: 2.504900e-03
Relative change in loss: 4.801779e-01
Epoch: 44   Loss: 5.245e-03   Count: 60000
loss_counter: 45
Average change in loss: 2.448595e-03
Relative change in loss: 4.668035e-01
Epoch: 45   Loss: 5.114e-03   Count: 60000
loss_counter: 46
Average change in loss: 2.398217e-03
Relative change in loss: 4.689281e-01
Epoch: 46   Loss: 4.907e-03   Count: 60000
loss_counter: 47
Average change in loss: 2.351607e-03
Relative change in loss: 4.792630e-01
Epoch: 47   Loss: 4.707e-03   Count: 60000
loss_counter: 48
Average change in loss: 2.306771e-03
Relative change in loss: 4.900485e-01
Epoch: 48   Loss: 4.785e-03   Count: 60000
loss_counter: 49
Average change in loss: 2.258097e-03
Relative change in loss: 4.718654e-01
Epoch: 49   Loss: 4.600e-03   Count: 60000
loss_counter: 50
Average change in loss: 2.216647e-03
Relative change in loss: 4.818911e-01
Finished Training
Generated 10000 examples with SNR -10 dB
Generated 10000 examples with SNR -9 dB
Generated 10000 examples with SNR -8 dB
Generated 10000 examples with SNR -7 dB
Generated 10000 examples with SNR -6 dB
Generated 10000 examples with SNR -5 dB
Generated 10000 examples with SNR -4 dB
Generated 10000 examples with SNR -3 dB
Generated 10000 examples with SNR -2 dB
Generated 10000 examples with SNR -1 dB
Generated 10000 examples with SNR 0 dB
Generated 10000 examples with SNR 1 dB
Generated 10000 examples with SNR 2 dB
Generated 10000 examples with SNR 3 dB
Generated 10000 examples with SNR 4 dB
Generated 10000 examples with SNR 5 dB
Generated 10000 examples with SNR 6 dB
Generated 10000 examples with SNR 7 dB
Generated 10000 examples with SNR 8 dB
Generated 10000 examples with SNR 9 dB
Generated 10000 examples with SNR 10 dB
The source variance is 0.333792
Accuracy: 87.52 percent
Mean Square Error: 0.088552
Signal-to-Distorion Ratio: 5.763 [dB]
Generated 10000 examples with SNR -10 dB
The source variance is 0.330551
SNR: -10 	 SDR: -0.91584 	 MSE: 0.40815 	 Accuracy: 54.64 percent
Generated 10000 examples with SNR -9 dB
The source variance is 0.331829
SNR: -9 	 SDR: -0.23449 	 MSE: 0.35024 	 Accuracy: 59.18 percent
Generated 10000 examples with SNR -8 dB
The source variance is 0.331389
SNR: -8 	 SDR: 0.61353 	 MSE: 0.28773 	 Accuracy: 64.36 percent
Generated 10000 examples with SNR -7 dB
The source variance is 0.335374
SNR: -7 	 SDR: 1.22008 	 MSE: 0.25323 	 Accuracy: 68.23 percent
Generated 10000 examples with SNR -6 dB
The source variance is 0.336184
SNR: -6 	 SDR: 2.52232 	 MSE: 0.18808 	 Accuracy: 74.22 percent
Generated 10000 examples with SNR -5 dB
The source variance is 0.336490
SNR: -5 	 SDR: 3.57394 	 MSE: 0.14777 	 Accuracy: 78.76 percent
Generated 10000 examples with SNR -4 dB
The source variance is 0.333419
SNR: -4 	 SDR: 5.32531 	 MSE: 0.09783 	 Accuracy: 84.17 percent
Generated 10000 examples with SNR -3 dB
The source variance is 0.329989
SNR: -3 	 SDR: 7.18794 	 MSE: 0.06305 	 Accuracy: 88.45 percent
Generated 10000 examples with SNR -2 dB
The source variance is 0.335883
SNR: -2 	 SDR: 9.89171 	 MSE: 0.03444 	 Accuracy: 92.31 percent
Generated 10000 examples with SNR -1 dB
The source variance is 0.337432
SNR: -1 	 SDR: 12.14397 	 MSE: 0.02060 	 Accuracy: 94.64 percent
Generated 10000 examples with SNR 0 dB
The source variance is 0.337739
SNR: 0 	 SDR: 15.86885 	 MSE: 0.00874 	 Accuracy: 96.48 percent
Generated 10000 examples with SNR 1 dB
The source variance is 0.336008
SNR: 1 	 SDR: 19.85481 	 MSE: 0.00347 	 Accuracy: 97.51 percent
Generated 10000 examples with SNR 2 dB
The source variance is 0.337203
SNR: 2 	 SDR: 24.47684 	 MSE: 0.00120 	 Accuracy: 97.98 percent
Generated 10000 examples with SNR 3 dB
The source variance is 0.338738
SNR: 3 	 SDR: 27.68131 	 MSE: 0.00058 	 Accuracy: 98.21 percent
Generated 10000 examples with SNR 4 dB
The source variance is 0.337350
SNR: 4 	 SDR: 28.09867 	 MSE: 0.00052 	 Accuracy: 98.26 percent
Generated 10000 examples with SNR 5 dB
The source variance is 0.334275
SNR: 5 	 SDR: 28.68652 	 MSE: 0.00045 	 Accuracy: 98.30 percent
Generated 10000 examples with SNR 6 dB
The source variance is 0.334050
SNR: 6 	 SDR: 28.91892 	 MSE: 0.00043 	 Accuracy: 98.33 percent
Generated 10000 examples with SNR 7 dB
The source variance is 0.331321
SNR: 7 	 SDR: 28.96743 	 MSE: 0.00042 	 Accuracy: 98.35 percent
Generated 10000 examples with SNR 8 dB
The source variance is 0.331386
SNR: 8 	 SDR: 28.97015 	 MSE: 0.00042 	 Accuracy: 98.35 percent
Generated 10000 examples with SNR 9 dB
The source variance is 0.332548
SNR: 9 	 SDR: 29.13419 	 MSE: 0.00041 	 Accuracy: 98.38 percent
Generated 10000 examples with SNR 10 dB
The source variance is 0.333741
SNR: 10 	 SDR: 29.22256 	 MSE: 0.00040 	 Accuracy: 98.39 percent
res: [-0.9158369450365733, -0.2344882734057469, 0.6135310961604584, 1.220078064561636, 2.5223180820572337, 3.573936295004041, 5.325314972347652, 7.18794103072894, 9.891708753596856, 12.14397319627743, 15.868849902654249, 19.854806577657627, 24.47684411091116, 27.68130597559935, 28.098668443506764, 28.686520237899423, 28.918922638353774, 28.967425164997916, 28.97015309037277, 29.134193514433406, 29.2225558456353]
[0.12382308, 0.10847692, 0.15717168, 0.17994176, 0.06367002, 0.01645952, 0.17460105, 0.13555256, 0.12844574, 0.11499835, 0.11685306, 0.19139462, 0.20346245, 0.12069767, 0.20684936, 0.05684767, 0.20472595, 0.03709949, 0.10415552, 0.08590632, 0.22643255, 0.21105967, 0.19915536, 0.16115404, 0.03118854, 0.18561472, 0.12057285, 0.05374938, 0.17488107, 0.17700132, 0.15179817, 0.20157967, 0.13781863, 0.0384858, 0.14233394, 0.1544325, 0.12247276, 0.06843885, 0.15669923, 0.16760792, 0.17626555, 0.17180191, 0.06182777, 0.10257803, 0.08054303, 0.13447065, 0.1068071, 0.12692424, 0.11186648, 0.05276959]
Log is written to logs/log_n100_RawTP.txt
