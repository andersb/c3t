"""
    Constant Curvature Curve Tube Codes for Analog Error Correction
    
    Train the MLP

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering, Virginia Tech
"""

import sys
import torch
import numpy as np
import torch.nn as nn
import torch.optim as optim
		
def c3t_train(model, trainloader, epochs, lr, decay):

    # Use Mean Square Error for loss function
    criterion = nn.MSELoss()

    # Use the Adam optimizer
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=decay)

    # Number of examples in trainloader
    num = len(trainloader)
    print('Training for %d epochs with %d batches' % (epochs,num))

    # Loss counter
    prev_loss = 1
    loss_counter = 0
    loss_count = 20
    loss_threshold = 0.95
    lr_reduction_fraction = 0.85

    # For each epoch
    for epoch in range(epochs):
        total = 0
        running_loss = 0.0
        sys.stdout.flush()
        for count, data in enumerate(trainloader,0):

            # Reset optimizer
            optimizer.zero_grad()

            # Extract data from trainloader
            inp, lbl = data
            total += lbl.size(0)

            # Forward propagation
            outp = model(inp)

            # Backward propagation
            loss = criterion(outp, lbl)
            loss.backward()
            optimizer.step()
            running_loss += loss.item()

        # Compute average loss
        ave_loss = running_loss / total

        # Compute relative/diff loss
        rel_loss = ave_loss / prev_loss
        di = ave_loss - prev_loss
        prev_loss = ave_loss

        # Update loss counter
        loss_counter += 1

        # Check if we should lower he learning rate
        if loss_counter == loss_count and rel_loss > loss_threshold:
            lr *= lr_reduction_fraction
            optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=decay)
            loss_counter = 0

        print('Epoch: %d\tLoss: %.3e\t\tDiff: %.3e\tCount: %d\tLearning Rate: %.5f' % (epoch,ave_loss,di,num,lr))
        #print('Loss Counter: %d\tRel loss change: %e' % (loss_counter,ar))

    print('Finished Training')

# EOF
