#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and
    Vijay Mishra 
"""

from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd

# Repetition code encoder
def rep_enc(s,src_pwr,mod_order,snr):
   
    # Compute the linear SNR
    snr_lin = 10.0**(snr/10.0)

    # Get the number of symbols
    num_symbs = len(s)
    
    # Allocate the output
    rx = np.empty((mod_order,num_symbs))
    v = np.empty(num_symbs)

    # Allocate the tx vector
    tx = np.empty(mod_order)
    
    # Loop for each symbol
    for ix in range(num_symbs):
        
        # Create encoded symbol
        source = s[ix]
        tx.fill(source)
        
        # Compute the noise variance
        symb_pow = src_pwr[ix]
        va = symb_pow/snr_lin
        noise_std = np.sqrt(va)

        # Generate AWGN
        n = rnd.normal(0.0, noise_std, mod_order)
 
        # Save the received symbol 
        rx[:,ix] = tx + n

        # Save the variance
        v[ix] = va

    # Return received symbols
    return rx,v

# Decode and measure accuracy
def accuracy(s,symbs,v,vs,flag):

    # Decode symbols
    outp = np.mean(symbs, axis=0)
    if flag == 1:
        outp = vs/(vs+v) * outp

    # Compute errors
    err = np.abs(s-outp)
    sqerr = err**2
        
    # Accumulate errors
    sum_err = np.sum(err)
    sqe = np.sum(sqerr)

    # Total number of symbols        
    total = len(s)
    
    # Compute the Accuracy 
    acc = 1 - sum_err / total
    
    # Compute the MSE
    mse = sqe / total

    # Compute variance
    v = np.var(s)

    # Compute SDR
    sdr = 10.0*np.log10(v/mse)

    # Return Accuracy, MSE and SDR
    return acc,mse,sdr

    
"""
##############################################################################

    Main entry point for the C3T codes Monte-Carlo simulation

##############################################################################
"""

# Flags
plot_flag = False           # Plotting
#plot_flag = True

# Device
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print('Device:', device)

# Number of examples
num_examples = 50000

# Range of source
mag = 1

# Generate the sources
s = np.linspace(-mag,+mag,num_examples)

# Variance of the source
vs = np.var(s)

# Power of the source symbols
src_pwr = s**2

# SNR range
snr_min = -10
snr_max = +10
print('SNR Range [dB]: [%d,%d]' % (snr_min,snr_max))

# Specify modulation order
mod_order = [100]
mod_order = [2,4,6,8,20,40,100]

# Loop for each modulation order
for order in mod_order:
    
    # Modulation order
    print('Modulation Order:\t n =', order)
    
    # Compute SDR, MSE and Accuracy for a range of snr SNR levels
    sdr_list = []
    mmse = []
    snr_range = np.arange(snr_min, snr_max+1)
    for snr in snr_range:

        # Generate symbols at a particular SNR value
        symbols,v = rep_enc(s,src_pwr,order,snr)
        
        # Measure performance of the repetition decoder
        acc,mse,sdr = accuracy(s,symbols,v/order,vs,0)
        
        # Save SDR to list
        sdr_list.append(sdr)
        print('SNR: %d \t SDR: %.5f \t MSE: %.5f \t Accuracy: %0.2f percent' % (snr,sdr,mse,acc*100.0))

        acc,mse,sdr = accuracy(s,symbols,v/order,vs,1)
        mmse.append(sdr)
        if snr == 10:
            print('REP MMSE Accuracy: %0.2f percent' % (acc*100.0))

    print('res:',sdr_list)
    
    # Plot accuracy
    dim = 'n = %d' % order
    plt.plot(snr_range, mmse, label='Repetition code with MMSE decoder')
    plt.plot(snr_range, sdr_list, color = "orange", label='Repetition code with Mean decoder')
    plt.xlim(snr_min-1,snr_max+1)
    plt.ylim(min(sdr_list)-1,max(sdr_list)+1)
    plt.grid(True)
    plt.title('Signal-to-Distorsion Rato vs SNR, '+dim)
    plt.xlabel('SNR [dB]')
    plt.ylabel('SDR [dB]')
    plt.legend()
    fname = 'plots/c3t_sdr_vs_snr_n%d_rep.png' % order
    plt.savefig(fname)
    if plot_flag == True:
        plt.show()
    
    # Save accuracy to file
    fname = 'accuracy/c3t_sdr_vs_snr_n%d_Rep.txt' % order
    np.savetxt(fname,np.asarray(sdr_list), delimiter=',')
    fname = 'accuracy/c3t_sdr_vs_snr_n%d_Rep_MMSE.txt' % order
    np.savetxt(fname,np.asarray(mmse), delimiter=',')
    
# EOF
