#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Based on prior work by Robert Taylor, Drs. Lamine Milli, Amir Zaghloul and
    Vijay Mishra 
"""

from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from c3t_enc import c3t_enc
from c3t_radii import get_radii

def get_alphas(num,mag):
    
    # Get alpha values
    a = np.linspace(-mag,+mag,num)
    return a
    

def map_lut(r,alphas):
    
    # Get the number of alpha values
    num_alphas = len(alphas)
    
    # Modulation order
    num_r = len(r)
    mod_order = 2*num_r
    
    # Allocate the look-up table
    lut = np.empty((mod_order,num_alphas))
    
    # Loop for each cos/sin pair
    for ix in range(num_r):
        
        # Compute omega * alphas
        omega = ix + 1
        oa = omega*alphas
        
        # Compute possibly transmitted values
        kx = ix*2
        lut[kx,:] = r[ix] * np.cos(oa)
        lut[kx+1,:] = r[ix] * np.sin(oa)
            
    # Return the look-up table
    return lut

def map_dec(lut,alphas,symb):
    
    # Number of r-values
    symb = symb.view(-1)
    num_r = len(symb) // 2
    
    # Allocate confidence levels for the possible alpha values
    num_alpha = lut.shape[1]
    confidence = torch.zeros(num_alpha, dtype=torch.double)
        
    # Loop for each cos/sin pair
    for ix in range(num_r):
            
        # Compute index
        kx = 2*ix
        qx = kx+1

        # Extract the received re/im
        re = symb[kx]
        im = symb[qx]
            
        # Extract cross-correlation reference
        ref_re = lut[kx,:]
        ref_im = lut[qx,:]
        
        # Multiply the received with the transmitted and sum
        s = re * ref_re + im * ref_im
        confidence += s
     
    # Find index to highest confidence
    ix = np.argmax(confidence)
        
    # Find alpha_hat
    alpha_hat = alphas[ix]

    # Return decoded alpha_values after de-stretch
    return alpha_hat

def accuracy(lut,alphas,mag,testloader,variance_s):

    # Sum of squared error
    sqe = 0
    sum_err = 0
    
    # Loop for dataset
    for data in testloader:

        # Extract data from testloader
        inp, lbl = data

        # MAP Decoder
        dec = map_dec(lut,alphas,inp) / mag

        # Compute MSE
        err = torch.abs(dec - lbl.float()).item()
        sqerr = err**2
        
        # Accumulate errors
        sum_err += err
        sqe += sqerr

    # Total number of symbols        
    total = len(testloader)    
    print('total:',total)
    acc = 1 - sum_err / total
    mse = sqe / total
    sdr = 10.0*np.log10(variance_s / mse)
    return acc,mse,sdr

    
"""
##############################################################################

    Main entry point for the C3T codes Monte-Carlo simulation

##############################################################################
"""

# Plotting flag
plot_flag = True
plot_flag = False

# Device
device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
print('Device:', device)

# Load radii
r2, r4, r6, r8, r10, r12, r20, r40, r100 = get_radii()

# Number of examples
num_ex_per_snr = 10000

# C3T Encoder parameters
beta = 0.9
stretch = np.pi

# MAP Decoder parameters
num_alphas = 2000
# A larger magnitude gives better performance at low SNR
mag = np.pi*beta
alphas = get_alphas(num_alphas,mag)

# SNR range
snr_min = -10
snr_max = +10
snr_rng = np.arange(snr_min,snr_max+1)
print('SNR Range [dB]: [%d,%d]' % (snr_min,snr_max))

# Specify what radii to use
r_values = [r8]
r_values = [r2,r4,r6,r8,r20,r40,r100]

for radii in r_values:
    
    # Modulation order
    mod_order = 2*len(radii)
    print('Modulation Order:\t n =', mod_order)
    
    # Create the decoder look-up table
    lut = torch.tensor(map_lut(radii,alphas))
    

    # Create the C3T encoder
    batch_size = 1
    enc = c3t_enc('Raw', mod_order, radii, beta, stretch, batch_size, device)
    
    # Compute SDR, MSE and Accuracy for a range of snr SNR levels
    sdr_list = []
    for snr in snr_rng:
        
        # get_data() expects a range
        srng = np.arange(snr,snr+1)

        # Generate symbols at a particular SNR value
        test_loader, signal_variance = enc.get_data(num_ex_per_snr, srng, False)
        
        # Measure performance of the MAP decoder
        acc,mse,sdr = accuracy(lut,alphas,mag,test_loader,signal_variance)
        
        # Save SDR to list
        sdr_list.append(sdr)
        print('SNR: %d \t SDR: %.5f \t MSE: %.5f \t Accuracy: %0.2f percent' % (snr,sdr,mse,acc*100.0))

    print('res:',sdr_list)
    
    # Plot accuracy
    leg = 'n = %d' % mod_order
    plt.plot(snr_rng, sdr_list, color = "orange", label=leg)
    plt.xlim(snr_min-1,snr_max+1)
    plt.ylim(min(sdr_list)-1,max(sdr_list)+1)
    plt.grid(True)
    plt.title('Signal-to-Distorsion Rato vs SNR')
    plt.xlabel('SNR [dB]')
    plt.ylabel('SDR [dB]')
    plt.legend()
    fname = 'plots/c3t_sdr_vs_snr_n%d_MAP.png' % mod_order
    plt.savefig(fname)
    if plot_flag == True:
        plt.show()
    
    # Save accuracy to file
    fname = 'accuracy/c3t_sdr_vs_snr_n%d_MAP.txt' % mod_order
    np.savetxt(fname,np.asarray(sdr_list), delimiter=',')
    
# EOF
