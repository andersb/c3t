"""
    QNN for decoding C3T

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech
"""

import sys
from sys import exit
import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
import torch.optim as optim
sys.path.insert(0, '../../Quaternion-Neural-Networks')
from core_qnn.quaternion_layers import *

#
# Activation functions
#

# Complex-valued hyperbolic arcsin
def Casinh(x):
    d0 = x.shape[0]
    # Extract the complex numbers
    y = torch.reshape(x,(d0,-1,2))
    # Extract real/imag
    re = y[:,:,0]
    im = y[:,:,1]
    # Create complex numbers
    z = re+im*1j
    # Hyperbolic arcsin on the complex numbers
    x = torch.arcsinh(z)
    # Extract real/imag
    re = x.real
    im = x.imag
    y[:,:,0] = re
    y[:,:,1] = im
    # Make 4-D quaternions
    x = torch.reshape(y,(d0,-1))
    return x

# ModReLU
# relu(|z|+b) * (z / |z|)
# b is bias
def qModReLU(x):
    b = 0.8
    d0 = x.shape[0]
    y = torch.reshape(x,(d0,-1,4))
    norms = torch.linalg.norm(y,dim=2)
    act = F.relu(norms+b)
    scale = act / (norms + 1e-6)
    sc = torch.repeat_interleave(scale,4,dim=1)
    return x*sc

class QNN(nn.Module):
    def __init__(self, n_input, n_hidden, n_output, batch_size, act_fn, device):
        super(QNN, self).__init__()

        self.inp_dim = n_input
        self.hid_dim = n_hidden
        self.out_dim = n_output
        self.batch_size = batch_size
        
        # Activation function
        if act_fn == 'Casinh':
            self.act_fn = Casinh
        elif act_fn == 'qModReLU':
            self.act_fn = qModReLU
        elif act_fn == 'ReLU':
            self.act_fn = nn.ReLU()
        elif act_fn == 'asinh':
            self.act_fn = torch.asinh
        elif act_fn == 'tanh':
            self.act_fn = nn.Tanh()
        elif act_fn == 'hardtanh':
            self.act_fn = nn.Hardtanh()
        else:
            self.act_fn = F.relu
        
        self.fc1    = QuaternionLinear(n_input, n_hidden)
        self.fc2    = QuaternionLinear(n_hidden, 2*n_hidden)
        self.fc3    = QuaternionLinear(2*n_hidden, 4*n_hidden)
        self.fc4    = QuaternionLinear(4*n_hidden, 2*n_hidden)
        self.fc5    = QuaternionLinear(2*n_hidden, n_hidden)
        self.fcOut  = nn.Linear(n_hidden, n_output)
        
        # The processing device; CPU or GPU
        self.device = device
        print('-----------------------')
        print('Created C3T QNN decoder')
        print('-----------------------')
		
    def forward(self, x):
        
        x = x.view(x.shape[0], -1)
        x = self.fc1(x)
        x = self.act_fn(x)
        x = self.fc2(x)
        x = self.act_fn(x)
        x = self.fc3(x)
        x = self.act_fn(x)
        x = self.fc4(x)
        x = self.act_fn(x)
        x = self.fc5(x)
        x = self.act_fn(x)
        x = self.fcOut(x)
        return x
    
# EOF
