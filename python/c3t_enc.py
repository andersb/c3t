"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

from sys import exit
import torch, numpy
import matplotlib.pyplot as plt
import numpy as np
import numpy.random as rnd
from huber import weights
from huber import irls

class c3t_enc:
    
    # Constructor
    def __init__(self, feat_type, feat_dim, radii, beta, stretch, batch_size, Huber_b, Huber_iter,device):
        self.feat_type = feat_type
        self.feat_dim = feat_dim
        num_pairs = len(radii)
        self.num_pairs = num_pairs
        self.mod_order = num_pairs*2
        self.batch_size = batch_size
        self.radius = radii
        self.sig_pwr = np.array(radii)**2
        self.device = device
        self.beta = beta
        self.stretch = stretch
        self.Huber_b = Huber_b
        self.Huber_iter = Huber_iter
        
        # Check if odd
        if feat_type == 'Odd':
            self.num_pairs = 1
            self.mod_order = 3
            self.b = radii[1]
            
    def plot(x,y):
        
        plt.figure(figsize=(10,6))
        plt.scatter(x, y, color = "orange")
        plt.title('Regression Analysis')
        plt.xlabel('Independent varible')
        plt.ylabel('Dependent varible')
        plt.grid()
        plt.show()
    
    def ch(self,symb,snr):
        
        # Initialize the output
        rx = np.empty(symb.shape)

        # Length of output
        L = symb.shape[1]
        
        # Compute inverse of the linear SNR
        il_snr = 10**(-snr/10)
        
        # Compute noise variance for each cos/sin pair
        noise_pwr = self.sig_pwr * il_snr
        
        # Noise std dev for each dimension
        noise_std = np.sqrt(noise_pwr/2.0)
        
        # Each pair has different signal power
        num = self.num_pairs
        for ix in range(num):
            
            # Generate noise for the cos/sin pair
            n = np.random.normal(0.0, noise_std[ix], (2,L))
            
            # AWGN
            jx = 2*ix
            kx = jx+1
            rng = [jx,kx]
            rx[rng,:] = symb[rng,:] + n
            
        if self.feat_type == 'Odd':
            
            # Signal power
            ox = kx+1
            sig_pwr = symb[ox,:]**2

            # Compute noise variance for each sample
            noise_pwr = sig_pwr * il_snr

            # Noise std dev for each sample
            noise_std = np.sqrt(noise_pwr)
            
            # Generate noise for each sample
            n = np.random.normal(0.0, noise_std, L)
            
            # AWGN
            rx[ox,:] = symb[ox,:] + n
            
        return rx
    
    # Path length
    def path_length(self):
        w  = np.arange(self.mod_order//2) + 1
        L = 2*np.pi * np.sqrt(sum((w * self.radius)**2))
        return L
    
    # Generate Continuous Curvature Curve Tube symbols
    def get_c3t_symbol(self,count):

        start = 1.0
        #s = np.linspace(-start,+start,count)
        s = rnd.uniform(-start,+start,count)
        alpha = s * self.stretch * self.beta
        #print(alpha[:8]*180/np.pi)
        #print(alpha[:8])

        # Allocate the output
        symb = np.empty((self.mod_order,count))

        # Get the radius
        for ix in range(self.num_pairs):
            
            # Get the next radius
            r = self.radius[ix]
            
            # Get real and imag parts
            omega = ix + 1
            re = r*np.cos(omega*alpha)
            im = r*np.sin(omega*alpha)
            
            # Save real and imag in rows
            # Each column is a symbol
            kx = 2*ix
            symb[kx,:] = re 
            symb[kx+1,:] = im
        
        if self.feat_type == 'Odd':
            symb[kx+2,:] = self.b*alpha
            
        # Return symbol and labels
        return symb, s

    # Compute angles between cos/sin pairs
    def angle_only(self,symb):
        
        # Allocate output
        num = self.num_pairs
        L = symb.shape[1]
        angles = np.empty((num,L))
        
        # Loop for each cos/sin pair
        for ix in range(num):
            kx = 2*ix
            re = symb[kx,:]
            im = symb[kx+1,:]

            # Find the angle
            angles[ix,:] = np.arctan2(im,re) 
            
        # Return angles
        return angles
    
    # Robust Torus Projection (RTP)
    def rtp(self,symb):
        
        # Get robust weights
        we,H,Rinv = weights(self.mod_order)
        
        # Allocate the output
        d = symb.shape
        rtp = np.empty(d)
        
        # Dimension, num radii, and number of symbols
        n = d[0]
        n2 = n // 2
        num_sym = d[1]

        # Loop for all symbols
        for ix in range(num_sym):
            
            # Get next symbol
            re = symb[0::2,ix]
            im = symb[1::2,ix]
            
            # Find the angles
            z = numpy.arctan2(im,re);
            
            # Huber
            angle = irls(self.Huber_b,n2,we,z,H,Rinv,self.Huber_iter)
    
            # Loop for each cos/sin pair
            w = 0
            for kx in range(n2):
                
                # Increment frequency parameter
                w += 1

                # Get the transmitted radius
                r = self.radius[kx]

                # Compute new cos/sin pair
                re = r*np.cos(w*angle)
                im = r*np.sin(w*angle)
                rtp[kx*2,ix]    = re
                rtp[kx*2+1,ix]  = im
                
        # Return the RTP
        return rtp

    # Compute the L^2-norm for cos/sin pairs
    def torus_projection(self,symb):
        
        # Allocate the output (Torus Projection)
        tp = np.empty(symb.shape)
        
        # Loop for each cos/sin pair
        num = self.num_pairs
        for ix in range(num):

            # Extract real/imag
            kx = 2*ix
            re = symb[kx,:]
            im = symb[kx+1,:]
            
            # Find the sign of real/imag parts
            re_sgn = np.sign(re)
            im_sgn = np.sign(im)
            
            # Compute the Gamma (L^2-norm)
            gamma = np.sqrt(im*im+re*re)
            
            # Find the ratios with gamma
            re_ratio = re / gamma * re_sgn
            im_ratio = im / gamma * im_sgn

            # Find the angles
            re_angle = np.arccos(re_ratio)
            im_angle = np.arcsin(im_ratio)

            # Average the angles
            angle = (re_angle + im_angle) / 2.0
            
            # Get the transmitted radius
            r = self.radius[ix]
            
            # Compute new cos/sin pair
            re = r*np.cos(angle) * re_sgn
            im = r*np.sin(angle) * im_sgn
            tp[kx,:] = re
            tp[kx+1,:] = im

        # Return the Torus Projection
        return tp


    # Generate symbols and transmit them through the AWGN channel
    # mod_order it the dimension 'n'
    def get_data(self, data_num, snr_rng, shuffle):
     
        # Index to data
        dx = np.arange(0,data_num)
 
        # Number of SNR values
        num_snr = np.size(snr_rng)

        # Extract dimensions
        R = self.mod_order
        D = self.feat_dim
        L = data_num * num_snr

        # Allocate memory for features and labels
        feat = np.empty((D,L))
        labs = np.empty(L)

        # Loop for the snr range
        for snr in snr_rng:

            # Generate C3T symbols with labels
            symb, labels = self.get_c3t_symbol(data_num)

            # Save the labels
            labs[dx] = labels

            # Send symbols through the AWGN channel
            rx_symb = self.ch(symb,snr)

            # Type of features
            ftype = self.feat_type
                
            # Check if we are doing Torus Projection
            S = 0
            if 'TP' in ftype or ftype == 'TorusProj': 
                if ftype == 'RTP':
                    feat[:R,dx] = self.rtp(rx_symb)
                else:
                    feat[:R,dx] = self.torus_projection(rx_symb)
                S = R
         
            # Check if we are doing Raw
            C = S
            if 'Raw' in ftype or ftype == 'Odd':
                C = S + R
                feat[S:C,dx] = rx_symb

            # Check if we are doing a combination
            if 'AO' in ftype or ftype == 'AngleOnly':
                
                # Concatenate the Angle Only values to the raw channel output
                feat[C:,dx] = self.angle_only(rx_symb)

            # Update the data index
            #print(feat[:,dx[:8]]*180/np.pi)
            dx += data_num
            print("Generated %d examples with SNR %d dB" % (data_num,snr))
                
        # Append labels to the data
        data_list = []
        for i in range(L):
            f = torch.DoubleTensor(np.array([feat[:,i]])).to(torch.device(self.device))
            f = torch.squeeze(f)
            l = torch.DoubleTensor(np.array([labs[i]])).to(torch.device(self.device))
            data_list.append([f,l])
            
        # Compute variance
        v = np.var(labs)
        loader = torch.utils.data.DataLoader(data_list, batch_size=self.batch_size, shuffle=shuffle)
        print("The source variance is %f" % v)
        return loader, v

# EOF
