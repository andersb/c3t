import numpy as np

r2 = [0.8165, 0.5774]
r3 = [0.5835, 0.6463, 0.4918]
r4 = [0.5125, 0.5162, 0.5551, 0.4035]
r5 = [0.4505, 0.4402, 0.4708, 0.5000, 0.3628]
r6 = [0.4141, 0.3953, 0.4102, 0.4346, 0.4566, 0.3265]


def w(i):
    return i


def path_length(r, n, w):
    return 2 * np.pi * np.sqrt(sum((w(i+1) * r[i])**2 for i in range(n//2)))


def t1(d, r, n, w, b=0):
    return 2*sum(r[i]**2 * (1-np.cos(w(i+1)*d)) for i in range(n//2))+(b*d)**2


def t2(r, n, w, b=0):
    return sum((w(i+1) * r[i])**2 for i in range(n//2)) + b**2


def t3(d, r, n, w, b=0):
    return sum(r[i]**2*w(i+1) * np.sin(w(i+1)*d) for i in range(n//2))+d*b**2


def circumradius(d, r, n, w):
    t = t1(d, r, n, w)
    return t**2 / (4 * (t - t3(d, r, n, w)**2 / t2(r, n, w)))


def global_circumradius(r, n, w):
    d = np.linspace(0.001, np.pi, 1000)
    return np.sqrt(np.min(circumradius(d, r, n, w)))


def volume_coef(n):
    if n % 2 == 0:
        return np.pi ** (n//2) / np.prod(range(1, n//2 + 1))
    else:
        return np.pi**((n-1)//2) * 2**((n+1)//2) / np.prod(range(1, n + 1, 2))


def objective(r, n, w):
    p = global_circumradius(r, n, w)
    return path_length(r, n, w) * p**(n-1) / (1 + p)**n


def density(r, n, w):
    return objective(r, n, w) * volume_coef(n-1) / volume_coef(n)


def case_n4_max(k=8000):
    return [0.8164770596324541, 0.5773778754801244]
    # e1 = np.linspace(0, 1, k)
    # e2 = np.sqrt(1 - e1 ** 2)
    # x = 0
    # r = [0, 0]
    # for i in range(k):
    #     y = objective([e1[i], e2[i]], 4, w)
    #     if y > x:
    #         x = y
    #         r = [e1[i], e2[i]]
    # return r


def a(k):
    A = 0
    alpha = 0.8
    a0 = 0.1
    return a0 * (k + 1 + A)**-alpha


def c(k):
    gamma = 0.8
    c0 = 0.01
    return c0 * (k + 1)**-gamma


def spsa(f, r0, a, c, tol):
    n = 2 * np.size(r0)
    p = np.size(r0)
    r = r0
    x = f(r, n, w)
    xprev = np.inf
    k = 0
    while k < 200 or np.abs(x - xprev) / np.abs(xprev) > tol:
        delta = 2 * np.round(np.random.rand(p)) - 1
        j1 = f(r + c(k) * delta, n, w)
        j2 = f(r - c(k) * delta, n, w)
        y = (j1 - j2) / (2 * c(k) * delta)
        r += a(k) * y
        r /= np.linalg.norm(r)
        xnext = f(r, n, w)
        x, xprev = xnext, x
        k += 1
        print(x)
    return r


#print(density(r2, 4, w))
#print(density(r3, 6, w))
k = 2
r0 = np.tile([1 / np.sqrt(k)], k)
r = spsa(objective, r0, a, c, 1e-3)
print(r)
print(case_n4_max())
