#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""
import matplotlib.pyplot as plt
import numpy as np
from sys import exit

# Compute SDR in dB
def SDR(n,snr):
    
    # Convert to linear SNR
    snr_lin = 10.0**(snr/10.0)
    sdr_lin = (np.pi * np.e / 6.0) * (1+snr_lin)**n
    sdr = 10.0*np.log10(sdr_lin)   
    return sdr

# SNRs
snr_min = -10
snr_max = +10
snr_range = np.arange(snr_min, snr_max+1)


# Compute OPTA
opta20 = SDR(20,snr_range)
opta40 = SDR(40,snr_range)
opta100 = SDR(100,snr_range)

# Load Linear data to file
sdr20Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n20_Rep.txt', delimiter=',')
sdr40Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n40_Rep.txt', delimiter=',')
sdr100Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n100_Rep.txt', delimiter=',')

# Load MAP data to file
sdr20MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n20_MAP.txt', delimiter=',')
sdr40MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n40_MAP.txt', delimiter=',')
sdr100MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n100_MAP.txt', delimiter=',')

# Load Torus Projection data to file
sdr20TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n20_TorusProj_layers_5.txt', delimiter=',')
sdr40TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n40_TorusProj_layers_5.txt', delimiter=',')
sdr100TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n100_TorusProj_layers_5.txt', delimiter=',')

# Load Angle Only data to file
sdr20AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n20_AngleOnly_layers_5.txt', delimiter=',')
sdr40AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n40_AngleOnly_layers_5.txt', delimiter=',')
sdr100AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n100_AngleOnly_layers_5.txt', delimiter=',')

# Load Raw Channel data to file
sdr20Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n20_Raw_layers_5.txt', delimiter=',')
sdr40Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n40_Raw_layers_5.txt', delimiter=',')
sdr100Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n100_Raw_layers_5.txt', delimiter=',')

# Create plot
fig,(ax1,ax2,ax3) = plt.subplots(1,3,gridspec_kw={'width_ratios': [1,1,1]},figsize=(12,8))

# OPTA
ax1.plot(snr_range, opta20, '--', ms=8, c='0', label='OPTA')
ax2.plot(snr_range, opta40, '--', ms=8, c='0', label='OPTA')
ax3.plot(snr_range, opta100, '--', ms=8, c='0', label='OPTA')

# Linear
ax1.plot(snr_range, sdr20Rep, marker='*', ms=8, c='#FF7F0E', label='REP')
ax2.plot(snr_range, sdr40Rep, marker='*', ms=8, c='#FF7F0E', label='REP')
ax3.plot(snr_range, sdr100Rep, marker='*', ms=8, c='#FF7F0E', label='REP')

# MAP
ax1.plot(snr_range, sdr20MAP, marker='d', ms=8, c='#33FF00', label='MAP')
ax2.plot(snr_range, sdr40MAP, marker='d', ms=8, c='#33FF00', label='MAP')
ax3.plot(snr_range, sdr100MAP, marker='d', ms=8, c='#33FF00', label='MAP')
'''
# Torus Projection
ax1.plot(snr_range, sdr20TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')
ax2.plot(snr_range, sdr40TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')
ax3.plot(snr_range, sdr100TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')

# Angle only
ax1.plot(snr_range, sdr20AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')
ax2.plot(snr_range, sdr40AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')
ax3.plot(snr_range, sdr100AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')

# Raw observations
ax1.plot(snr_range, sdr20Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')
ax2.plot(snr_range, sdr40Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')
ax3.plot(snr_range, sdr100Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')
'''
# Labels for axis
ax1.set_xlabel('SNR [dB]')
ax2.set_xlabel('SNR [dB]')
ax3.set_xlabel('SNR [dB]')
ax1.set_ylabel('SDR [dB]')

# Turn on grid
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)

# Turn on legends
ax1.legend()
ax2.legend()
ax3.legend()

# Set title
ax1.set_title('n=20')
ax2.set_title('n=40')
ax3.set_title('n=100')

# Set range of axis
ymin = -5
ymax = 30
ax1.set_ylim([ymin,ymax])
ax2.set_ylim([ymin,ymax])
ax3.set_ylim([ymin,ymax])

# Save fig
fig.suptitle('Signal-to-Distorion Ratio vs SNR')
fig.savefig('plots/SDR_vs_SNR_n20_n40_n100'+'.png', bbox_inches='tight')

