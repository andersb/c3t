#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp
    
    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

    Assisted by Karl Buvarp
"""

from sys import exit
import numpy as np
from scipy.special import gamma

# r-values for R^n dimensions
r4 = [0.8165, 0.5774]
r6 = [0.5835, 0.6463, 0.4918]
r8 = [0.5125, 0.5162, 0.5551, 0.4035]
r10 = [0.4505, 0.4402, 0.4708, 0.5000, 0.3628]
r12 = [0.4141, 0.3953, 0.4102, 0.4346, 0.4566, 0.3265]
r20 = [0.35251032, 0.36757906, 0.37378317, 0.3705179, 0.36658535,
       0.34507531, 0.30497737, 0.24626995, 0.19969124, 0.12894291]
# Density (n=20): 2.294370762688e-6
r40 = [0.23258918, 0.21673772, 0.25316337, 0.23811528, 0.26905355,
       0.25429533, 0.27286513, 0.26387771, 0.26075637, 0.25428426,
       0.25006161, 0.23869478, 0.23285036, 0.22070924, 0.19165789,
       0.18310335, 0.17028559, 0.15342232, 0.09811064, 0.09736838]
# Density (n=40): 0.121540e-12

def w(i):
    return i


def path_length(r, n, w):
    return 2 * np.pi * np.sqrt(sum((w(i+1) * r[i])**2 for i in range(n//2)))


def t1(d, r, n, w, b=0):
    return 2*sum(r[i]**2 * (1-np.cos(w(i+1)*d)) for i in range(n//2))+(b*d)**2


def t2(r, n, w, b=0):
    return sum((w(i+1) * r[i])**2 for i in range(n//2)) + b**2


def t3(d, r, n, w, b=0):
    return sum(r[i]**2*w(i+1) * np.sin(w(i+1)*d) for i in range(n//2))+d*b**2


def circumradius(d, r, n, w):
    t = t1(d, r, n, w)
    r = t**2 / (4 * (t - t3(d, r, n, w)**2 / t2(r, n, w)))
    return r


def global_circumradius(r, n, w):
    num_points = 10000
    d = np.linspace(1/num_points, np.pi, num_points)
    max_flag = 1
    if max_flag:
        return np.sqrt(np.max(circumradius(d, r, n, w)))
    else:
        return np.sqrt(np.min(circumradius(d, r, n, w)))

def volume_coef(n):
    C_n = np.pi ** (n/2.0) / gamma(n/2.0 + 1.0)
    return C_n

def objective(r, n, w, j0):
    p = global_circumradius(r, n, w)
    j = -path_length(r, n, w) * p**(n-1) / (1 + p)**n
    return j / j0 

def density(r, n, w):
    return objective(r, n, w, 1) * volume_coef(n-1) / volume_coef(n)


def case_n4_max(k=8000):
    return [0.8164770596324541, 0.5773778754801244]
    # e1 = np.linspace(0, 1, k)
    # e2 = np.sqrt(1 - e1 ** 2)
    # x = 0
    # r = [0, 0]
    # for i in range(k):
    #     y = objective([e1[i], e2[i]], 4, w)
    #     if y > x:
    #         x = y
    #         r = [e1[i], e2[i]]
    # return r


def a(k):
    A = 10               # Robert Taylor has A = 10
    alpha = 0.602       # Robert Taylor has alpha = 0.602
    a0 = 0.001           # Robert Taylor has a0 = 0.01
    return a0 * (k + 1 + A)**-alpha


def c(k):
    gamma = 0.101       # Robert Taylor has gamma = 0.101
    c0 = 0.01           # Robert Taylor has a0 = 0.01
    return c0 * (k + 1)**-gamma


def spsa(f, j0, r0, a, c, tol, Niter):
    n = 2 * np.size(r0)
    p = np.size(r0)
    r = r0
    x = f(r, n, w, j0)
    xprev = np.inf
    k = 0
    while k < Niter or np.abs(x - xprev) / np.abs(xprev) > tol:
        delta = 2 * np.round(np.random.rand(p)) - 1
        j1 = f(r + c(k) * delta, n, w, j0)
        j2 = f(r - c(k) * delta, n, w, j0)
        y = (j1 - j2) / (2 * c(k) * delta)
        r += a(k) * y
        r /= np.linalg.norm(r)
        xnext = f(r, n, w, j0)
        x, xprev = xnext, x
        k += 1
        #print('Objective: ', x)
    print('Number of iterations: %d' % k)
    return r

##############################################################################
##############################################################################
##############################################################################

# Number of dimensions
n = 4
k = n // 2

# SPSA input parameters
tol = 1e-3
Niter = 600
r0 = np.tile([1 / np.sqrt(k)], k)

# The initial value for the objective function
j0 = objective(r0, n, w, 1) 

# Initial density
d0 = density(r0,n,w)

print('\nDimensions: %d' % n)
print('\nInitial radii:')
print(r0)
print('\nInitial Objective J(r): %6e' % j0)
print('\nInitial Density: %6e' % d0)

# Loop to find the optimal radii
r_max = r0
d_max = d0
num_iter = 100
for count in range(num_iter):
    print('\nCount: %d' % count)
    r = spsa(objective, j0, r_max, a, c, tol, Niter)
    #r = np.maximum(r,0)
    d = density(r,n,w)
    if d < d_max:
        d_max = d
        r_max = r
        rho = global_circumradius(r, n, w)
        print('SPSA radii:')
        print(r_max)
        print('Density: %6e\tRadius: %6e' % (d,rho))

#r_max = r4
#n = 2*len(r_max)
        
# Results
rho = global_circumradius(r_max, n, w)
den = density(r_max,n,w)
print('Radii')
print(r_max)
print('\nRadius: %e' % rho)
print('\nDensity: %6e' % den)
