#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""

import datetime
import sys
import json
from sys import exit
import torch, numpy
import matplotlib.pyplot as plt
import numpy as np
from c3t_enc import c3t_enc
from c3t_mlp import Net
from c3t_cplx import CplxNet
from c3t_qnn import QNN
from c3t_radii import get_radii
from c3t_train import c3t_train
from c3t_eval import c3t_eval
#import torch_xla
#import torch_xla.core.xla_model as xm

# Print function
def print_set(device,lr,train,evalu,feat_type,mod_order,feat_dim,radii,stretch,batch_size,data_type,act_fn,Huber_b,Huber_iter):

    print('Device:', device)
    print('Learning Rate:',lr)
    print('SNR Training Levels [dB]:',train)
    print('SNR Eval Range [dB]: [%d,%d]' % (evalu[0],evalu[-1]))
    print('Feature Type: ',feat_type)
    print('Feature Dimension: ',feat_dim)
    print('Modulation Order: ',mod_order)
    print('Radii:' ,radii)
    print('Stretching Function: pi *',stretch/np.pi)
    print('Batch Size: ', batch_size)
    print('Data Type: ', data_type)
    print('Activation Function:\t\t', act_fn)
    if feat_type == 'RTP':
        print('Huber b parameter: ', Huber_b)
        print('IRLS iterations: ', Huber_iter)

"""
##############################################################################

    Main entry point for the C3T codes Monte-Carlo simulation

##############################################################################
"""

# Time
print(datetime.datetime.now())

# Load config
fd = open('c3t.json')
cfg = json.load(fd)
fd.close()

# Flags
train_flag = cfg['train_flag']         # Train the network
eval_flag = cfg['eval_flag']           # Evaluate the network
sweep_flag = cfg['sweep_flag']         # SNR sweep
plot_flag = cfg['plot_flag']           # Plotting
log_flag = cfg['log_flag']             # Store std out to a log file
tpu_flag = cfg['tpu_flag']             # Run on Colab TPU
scale_flag = cfg['scale_flag']         # Scale source by n/2
scale = ''

# Device
if tpu_flag == True:
    """
    import os 
    os.environ['LD_LIBRARY_PATH']='/usr/local/lib'

    !echo $LD_LIBRARY_PATH
    !sudo ln -s /usr/local/lib/libmkl_intel_lp64.so /usr/local/lib/libmkl_intel_lp64.so.1
    !sudo ln -s /usr/local/lib/libmkl_intel_thread.so /usr/local/lib/libmkl_intel_thread.so.1
    !sudo ln -s /usr/local/lib/libmkl_core.so /usr/local/lib/libmkl_core.so.1

    !ldconfig
    !ldd /usr/local/lib/python3.7/dist-packages/torch/lib/libtorch.so

    !pip install cloud-tpu-client==0.10 https://storage.googleapis.com/tpu-pytorch/wheels/torch_xla-1.9-cp37-cp37m-linux_x86_64.whl
    device = xm.xla_device()
    """
    device = xm.xla_device()
else:
    device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")

# C3T radius
r2, r4, r6, r8, r10, r12, r16, r20, r40, r100 = get_radii()

# Number of hidden layers
layers = cfg['layers']

# Number of hidden neurons
n_hidden = cfg['n_hidden']

# The output dimension
n_output = cfg['n_output']

# Dataset quantities per SNR level
train_num = cfg['train_num']
test_num = cfg['test_num']

# Hyperparameters
batch_size = cfg['batch_size']
epochs = cfg['epochs']
learning_rate = cfg['lr']
decay = cfg['decay']

# Prevent the curve from touching
beta = cfg['beta']

# Huber parameters
Huber_b = cfg['Huber_b']
Huber_iter = cfg['Huber_iter']

# Training SNR range
train_snr_rng = np.array(cfg['train_snr_rng'])

# snr_max-snr_min+1 Evaluating SNR range
snr_min = cfg['snr_min']
snr_max = cfg['snr_max']
eval_snr_rng = np.arange(snr_min,snr_max+1)

# Data type
data_type = cfg['data_type']

# Activation function
act_fn = cfg['act_fn']

# Features
# "RTP", "Raw", "TorusProj", "AnglesOnly", "RawAO","TPAO","RawTP","RawTPAO","Odd" 
features = cfg['features']

# r values
r_values = cfg['r_values']

# Loop for the type of features
for feat_type in features:

    # Loop for each radii
    for count,r in enumerate(r_values):
        
        # Modulation order
        radii = eval(r)
        num_r = len(radii)
        mod_order = 2*num_r
        
        # Check if odd
        if feat_type == 'Odd':
            mod_order = 3
            radii = [ 0.5, 0.866 ]

        # Set number of features and stretching function
        stretch = np.pi
        if scale_flag:
            stretch /= num_r
            scale = '_scale'
        n_feature = mod_order
        if feat_type == 'AngleOnly':
            n_feature = 0

        # Raw and Torus Projection
        if 'RawTP' in feat_type: 
            n_feature += n_feature

        # Check for Angle Only
        if 'AO' in feat_type or feat_type == 'AngleOnly':
            n_feature += num_r

        # Get the Learning Rate
        lr = learning_rate[count]

        # Create the C3T encoder
        enc = c3t_enc(feat_type, n_feature, radii, beta, stretch, batch_size, Huber_b, Huber_iter, device)
        
        # Create the MLP network decoder
        if data_type == 'real':
            mlp = Net(n_feature, n_hidden, n_output, batch_size, act_fn, device)
        elif data_type == 'cplx':
            mlp = CplxNet(n_feature, n_hidden, n_output, batch_size, act_fn, device)
        else:
            mlp = QNN(n_feature, n_hidden, n_output, batch_size, act_fn, device)
            
        mlp = mlp.double()
        mlp.to(device)
        
        # Model file name
        fname = 'c3t_n%d_%s_layers_%d_type_%s_act_%s%s' % (mod_order,feat_type,layers,data_type,act_fn,scale)
        model_fname = 'models/%s.ptm' % fname

	    # Set std out
        if log_flag == True:

            # Re-direct std out to a log file
            log_file = 'logs/log_%s.txt' % fname
            print('Log is written to %s' % log_file)
            sys.stdout = open(log_file,'w')

        # Print the simulation parameters
        print_set(device,lr,train_snr_rng,eval_snr_rng,feat_type,mod_order,n_feature,radii,stretch,batch_size,data_type,act_fn, Huber_b, Huber_iter)

        # Check the Train Network flag
        if train_flag == True:
        
            # Generate data
            trainloader, signal_variance = enc.get_data(train_num, train_snr_rng, True)
            
            # Flush stdout to file
            sys.stdout.flush()

            # Train the network
            c3t_train(mlp,trainloader, epochs, lr, decay)
        
            # Save MLP to file
            torch.save(mlp.state_dict(), model_fname)
        
        # Check the Test Network flag
        if eval_flag == True:
        
            # Load model from file
            mlp.load_state_dict(torch.load(model_fname))
            
            # Test network
            testloader, signal_variance = enc.get_data(test_num, eval_snr_rng, False)
            acc,mse,sdr = c3t_eval(mlp, testloader, signal_variance, snr_min, snr_max, plot_flag)
            print('Accuracy: %0.2f percent' % (acc*100))
            print('Mean Square Error: %f' % mse)
            print('Signal-to-Distorion Ratio: %.3f [dB]' % sdr)
        
        # Check for sweeping SNR
        if sweep_flag == True:
        
            # Load model from file
            mlp.load_state_dict(torch.load(model_fname))

            # Compute SDR, MSE and Accuracy for a range of snr SNR levels
            enc.samps_per_symb = test_num
            sdr_list = []
            for snr in eval_snr_rng:
        
                # Generate symbols at a particular SNR value
                test_loader, signal_variance = enc.get_data(test_num, [snr], False) 
                acc,mse,sdr = c3t_eval(mlp, test_loader, signal_variance, snr, snr)
                sdr_list.append(sdr)
                print('SNR: %d \t SDR: %.5f \t MSE: %.5f \t Accuracy: %0.2f percent' % (snr,sdr,mse,acc*100.0))

            print('res:',sdr_list)
            
            # Plot accuracy
            leg = 'n = %d   %s' % (mod_order, feat_type)
            plt.plot(eval_snr_rng, sdr_list, color = "orange", label=leg)
            plt.xlim(snr_min-1,snr_max+1)
            plt.ylim(min(sdr_list)-1,max(sdr_list)+1)
            plt.grid(True)
            plt.title('Signal-to-Distorsion Rato vs SNR')
            plt.xlabel('SNR [dB]')
            plt.ylabel('SDR [dB]')
            plt.legend()
            plot_fname = 'plots/%s.png' % fname
            plt.savefig(plot_fname)
            if plot_flag == True:
                plt.show()
            
            # Save accuracy to file
            accu_fname = 'accuracy/n%d/%s/accu_%s.txt' % (mod_order,data_type,fname)
            np.savetxt(accu_fname,np.asarray(sdr_list), delimiter=',')

        # Flush to stdout
        sys.stdout.flush()
    
# Time
print(datetime.datetime.now())

# EOF
