#!/usr/bin/env python3
"""
    Constant Curvature Curve Tube Codes for Analog Error Correction

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech

"""
import matplotlib.pyplot as plt
import numpy as np
from sys import exit

# Compute SDR in dB
def SDR(n,snr):

    # Convert to linear SNR
    snr_lin = 10.0**(snr/10.0)
    sdr_lin = (np.pi * np.e / 6.0) * (1+snr_lin)**n
    sdr = 10.0*np.log10(sdr_lin)
    return sdr

# SNRs
snr_min = -10
snr_max = +10
snr_range = np.arange(snr_min, snr_max+1)

# Compute OPTA
opta4 = SDR(4,snr_range)
opta6 = SDR(6,snr_range)
opta8 = SDR(8,snr_range)

# Load Linear data to file
sdr4Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n4_Rep.txt', delimiter=',')
sdr6Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n6_Rep.txt', delimiter=',')
sdr8Rep = np.loadtxt('accuracy/c3t_sdr_vs_snr_n8_Rep.txt', delimiter=',')

# Load MAP data to file
sdr4MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n4_MAP.txt', delimiter=',')
sdr6MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n6_MAP.txt', delimiter=',')
sdr8MAP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n8_MAP.txt', delimiter=',')

# Load Torus Projection data to file
sdr4TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n4_TorusProj_layers_5.txt', delimiter=',')
sdr6TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n6_TorusProj_layers_5.txt', delimiter=',')
sdr8TP = np.loadtxt('accuracy/c3t_sdr_vs_snr_n8_TorusProj_layers_5.txt', delimiter=',')

# Load Angle Only data to file
sdr4AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n4_AngleOnly_layers_5.txt', delimiter=',')
sdr6AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n6_AngleOnly_layers_5.txt', delimiter=',')
sdr8AO = np.loadtxt('accuracy/c3t_sdr_vs_snr_n8_AngleOnly_layers_5.txt', delimiter=',')

# Load Raw Channel data to file
sdr4Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n4_Raw_layers_5.txt', delimiter=',')
sdr6Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n6_Raw_layers_5.txt', delimiter=',')
sdr8Raw = np.loadtxt('accuracy/c3t_sdr_vs_snr_n8_Raw_layers_5.txt', delimiter=',')

# Create plot
fig,(ax1,ax2,ax3) = plt.subplots(1,3,gridspec_kw={'width_ratios': [1,1,1]},figsize=(12,8))

# OPTA
ax1.plot(snr_range, opta4, '--', ms=8, c='0', label='OPTA')
ax2.plot(snr_range, opta6, '--', ms=8, c='0', label='OPTA')
ax3.plot(snr_range, opta8, '--', ms=8, c='0', label='OPTA')

# Repetition code
ax1.plot(snr_range, sdr4Rep, marker='*', ms=8, c='#FF7F0E', label='REP')
ax2.plot(snr_range, sdr6Rep, marker='*', ms=8, c='#FF7F0E', label='REP')
ax3.plot(snr_range, sdr8Rep, marker='*', ms=8, c='#FF7F0E', label='REP')

# MAP
ax1.plot(snr_range, sdr4MAP, marker='d', ms=8, c='#33FF00', label='MAP')
ax2.plot(snr_range, sdr6MAP, marker='d', ms=8, c='#33FF00', label='MAP')
ax3.plot(snr_range, sdr8MAP, marker='d', ms=8, c='#33FF00', label='MAP')

# Torus Projection
ax1.plot(snr_range, sdr4TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')
ax2.plot(snr_range, sdr6TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')
ax3.plot(snr_range, sdr8TP, marker='o', ms=8, c='#f032e6', label='C3T_TP')

# Angle only
'''
ax1.plot(snr_range, sdr4AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')
ax2.plot(snr_range, sdr6AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')
ax3.plot(snr_range, sdr8AO, marker='s', ms=8, c='#0A75AD', label='C3T_AO')
'''
# Raw observations
ax1.plot(snr_range, sdr4Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')
ax2.plot(snr_range, sdr6Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')
ax3.plot(snr_range, sdr8Raw, marker='P', ms=8, c='#FF0000', label='C3T_RAW')

# Labels for axis
ax1.set_xlabel('SNR [dB]')
ax2.set_xlabel('SNR [dB]')
ax3.set_xlabel('SNR [dB]')
ax1.set_ylabel('SDR [dB]')

# Turn on grid
ax1.grid(True)
ax2.grid(True)
ax3.grid(True)

# Turn on legends
ax1.legend()
ax2.legend()
ax3.legend()

# Set title
ax1.set_title('n=4')
ax2.set_title('n=6')
ax3.set_title('n=8')

# Set range of axis
ymin = -5
ymax = 35
ax1.set_ylim([ymin,ymax])
ax2.set_ylim([ymin,ymax])
ax3.set_ylim([ymin,ymax])

# Save fig
fig.suptitle('Signal-to-Distorion Ratio vs SNR')
fig.savefig('plots/SDR_vs_SNR_n4_n6_n8'+'.png', bbox_inches='tight')

