"""
    Constant Curvature Curve Tube Codes for Analog Error Correction
    
    Evaluate the MLP

    Anders Buvarp

    The Bradley Department of Electrical and Computer Engineering
    Virginia Tech
"""

import torch
import matplotlib.pyplot as plt
import numpy as np
import torch.nn as nn
		
def calc_sdr(variance,mse):
    sdr = 10*np.log10(variance/mse)
    return sdr
    
def c3t_eval(model, testloader, signal_variance, snr_min, snr_max, plot_flag=False):

    # Sum of squared error
    sum_sqe = 0
    sum_err = 0
    total = 0

    with torch.no_grad():
        for data in testloader:

            # Extract data from testloader
            inp, lbl = data
            total += lbl.size(0)

            # Forward propagation
            outp = model(inp)
            outp = outp.view(-1,1)

            # Compute MSE
            err = torch.abs(outp - lbl)
            sqe = err**2
            sum_err += err.sum().item()
            sum_sqe += sqe.sum().item()

    acc = 1 - sum_err / total
    mse = sum_sqe / total
    sdr = calc_sdr(signal_variance,mse)
    return acc,mse,sdr

def plot(x,y,z,ylabel,zlabel,snr_min,snr_max):

    plt.figure(figsize=(10,6))
    plt.scatter(x, y, color = "orange", label=ylabel)
    plt.scatter(x, z, color = "red", label=zlabel)
    plt.title('Alpha vs Alpha Hat    SNR Range: %d dB to %d dB' % (snr_min,snr_max))
    plt.xlabel('Alpha')
    plt.ylabel('Alpha Hat')
    plt.grid(True)
    plt.legend()
    plt.savefig("regr.png")

# EOF
